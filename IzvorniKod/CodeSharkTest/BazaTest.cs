using System;
using Xunit;
using backend;
using backend.Models;
using backend.DTOs;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;

namespace CodeSharkTest
{
    public class BazaTest
    { 
        [Fact]
        public void db_get_stats_dobro_izracunato()
        {
            //Arrange
            Natjecanje natjecanje = new Natjecanje
            {
                IdNatjecanje = 13,
                BrojZadataka = 3,
            };

            var staVracaBaza = new List<KorisnikRezultatDTO> {
                new KorisnikRezultatDTO(129, "filiphorvat", 12,  3, 0,  new DateTime(2022, 1, 8, 17, 14, 29), new DateTime(2022, 1, 20, 18, 0, 0, 0)),
                new KorisnikRezultatDTO(129, "filiphorvat", 12,  3, 3,  new DateTime(2022, 1, 8, 17, 18, 28), new DateTime(2022, 1, 20, 18, 0, 0, 0)),
                new KorisnikRezultatDTO(118, "djano123", 11,  5, 0,  new DateTime(2022, 1, 9, 14, 34, 2), new DateTime(2022, 1, 20, 18, 0, 0, 0)),
                new KorisnikRezultatDTO(118, "djano123", 11,  5, 0,  new DateTime(2022, 1, 9, 14, 37, 11), new DateTime(2022, 1, 20, 18, 0, 0, 0))
            };

            var korisnici = new List<Korisnik>
            {
                new Korisnik
                {
                    IdKorisnik = 129,
                    KorisnickoIme = "filiphorvat",
                },
                new Korisnik
                {
                    IdKorisnik = 118,
                    KorisnickoIme = "djano123",
                }
            };

            var statsOcekivano = new List<Statistika> {
                new Statistika
                {
                    Bodovi = 3,
                    KorisnickoIme = "filiphorvat",
                    IdNatjecanje = 13,
                    IdKorisnik = 129,
                },
                new Statistika
                {
                    Bodovi = 0,
                    KorisnickoIme = "djano123",
                    IdNatjecanje = 13,
                    IdKorisnik = 118
                }
            };
            var statsDobiveno = new List<Statistika>();

            //Act - maknut try catch block
            foreach (var kor in korisnici)
            {
                var korisnik = kor.KorisnickoIme;
                var stat = new Statistika
                {
                    IdNatjecanje = natjecanje.IdNatjecanje,
                    IdKorisnik = kor.IdKorisnik,
                    KorisnickoIme = kor.KorisnickoIme,
                    Bodovi = 0
                };

                var rezultati = staVracaBaza.Where(m => m.KorisnickoIme == korisnik).ToList();
                var zadatci = rezultati.Select(rez => rez.IdZadatak).Distinct().ToList();
                var najboljiRezultati =
                    zadatci
                        .Select(zadatak => rezultati.Where(rez => rez.IdZadatak == zadatak)
                            .OrderByDescending(rez => rez.DobiveniBodovi)
                            .First())
                        .ToList();

                foreach (var rezultat in najboljiRezultati)
                {
                    stat.Bodovi += rezultat.DobiveniBodovi;
                }

                if (najboljiRezultati.Count == natjecanje.BrojZadataka && najboljiRezultati.All(rez => rez.DobiveniBodovi == rez.Bodovi))
                {
                    var zadnjiRezultat = rezultati.OrderByDescending(rez => rez.VrijemePredaje).First();
                    var bonus = Convert.ToInt32((zadnjiRezultat.VrijemeKraj - zadnjiRezultat.VrijemePredaje).TotalMilliseconds) * decimal.Divide(1, 1000000);
                    stat.Bodovi += bonus;
                }

                statsDobiveno.Add(stat);
            }

            //Assert
            Assert.Equal(3, statsOcekivano.Sum(x => x.Bodovi));
        }
        [Fact]
        public void db_get_stats_natjecanje_koje_nepostoji()
        {
            //Arrange
            Natjecanje natjecanje = new Natjecanje
            {
                IdNatjecanje = 42,
                BrojZadataka = 0,
            };

            var staVracaBaza = new List<KorisnikRezultatDTO>();

            var korisnici = new List<Korisnik>();

            var statsOcekivano = new List<Statistika>();
            var statsDobiveno = new List<Statistika>();

            //Act - maknut try catch block
            foreach (var kor in korisnici)
            {
                var korisnik = kor.KorisnickoIme;
                var stat = new Statistika
                {
                    IdNatjecanje = natjecanje.IdNatjecanje,
                    IdKorisnik = kor.IdKorisnik,
                    KorisnickoIme = kor.KorisnickoIme,
                    Bodovi = 0
                };

                var rezultati = staVracaBaza.Where(m => m.KorisnickoIme == korisnik).ToList();
                var zadatci = rezultati.Select(rez => rez.IdZadatak).Distinct().ToList();
                var najboljiRezultati =
                    zadatci
                        .Select(zadatak => rezultati.Where(rez => rez.IdZadatak == zadatak)
                            .OrderByDescending(rez => rez.DobiveniBodovi)
                            .First())
                        .ToList();

                foreach (var rezultat in najboljiRezultati)
                {
                    stat.Bodovi += rezultat.DobiveniBodovi;
                }

                if (najboljiRezultati.Count == natjecanje.BrojZadataka && najboljiRezultati.All(rez => rez.DobiveniBodovi == rez.Bodovi))
                {
                    var zadnjiRezultat = rezultati.OrderByDescending(rez => rez.VrijemePredaje).First();
                    var bonus = Convert.ToInt32((zadnjiRezultat.VrijemeKraj - zadnjiRezultat.VrijemePredaje).TotalMilliseconds) * decimal.Divide(1, 1000000);
                    stat.Bodovi += bonus;
                }

                statsDobiveno.Add(stat);
            }

            //Assert
            Assert.Equal(0, statsOcekivano.Sum(x => x.Bodovi));
        }

        [Fact]
        public void db_postoji_li_natjecanje_ocekivano_da()
        {
            //Arrange
            int idNatjecanje = 3214;
            var Natjecanjes = new List<Natjecanje>(); 
            Natjecanje natjecanje = new Natjecanje
            {
                IdNatjecanje = idNatjecanje,
                BrojZadataka = 0,
            };
            Natjecanjes.Add(natjecanje);

            //Act
            bool dobiveno = Natjecanjes.Any(x => x.IdNatjecanje == idNatjecanje);

            //Assert
            Assert.True(dobiveno);
        }

        [Fact]
        public void db_postoji_li_natjecanje_ocekivano_ne()
        {
            //Arrange
            int idNatjecanje = 3214;
            var Natjecanjes = new List<Natjecanje>();
            Natjecanje natjecanje = new Natjecanje
            {
                IdNatjecanje = 1234,
                BrojZadataka = 0,
            };

            Natjecanjes.Add(natjecanje);

            //Act
            bool dobiveno = Natjecanjes.Any(x => x.IdNatjecanje == idNatjecanje);

            //Assert
            Assert.False(dobiveno);
        }
        [Fact]
        public void db_dohvati_korisnike_nepotvrdena_privilegija_ima()
        {
            //Arrange
            var Korisniks = new List<Korisnik>();
            Korisnik fakeKorisnik = new Korisnik
            {
                IdKorisnik = 123,
                PotvrdjenaPrivilegija = false
            };

            var fakeKorisnik2 = new Korisnik { 
                IdKorisnik = 124, 
                PotvrdjenaPrivilegija = true};

            Korisniks.Add(fakeKorisnik);
            Korisniks.Add(fakeKorisnik2);

            var ocekivano = 1;
            //Act
            var nepotvrdeni = Korisniks.Where(k => k.PotvrdjenaPrivilegija == false).ToList();

            //Assert
            Assert.Equal(ocekivano, nepotvrdeni.Count);
        }

        [Fact]
        public void db_dohvati_korisnike_nepotvrdena_privilegija_nema()
        {
            //Arrange
            var Korisniks = new List<Korisnik>();
            Korisnik fakeKorisnik = new Korisnik
            {
                IdKorisnik = 123,
                PotvrdjenaPrivilegija = true
            };

            var fakeKorisnik2 = new Korisnik
            {
                IdKorisnik = 124,
                PotvrdjenaPrivilegija = true
            };

            Korisniks.Add(fakeKorisnik);
            Korisniks.Add(fakeKorisnik2);

            Korisniks.Add(fakeKorisnik);
            Korisniks.Add(fakeKorisnik2);

            var ocekivano = 0;
            //Act
            var nepotvrdeni = Korisniks.Where(k => k.PotvrdjenaPrivilegija == false).ToList();

            //Assert
            Assert.Equal(ocekivano, nepotvrdeni.Count);
        }
        
        
    }
}
