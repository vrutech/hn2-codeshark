import React, { FC } from "react";
import { OldCompView } from "components/competitions/OldCompView";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const OldCompViewPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <OldCompView />
        </Flex>
    );
};

export default OldCompViewPage;