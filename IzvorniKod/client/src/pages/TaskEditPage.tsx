import React, { FC } from "react";
import { TaskEdit } from "components/tasks/TaskEdit";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const TaskEditPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <TaskEdit />
        </Flex>
    );
};

export default TaskEditPage;