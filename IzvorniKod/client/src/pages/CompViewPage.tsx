import React, { FC } from "react";
import { CompView } from "components/competitions/CompView";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const CompViewPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <CompView />
        </Flex>
    );
};

export default CompViewPage;