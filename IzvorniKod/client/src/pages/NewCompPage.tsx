import React, { FC } from "react";
import { NewComp } from "components/competitions/NewComp";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const NewCompPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <NewComp />
        </Flex>
    );
};

export default NewCompPage;