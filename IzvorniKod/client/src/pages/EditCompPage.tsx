import React, { FC } from "react";
import { EditComp } from "components/competitions/EditComp";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const EditCompPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <EditComp />
        </Flex>
    );
};

export default EditCompPage;