import { Button, Circle, Flex, Grid, Link, Text } from "@chakra-ui/react";
import axios from "axios";
import { NavBar } from "components/shared/NavBar";
import React, { FC, useEffect, useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { useHistory } from "react-router";

export interface Natjecanje {
  idNatjecanje: number;
  nazivNatjecanje: string;
  vrijemePocetak: string;
  vrijemeKraj: string;
  brojZadataka: number;
  voditeljTvorac?: string;
}

const getNormalizedDate = (date: Date) => {
  date.setHours(0, 0, 0, 0);

  return date;
};

const CalendarPage: FC = () => {
  const [natjecanja, setNatjecanja] = useState<Natjecanje[]>([]);
  const [filtriranaNatjecanja, setFiltriranaNatjecanja] = useState<
    Natjecanje[]
  >([]);

  const [datumi, setDatumi] = useState<Date[]>([]);
  const { push } = useHistory();

  useEffect(() => {
    const getNatjecanja = async () => {
      const response = await axios.get<Natjecanje[]>(
        process.env.REACT_APP_URL_PREFIX + "/Natjecanje/GetSvaNatjecanja"
      );

      setNatjecanja(response.data);
      setFiltriranaNatjecanja(response.data);
      const dates = response.data.map((natjecanje) =>
        getNormalizedDate(new Date(natjecanje.vrijemePocetak))
      );

      setDatumi(dates);
    };

    getNatjecanja();
  }, []);

  function chooseComp(id) {
    push('/comp/' + id);
  }

  return (
    <>
      <NavBar />

      <Flex
        direction="column"
        align="center"
        bg="white"
        borderRadius="md"
        mx="3"
      >
        <Calendar
          view="month"
          onClickDay={(day) => {
            const filtr = natjecanja.filter(
              (natjecanje) =>
                getNormalizedDate(
                  new Date(natjecanje.vrijemePocetak)
                ).toString() === day.toString()
            );

            setFiltriranaNatjecanja(filtr);
          }}
          tileContent={({ date }) => {
            const hasDate = datumi.find(
              (datum) => datum.toString() === date.toString()
            );

            return hasDate ? <Circle size="1" bg="blue.500" mx="auto" /> : null;
          }}
        />
        <Grid
          templateColumns="repeat(3, 1fr)"
          gap="1"
          width="550px"
          placeItems="center"
          fontWeight="bold"
          fontSize="xl"
        >
          <Text>Naziv natjecanja</Text>
          <Text>Broj zadataka</Text>
          <Text>Vrijeme natjecanja</Text>
        </Grid>
        {filtriranaNatjecanja
          ?.sort(
            (a, b) =>
              new Date(a.vrijemePocetak).getTime() -
              new Date(b.vrijemePocetak).getTime()
          )
          .map((natjecanje) => (
            <Grid
              key={natjecanje.idNatjecanje}
              templateColumns="repeat(3, 1fr)"
              gap="1"
              width="550px"
              placeItems="center"
              p="1"
            ><Link>
                <Text onClick={() => chooseComp(natjecanje.idNatjecanje)}>{natjecanje.nazivNatjecanje}</Text>
              </Link>
              <Text>{natjecanje.brojZadataka}</Text>
              <Text>
                {new Date(natjecanje.vrijemePocetak).toLocaleTimeString()}

              </Text>

            </Grid>

          ))}
      </Flex>
    </>
  );
};

export default CalendarPage;
