import React, { FC } from "react";
import { NewTaskForm } from "components/tasks/NewTask";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const NewTaskPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <NewTaskForm />
        </Flex>
    );
};

export default NewTaskPage;

