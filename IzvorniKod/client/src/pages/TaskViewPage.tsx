import React, { FC } from "react";
import { TaskView } from "components/tasks/TaskView";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const TaskViewPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >

            </Flex>
            <TaskView />
        </Flex>
    );
};

export default TaskViewPage;