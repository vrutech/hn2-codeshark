import { Flex } from "@chakra-ui/react";
import { ProfileBody } from "components/profileCreation/ProfileBody";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";

const ProfilePage: FC = () => {
  return (
    <Flex direction="column">
      <NavBar />

      <ProfileBody />
    </Flex>
  );
};

export default ProfilePage;
