import React, { FC } from "react";
import { TaskSearch } from "components/tasks/TaskSearch";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const TaskSearchPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >

            </Flex>
            <TaskSearch />
        </Flex>
    );
};

export default TaskSearchPage;