import { Box, Center, Heading, Link, VStack, Text } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";
import { getUser } from "../components/shared/Utils";
import { PhoneIcon, AddIcon, WarningIcon, SettingsIcon, HamburgerIcon, SearchIcon, PlusSquareIcon } from '@chakra-ui/icons'

const LandingPage: FC = () => {
  const user = getUser();

  return (
    <>
      <NavBar />
      <VStack>
        {user.idPrivilegija === 1 ? (
          <Center>
            <SettingsIcon />
            <Link href="/admin-page">
              <Heading as="h4" size="md" ml={2} isTruncated>
                Administratorske stranice
              </Heading>
            </Link>
          </Center>
        ) : (
          <></>
        )}
        <Center>
          <HamburgerIcon />
          <Link href="/alltasks">
            <Heading as="h4" size="md" ml={2} isTruncated>
              Pregled svih zadataka
            </Heading>
          </Link>
        </Center>
        <Center>
          <SearchIcon />
          <Link href="/task-search">
            <Heading as="h4" size="md" ml={2} isTruncated>
              Pretraga zadataka
            </Heading>
          </Link>
        </Center>
        <Center>
          <SearchIcon />
          <Link href="/user-search">
            <Heading as="h4" size="md" ml={2} isTruncated>
              Pretraga korisnika
            </Heading>
          </Link>
        </Center>
        {user.idPrivilegija === 1 || user.idPrivilegija === 2 ? (
          <Center>
            <PlusSquareIcon />
            <Link href="/newcomp">
              <Heading as="h4" size="md" ml={2} isTruncated>
                Novo natjecanje
              </Heading>
            </Link>
          </Center>
        ) : (
          <></>
        )}
        <Center>
          <AddIcon />
          <Link href="/virtualcomp">
            <Heading as="h4" size="md" ml={2} isTruncated>
              Novo virtualno natjecanje
            </Heading>
          </Link>
        </Center>

        <Center>
          <Box w="35%" mt="20">
            <Center h="50%" w="100%"></Center>
          </Box>
        </Center>
        <Center>
          <Box>
            <Center>
              <Heading as="h2" size="lg" isTruncated>
                Dobrodošli na CodeShark!
              </Heading>
            </Center>
            <Center>
              <Heading as="h2" size="md" isTruncated>
                Možete krenuti sa rješavanjem zadataka...
              </Heading>
            </Center>
          </Box>
        </Center>
      </VStack>
    </>
  );
};

export default LandingPage;
