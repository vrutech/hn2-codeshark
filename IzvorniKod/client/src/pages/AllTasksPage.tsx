import React, { FC } from "react";
import { AllTasks } from "components/tasks/AllTasks";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const AllTasksPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >

            </Flex>
            <AllTasks />
        </Flex>
    );
};

export default AllTasksPage;