import React, { FC } from "react";
import { Statistics } from "components/competitions/Statistics";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const StatisticsPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <Statistics />
        </Flex>
    );
};

export default StatisticsPage;