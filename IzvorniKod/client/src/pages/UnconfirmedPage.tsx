import React, { FC } from "react";
import {
    Box,
    Flex,
    Text,
    useColorModeValue,
    useBreakpointValue,
    Heading,
    Center,
    Link
} from '@chakra-ui/react';
import ErrorImage from "components/ErrorImage";

const UnconfirmedPage: FC = () => {
    return <>
        <Box>
            <Flex
                bg={useColorModeValue('white', 'gray.800')}
                color={useColorModeValue('gray.600', 'white')}
                minH={'60px'}
                py={{ base: 2 }}
                px={{ base: 4 }}
                borderBottom={1}
                borderStyle={'solid'}
                borderColor={useColorModeValue('gray.200', 'gray.900')}
                align={'center'}>
                <Flex
                    flex={{ base: 1, md: 'auto' }}
                    ml={{ base: -2 }}
                    display={{ base: 'flex', md: 'none' }}>
                </Flex>
                <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
                    <Link href="/">
                        <Text
                            textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
                            fontFamily={'heading'}
                            color={useColorModeValue('gray.800', 'white')}>
                            <Heading>CodeShark</Heading>
                        </Text>
                    </Link>
                    <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
                        <Center>
                            <Heading as="h4" size="md" isTruncated>Nepotvrđeni korisnik</Heading>
                        </Center>
                    </Flex>
                </Flex>

            </Flex>
        </Box>
        <Heading> </Heading>
        <Center>
            <Box w="30%">
                <Center h="50%" w="100%">
                    <ErrorImage />
                </Center>
            </Box>
        </Center>
        <Box>
            <Center>
                <Heading as="h2" size="lg" isTruncated>Niste potvrdili svoj e-mail ili Vas administrator stranice nije potvrdio...</Heading>
            </Center>
        </Box>

    </>

};

export default UnconfirmedPage;
