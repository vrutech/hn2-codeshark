import React, { FC } from "react";
import { VirtualComp } from "components/competitions/VirtualComp";
import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";

const VirtualCompPage: FC = () => {
    return (
        <Flex direction="column">
            <NavBar />
            <Flex
                p={{
                    base: "0",
                    md: "6",
                }}
            >
            </Flex>
            <VirtualComp />
        </Flex>
    );
};

export default VirtualCompPage;