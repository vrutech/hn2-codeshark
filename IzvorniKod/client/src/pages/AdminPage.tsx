import React, { FC, useEffect, useState } from "react";
import {
  Box,
  Flex,
  Text,
  useColorModeValue,
  useBreakpointValue,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Heading,
  Center,
  Link,
} from "@chakra-ui/react";
import axios from "axios";
import { VerifyButton } from "components/shared/Buttons";
import { privilegijaMap } from "components/shared/Utils";

const AdminPage: FC = () => {
  var [repo, setRepo] = useState([]);

  const getRepo = () => {
    axios
      .get(process.env.REACT_APP_URL_PREFIX + "/korisnik/unconfirmedUsers")
      .then((response) => {
        console.log(response.status);
        console.log(response.data);
        const myRepo = response.data;

        for (const repo of myRepo) {
          repo.privilegijaIme = privilegijaMap[repo.idPrivilegija];
        }
        console.log(myRepo);
        setRepo(myRepo);
      });
  };

  useEffect(() => getRepo(), []);

  async function acceptUser(id) {
    try {
      await axios.get(
        process.env.REACT_APP_URL_PREFIX +
        "/korisnik/confirmUserPrivilege/" +
        id
      );
      getRepo();
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <>
      <Box>
        <Flex
          bg={useColorModeValue("white", "gray.800")}
          color={useColorModeValue("gray.600", "white")}
          minH={"60px"}
          py={{ base: 2 }}
          px={{ base: 4 }}
          borderBottom={1}
          borderStyle={"solid"}
          borderColor={useColorModeValue("gray.200", "gray.900")}
          align={"center"}
        >
          <Flex
            flex={{ base: 1, md: "auto" }}
            ml={{ base: -2 }}
            display={{ base: "flex", md: "none" }}
          ></Flex>
          <Flex flex={{ base: 1 }} justify={{ base: "center", md: "start" }}>
            <Link href="/landing-page">
              <Text
                textAlign={useBreakpointValue({ base: "center", md: "left" })}
                fontFamily={"heading"}
                color={useColorModeValue("gray.800", "white")}
              >
                <Heading>CodeShark</Heading>
              </Text>
            </Link>
            <Flex display={{ base: "none", md: "flex" }} ml={10}>
              <Center>
                <Heading as="h4" size="md" isTruncated>
                  Administratorske stranice
                </Heading>
              </Center>
            </Flex>

          </Flex>
        </Flex>
        <Center h="50%" w="100%">
          <Box>
            {repo.length === 0 ? (
              <>
                {" "}
                <Heading as="h2" size="lg" isTruncated>
                  Nema nepotvrđenih korisnika!
                </Heading>{" "}
              </>
            ) : (
              <>
                <Center>
                  <Heading as="h4" size="md" isTruncated>
                    Nepotvrđeni korisnici:
                  </Heading>
                </Center>

                <Table variant="striped" colorScheme="purple">
                  <Thead>
                    <Tr>
                      <Th>Korisničko ime</Th>
                      <Th>Email</Th>
                      <Th>Privilegija</Th>
                      <Th></Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {repo.map((element) => {
                      return (
                        <Tr key={element.idKorisnik}>
                          <Td>{element.korisnickoIme}</Td>
                          <Td>{element.email}</Td>
                          <Td>{element.privilegijaIme}</Td>
                          <Td>
                            <VerifyButton
                              colorScheme="facebook"
                              onClick={() => acceptUser(element.idKorisnik)}
                              value={element.idKorisnik}
                            >
                              Prihvati
                            </VerifyButton>
                          </Td>
                        </Tr>
                      );
                    })}
                  </Tbody>
                </Table>
              </>
            )}
          </Box>
        </Center>
      </Box>
    </>
  );
};

export default AdminPage;
