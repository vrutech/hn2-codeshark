import { VStack } from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

export const Logout = () => {
    localStorage.removeItem("user");
    const { push } = useHistory();
    push('/')
    return (<></>)
};

