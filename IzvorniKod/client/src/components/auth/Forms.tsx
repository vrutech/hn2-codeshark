import {
  Box,
  Flex,
  FormLabel,
  HStack,
  IconButton,
  Input,
  Radio,
  RadioGroup,
  Text,
  VStack,
} from "@chakra-ui/react";
import React, { FC, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory } from "react-router";
import { PrimaryButton, SecondaryButton } from "../shared/Buttons";
import axios from "axios";

interface Props {
  switchFormMode: VoidFunction;
}

interface LoginFormFields {
  email: string;
  password: string;
}

export const LoginForm: FC<Props> = ({ switchFormMode }) => {
  const { push } = useHistory();
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<LoginFormFields>({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const [authError, setAuthError] = useState<string | null>(null);

  const login: SubmitHandler<LoginFormFields> = async (data) => {
    setAuthError(null);

    try {
      const response = await axios.post(
        process.env.REACT_APP_URL_PREFIX + "/login",
        data
      );
      if (
        response.data.idPrivilegija === 1 &&
        response.data.potvrdjenaPrivilegija &&
        response.data.potvrdjenMail
      ) {
        if (response.data.token) {
          push("/admin-page");
          localStorage.setItem("user", JSON.stringify(response.data));
        }
      } else if (
        response.data.potvrdjenaPrivilegija &&
        response.data.potvrdjenMail
      ) {
        if (response.data.token) {
          console.log(response.data);
          localStorage.setItem("user", JSON.stringify(response.data));
          push("/landing-page");
        }
      } else {
        localStorage.setItem("user", JSON.stringify(response.data));
        push("/unconfirmed-page");
      }
    } catch (err) {
      console.log(err);
      setAuthError("Neispravan e-mail ili lozinka!");
    }
  };

  return (
    <>
      <Flex minW="full" justify="space-between">
        <Text fontSize="3xl" fontWeight="bold">
          Prijava
        </Text>
      </Flex>
      <VStack
        as="form"
        spacing="6"
        align="start"
        minW="full"
        onSubmit={handleSubmit(login)}
      >
        <Box minW="full">
          <FormLabel>E-mail</FormLabel>
          <Input {...register("email")} type="email" />
        </Box>
        <Box minW="full">
          <FormLabel>Lozinka</FormLabel>
          <Input {...register("password")} type="password" />
        </Box>
        {authError != null && <Box color="red.400">{authError}</Box>}
        <PrimaryButton
          type="submit"
          isLoading={isSubmitting}
          loadingText="Provjeravanje"
        >
          Prijava
        </PrimaryButton>
      </VStack>
      <Box minW="full">
        <Text textAlign="center" mb="3">
          Nemaš račun?
        </Text>
        <SecondaryButton onClick={switchFormMode}>
          Registriraj se
        </SecondaryButton>
      </Box>
    </>
  );
};

type Role = "admin" | "voditelj" | "natjecatelj";

const convertRole = (role: Role) => {
  switch (role) {
    case "admin":
      return 1;
    case "voditelj":
      return 2;
    case "natjecatelj":
      return 3;
    default:
      break;
  }
};

interface SignUpFormFields {
  ime: string;
  prezime: string;
  korisnickoIme: string;
  email: string;
  password: string;
}

export const SignUpForm: FC<Props> = ({ switchFormMode }) => {
  const { push } = useHistory();
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<SignUpFormFields>({
    defaultValues: {
      ime: "",
      prezime: "",
      korisnickoIme: "",
      email: "",
      password: "",
    },
  });
  const [role, setRole] = useState<Role>("natjecatelj");

  const signUp: SubmitHandler<SignUpFormFields> = async (data) => {
    const idPrivilegija = convertRole(role);

    const allData = {
      ...data,
      idPrivilegija,
    };
    console.log(allData);

    try {
      const response = await axios.post(
        process.env.REACT_APP_URL_PREFIX + "/register",
        allData
      );
      console.log(response.status);
      push("/unconfirmed-page");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Flex justify="space-between" minW="full">
        <Text fontSize="3xl" fontWeight="bold">
          Registracija
        </Text>
        <IconButton
          onClick={switchFormMode}
          aria-label="close sign up form"
          icon={<MdClose size="24px" />}
          variant="ghost"
        />
      </Flex>
      <VStack
        as="form"
        minW="full"
        align="start"
        spacing="3"
        onSubmit={handleSubmit(signUp)}
      >
        <HStack spacing="3">
          <Box>
            <FormLabel>Ime</FormLabel>
            <Input
              {...register("ime")}
              required
              disabled={isSubmitting}
              type="text"
            />
          </Box>
          <Box>
            <FormLabel>Prezime</FormLabel>
            <Input
              {...register("prezime")}
              required
              disabled={isSubmitting}
              type="text"
            />
          </Box>
        </HStack>
        <Box minW="full">
          <FormLabel>Korisničko ime</FormLabel>
          <Input {...register("korisnickoIme")} type="text" required />
        </Box>
        <Box minW="full">
          <FormLabel>E-mail</FormLabel>
          <Input {...register("email")} type="email" required />
        </Box>

        <Box minW="full">
          <FormLabel>Uloga</FormLabel>
          <RadioGroup
            name="role"
            defaultValue={role}
            onChange={(newRole: Role) => {
              setRole(newRole);
            }}
          >
            <Flex justify="space-between" w="full">
              <Radio value="natjecatelj"> Natjecatelj </Radio>
              <Radio value="voditelj"> Voditelj </Radio>
              <Radio value="admin">Admin</Radio>
            </Flex>
          </RadioGroup>
        </Box>

        <Box minW="full">
          <FormLabel>Lozinka</FormLabel>
          <Input
            {...register("password")}
            required
            disabled={isSubmitting}
            type="password"
          />
        </Box>
        <PrimaryButton
          type="submit"
          isLoading={isSubmitting}
          loadingText="Registriram"
        >
          Registracija
        </PrimaryButton>
      </VStack>
    </>
  );
};
