import { Flex, Heading, Link, useColorModeValue } from "@chakra-ui/react";
import React, { FC } from "react";

export const NavBar: FC = ({ children }) => {
  return (
    <Flex
      bg={useColorModeValue("white", "gray.800")}
      color={useColorModeValue("gray.600", "white")}
      minH="60px"
      py={{ base: "2" }}
      px={{ base: "4" }}
      borderBottom="1"
      borderStyle={"solid"}
      borderColor={useColorModeValue("gray.200", "gray.900")}
      align="center"
    >
      <Link href="/landing-page">
        <Heading>CodeShark</Heading>
      </Link>
      <Link href="/profile">
        <Heading as="h4" size="md" ml={10} isTruncated>
          Profil
        </Heading>
      </Link>
      <Link href="/calendar">
        <Heading as="h4" size="md" ml={10} isTruncated>
          Kalendar
        </Heading>
      </Link>
      <Link href="/logout" position="absolute" right="3%">
        <Heading as="h4" size="md" ml={10} isTruncated>
          Odjava
        </Heading>
      </Link>
    </Flex>
  );
};
