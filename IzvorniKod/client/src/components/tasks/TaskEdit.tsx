import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Button,
    Container,
    Center,
    Textarea
} from "@chakra-ui/react";
import React, { FC, useState, useEffect, Component, FormEventHandler } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { PrimaryButton, SecondaryButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";

interface Task {
    idZadatak: number,
    nazivZadatak: string,
    bodovi: number,
    maxVrijeme: number,
    tekstZadatak: string
}

const convertVidljivost = (vidljivostZad
    : Vidljivost) => {
    switch (vidljivostZad
    ) {
        case "privatan":
            return false;
        case "javan":
            return true;
        default:
            break;
    }
};



type Vidljivost = "privatan" | "javan";

export const TaskEdit: FC = ({ }) => {

    const [vidljivostZad
        , setVidljivost] = useState<Vidljivost>("javan");

    const [task, setTask] = useState<Task>();
    const { push } = useHistory();
    const taskId = useParams();
    const getTasks = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Zadatak/pregledSvihZadataka")
            .then((response) => {
                //console.log(response.status);
                //console.log(response.data);
                const myTasks = response.data;
                var myTask = null;
                console.log(myTasks)
                for (const zad of myTasks) {
                    if (zad.idZadatak == taskId.id) {
                        console.log(zad);
                        myTask = zad;
                    }
                }
                console.log(myTask)

                setTask(myTask);
            });
    }

    useEffect(() => getTasks(), []);
    var poruka = [];
    const [greska, setGreska] = useState([]);


    const submitForm = async (event) => {
        event.preventDefault();
        const NazivZadatak = document.getElementById("nazivZadatak").value;
        const Bodovi = parseInt(document.getElementById("bodovi").value);
        const MaxVrijeme = parseInt(document.getElementById("maxVrijeme").value);
        const TekstZadatak = document.getElementById("tekstZadatak").value;
        const Vidljivost = convertVidljivost(vidljivostZad);
        const zad = {
            NazivZadatak,
            Bodovi,
            MaxVrijeme,
            TekstZadatak,
            Vidljivost
        }
        console.log(zad);
        try {
            const response = await axios.post(process.env.REACT_APP_URL_PREFIX + "/Zadatak/promijeniZadatak/" + task?.idZadatak,
                zad)
            if (response.status === 200) {
                push('/alltasks')
            } else if (response.status === 400) {
                console.log(response);
            }
        } catch (err) {
            /*poruka.push(<>
                <Text color="red" minW="0" fontSize="lg">Zadatak s tim imenom već postoji</Text>
                </>)*/
            console.log(err)
            setGreska("Zadatak s tim imenom već postoji!");
        }

    }

    return (<>
        <Center w="100%">
            <VStack>
                <Center>
                    {greska !== null && <Center><Box minw="0" color="red.400" padding="0px"><Text>{greska}</Text></Box></Center>}
                </Center>
                <VStack
                    align="start"
                    minW={{
                        base: "100vw",
                        md: "300px",
                    }}
                    w="full"
                    maxW={{
                        base: "100vw",
                        md: "800px",
                    }}
                    minH={{
                        base: "100vh",
                        md: "400px",
                    }}
                    h="full"
                    maxH={{
                        base: "100vh",
                        md: "650px",
                    }}
                    bg="white"
                    boxShadow={{
                        base: "none",
                        md: "lg",
                    }}
                    borderRadius={{
                        base: "none",
                        md: "lg",
                    }}
                    mx="auto"
                    p="6"
                    spacing="6"
                >


                    <Flex justify="space-between" minW="full">
                        <Center w="full">
                            <VStack as="form" spacing="6" align="start" minW="full">

                                <Center w="full">
                                    <Flex minW="0">
                                        <Text fontSize="3xl" fontWeight="bold">
                                            Uređivanje zadatka broj {task?.idZadatak}
                                        </Text>
                                    </Flex>
                                </Center>
                                <Box minW="0" fontSize="lg">
                                    <Text fontWeight="bold">Naziv zadatka:</Text>
                                    <Input defaultValue={task?.nazivZadatak} w="25rem" id="nazivZadatak" required></Input>
                                </Box>

                                <Box minW="0" fontSize="lg">
                                    <Text fontWeight="bold">Maksimalni bodovi:</Text>
                                    <Input defaultValue={task?.bodovi} w="10rem" id="bodovi" required type="number"></Input>
                                </Box>
                                <Box minW="0" fontSize="lg">
                                    <Text fontWeight="bold">Vrijeme izvođenja u sekundama:</Text>
                                    <Input defaultValue={task?.maxVrijeme.totalSeconds} w="10rem" id="maxVrijeme" required type="number"></Input>
                                </Box>
                                <Box minW="0" fontSize="lg">
                                    <Text fontWeight="bold">Tekst zadatka:</Text>
                                    <Textarea defaultValue={task?.tekstZadatak} w="40rem" id="tekstZadatak" required></Textarea>
                                </Box>
                                <Box minW="full">
                                    <Text fontSize="lg" fontWeight="bold">Vidljivost zadatka:</Text>
                                    <RadioGroup
                                        name="vidljivostZad
                                    " w="15rem"
                                        defaultValue={vidljivostZad
                                        }
                                        onChange={(newVidljivost: Vidljivost) => {
                                            setVidljivost(newVidljivost);
                                        }}
                                    >
                                        <Flex justify="space-between" w="full">
                                            <Radio value="privatan"> Privatan </Radio>
                                            <Radio value="javan"> Javan </Radio>
                                        </Flex>
                                    </RadioGroup>
                                </Box>
                                <Flex as="form">
                                    <Button onClick={submitForm}>Spremanje promjena</Button>
                                </Flex>

                            </VStack>
                        </Center>
                    </Flex>
                </VStack>
            </VStack>
        </Center>
    </>)
}