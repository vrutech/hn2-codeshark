import {
    Box,
    Flex,
    Text,
    useColorModeValue,
    useBreakpointValue,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Heading,
    Center,
    Link,
    Button,
    FormControl,
    FormLabel,
    Input,
    VStack,
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";

interface Task {
    idZadatak: number,
    nazivZadatak: string,
    bodovi: number,
    maxVrijeme: number,
    tekstZadatak: string
}

export const TaskSearch: FC = () => {
    var [task, setTasks] = useState([]);
    const { push } = useHistory();
    const user = getUser();
    var [str, setStr] = useState("");
    var [search, setSearch] = useState(false);

    const getTasks = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Zadatak/pregledSvihZadataka")
            .then((response) => {
                console.log(response.status);
                console.log(response.data);
                const myTasks = response.data;
                var lista = []
                for (const task of response.data) {
                    if (task.nazivZadatak.includes(str) && str !== "") {
                        lista.push(task);
                    }
                }
                setSearch(true);
                setTasks(lista);
            });
    }

    function chooseTask(id) {
        push('/task/' + id);
    }
    function editTask(id) {
        push('/task/edit/' + id);
    }

    return (
        <>
            <Center w="full">
                <VStack>
                    <Heading size="lg">Pretraga zadataka</Heading>
                    <Flex
                        bg={useColorModeValue('white', 'gray.800')}
                        color={useColorModeValue('gray.600', 'white')}
                        minH={'60px'}
                        py={{ base: 2 }}
                        px={{ base: 4 }}
                        borderBottom={1}
                        borderStyle={'solid'}
                        borderColor={useColorModeValue('gray.200', 'gray.900')}
                        align={'center'}
                    >

                        <Box>
                            <FormControl>

                                <FormLabel htmlFor='username'>Unesite znakove za pretraživanje zadatka:</FormLabel>
                                <Input
                                    id='username'
                                    type='text'
                                    required
                                    value={str}
                                    onChange={(e) => setStr(e.target.value)} />
                                <Center w="full">
                                    <Button
                                        onClick={getTasks}
                                    >Pretraga
                                    </Button>
                                </Center>
                            </FormControl>
                            <Center h="50%" w="100%">
                                <Box>
                                    {!search ? <></> : <>
                                        {task.length === 0 ? (
                                            <>
                                                {" "}
                                                <Heading as="h2" size="lg" isTruncated>
                                                    Nema zadataka!
                                                </Heading>{" "}
                                            </>
                                        ) : (
                                            <>
                                                <Center>
                                                    <Heading as="h4" size="md" isTruncated>
                                                        Lista svih zadataka:
                                                    </Heading>
                                                </Center>

                                                <Table variant="striped" colorScheme="purple">
                                                    <Thead>
                                                        <Tr>
                                                            <Th>Naziv zadatka</Th>
                                                            <Th>Broj bodova</Th>
                                                            <Th>Vrijeme izvođenja</Th>
                                                            <Th></Th>
                                                            <Th></Th>
                                                        </Tr>
                                                    </Thead>
                                                    <Tbody>
                                                        {task.map((element) => {
                                                            return (
                                                                <Tr key={element.idZadatak}>
                                                                    <Td>{element.nazivZadatak}</Td>
                                                                    <Td>{element.bodovi}</Td>
                                                                    <Td>{element.maxVrijeme.totalSeconds}</Td>
                                                                    <Td>
                                                                        <VerifyButton
                                                                            colorScheme="facebook"
                                                                            onClick={() => chooseTask(element.idZadatak)}
                                                                            value={element.idZadatak}
                                                                        >
                                                                            Odaberi zadatak
                                                                        </VerifyButton>
                                                                    </Td>
                                                                    {element.idKorisnik === user.idKorisnik || user.idPrivilegija === 1 ? <Td>
                                                                        <VerifyButton
                                                                            colorScheme="facebook"
                                                                            onClick={() => editTask(element.idZadatak)}
                                                                            value={element.idZadatak}
                                                                        >
                                                                            Uređivanje
                                                                        </VerifyButton>
                                                                    </Td> : <></>}
                                                                </Tr>
                                                            );
                                                        })}
                                                    </Tbody>
                                                </Table>
                                            </>
                                        )}
                                    </>}
                                    {user.idPrivilegija === 3 ? <></> : (
                                        <>
                                            <Center>

                                                <Link href="/newtask">

                                                    <Heading as="h4" size="md" isTruncated>
                                                        Dodajte novi zadatak
                                                    </Heading>
                                                </Link>
                                            </Center>
                                        </>)}

                                </Box>
                            </Center>

                        </Box>
                    </Flex>
                </VStack>
            </Center>
        </>
    );;
}
