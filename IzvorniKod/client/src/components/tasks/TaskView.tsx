import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Button,
    Container,
    Center,
    Spinner
} from "@chakra-ui/react";
import React, { FC, useState, useEffect, Component, FormEventHandler } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { PrimaryButton, SecondaryButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";

interface Task {
    idZadatak: number,
    nazivZadatak: string,
    bodovi: number,
    maxVrijeme: number,
    tekstZadatak: string
}

export const TaskView: FC = () => {

    const [task, setTask] = useState<Task>();
    const { push } = useHistory();
    const taskId = useParams();
    var [spinner, setSpinner] = useState(false);
    const getTasks = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Zadatak/pregledSvihZadataka")
            .then((response) => {
                //console.log(response.status);
                //console.log(response.data);
                const myTasks = response.data;
                var myTask = null;
                console.log(myTasks)
                for (const zad of myTasks) {
                    if (zad.idZadatak == taskId.id) {
                        console.log(zad);
                        myTask = zad;
                    }
                }
                console.log(myTask)

                setTask(myTask);
            });
    }

    useEffect(() => getTasks(), []);
    //console.log(taskId);
    /*useEffect(() => {
        const getTasks = async () => {
            var myTask = null;

            const response = await axios.get(process.env.REACT_APP_URL_PREFIX + "/Zadatak/PregledSvihZadataka");
            const myTasks = response.data;
            console.log(myTasks)
            for (const zad of myTasks) {
                if (zad.idZadatak === taskId) {
                    myTask = zad;
                }
            }

            setTask(myTask);


            /*var myTasks = null
            axios
                .get(process.env.REACT_APP_URL_PREFIX + "/PregledSvihZadataka")
                .then((response) => {
                    console.log(response.status);
                    console.log(response.data);
                    myTasks = JSON.parse(response.data);
                });
    
            for (zad of myTasks) {
    
            }
        }
    }, []);*/
    /*async function getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }*/
    /*const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });*/
    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(26));
        reader.onerror = error => reject(error);
    });

    async function getBase64(file) {
        var reader = new FileReader();
        await reader.readAsDataURL(file);
        reader.onload = function () {
            return reader.result?.slice(26);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }


    const sendFile = async (event) => {
        event.preventDefault();
        const formData = new FormData(event.target as HTMLFormElement);
        const rjesenje = formData.get("file-input") as File;
        const base64rjesenje = await toBase64(rjesenje);

        const array = await rjesenje.arrayBuffer();
        console.log(array);

        const response = await axios.post(
            process.env.REACT_APP_URL_PREFIX + "/Rezultat/provjeraRjesenja",
            {
                programBase64: base64rjesenje,
            },
            {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            }
        );
        console.log(response.data);
    };

    const [uploadFile, setUploadFile] = React.useState();
    var povratnaPoruka;

    const [dobiveniBodovi, setDobiveniBodovi] = useState();
    const [vrijemeMS, setVrijemeMS] = useState();
    //var dobiveniBodovi;
    //var vrijemeMS;

    const submitForm = async (event) => {
        setSpinner(true);
        event.preventDefault();
        var programBase64 = null;
        var file = await document.getElementById('fajl').files[0];
        const user = getUser();
        const idKorisnik = user.idKorisnik;
        const korisnickoIme = user.korisnickoIme;
        const idZadatak = task?.idZadatak;
        console.log(idKorisnik);
        console.log(korisnickoIme);
        console.log(idZadatak);

        programBase64 = toBase64(file).then(data => axios
            .post(process.env.REACT_APP_URL_PREFIX + "/Rezultat/predajRjesenje", {
                programBase64: data,
                idKorisnik: idKorisnik,
                korisnickoIme: korisnickoIme,
                idZadatak: idZadatak
            }
            )
            .then((response) => {
                povratnaPoruka = response.data.povratnaPoruka;
                setDobiveniBodovi(response.data.dobiveniBodovi);
                setVrijemeMS(response.data.vrijemeMS.totalSeconds);
                console.log(response.data)
                setSpinner(false);
            })
            .catch((error) => {
                console.log(error)
                setSpinner(false);
            }))

        /*axios
            .post(process.env.REACT_APP_URL_PREFIX + "/Rezultat/predajRjesenje", {
                programBase64: programBase64,
                idKorisnik: idKorisnik,
                korisnickoIme: korisnickoIme,
                idZadatak: idZadatak
            }
            )
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })

        /*const dataArray = new FormData();
        dataArray.append("uploadFile", uploadFile);
        base64rjesenje = await toBase64(dataArray);*/


    };


    return (<>
        <Center w="100%">
            <VStack
                align="start"
                minW={{
                    base: "100vw",
                    md: "300px",
                }}
                w="full"
                maxW={{
                    base: "100vw",
                    md: "800px",
                }}
                minH={{
                    base: "100vh",
                    md: "400px",
                }}
                h="full"
                maxH={{
                    base: "100vh",
                    md: "650px",
                }}
                bg="white"
                boxShadow={{
                    base: "none",
                    md: "lg",
                }}
                borderRadius={{
                    base: "none",
                    md: "lg",
                }}
                mx="auto"
                p="6"
                spacing="6"
            >


                <Flex justify="space-between" minW="full">

                    <Center w="full">
                        <VStack as="form" spacing="6" align="start" minW="full">
                            <Center w="full">
                                <Flex minW="0">
                                    <Text fontSize="3xl" fontWeight="bold">
                                        Zadatak: {task?.nazivZadatak}
                                    </Text>
                                </Flex>
                            </Center>

                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Id zadatka:</Text>
                                <Text>{task?.idZadatak}</Text>
                            </Box>

                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Maksimalni bodovi:</Text>
                                <Text> {task?.bodovi}</Text>
                            </Box>
                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Vrijeme izvođenja u sekundama:</Text>
                                <Text>{task?.maxVrijeme.totalSeconds}</Text>
                            </Box>

                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Tekst zadatka:</Text>
                                <Text>{task?.tekstZadatak}</Text>
                            </Box>
                            <Flex as="form">
                                <Input type="file" id="fajl" onChange={(e) => setUploadFile(e.target.files)} />
                                <Button onClick={submitForm}>Provjera rješenja</Button>
                            </Flex>
                            {spinner ? <Spinner></Spinner> : <></>}
                            {dobiveniBodovi !== undefined ? (<Text fontSize="md" fontWeight="bold">Dobiveni bodovi: {dobiveniBodovi} / {task?.bodovi}</Text>) : (<></>)}
                            {vrijemeMS !== undefined ? (<Text fontSize="md" fontWeight="bold">Vrijeme izvođenja: {vrijemeMS} sekundi</Text>) : (<></>)}
                        </VStack>
                    </Center>
                </Flex>
            </VStack>
        </Center>
    </>)
}