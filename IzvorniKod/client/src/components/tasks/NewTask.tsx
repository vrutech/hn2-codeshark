import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select
} from "@chakra-ui/react";
import React, { FC, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory } from "react-router";
import { PrimaryButton, SecondaryButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";
import { waitFor } from "@testing-library/react";

interface NewTaskFormFields {
    nazivZadatak: string;
    bodovi: number;
    maxVrijeme: number;
    tekstZadatak: string;
}

type Vidljivost = "privatan" | "javan";

/*interface TestniPodaciArray {
    TestniPodaci: TestniPodaciFields
}*/
interface TestniPodaciFields {
    [index: number]: TestniPodaciValues
}

interface TestniPodaciValues {
    Ulaz: string;
    Izlaz: string;
    IdTestnogPodatka: number;
}

const convertVidljivost = (vidljivostZad
    : Vidljivost) => {
    switch (vidljivostZad
    ) {
        case "privatan":
            return false;
        case "javan":
            return true;
        default:
            break;
    }
};
export const NewTaskForm: FC = ({ }) => {
    const { push } = useHistory();
    const {
        register,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm<NewTaskFormFields>({
        defaultValues: {
            nazivZadatak: "",
            bodovi: 1,
            maxVrijeme: 1
        },
    });

    const [authError, setAuthError] = useState<string | null>(null);

    const [vidljivostZad
        , setVidljivost] = useState<Vidljivost>("javan");
    console.log(localStorage.user.idKorisnik);
    const getInitialState = () => {
        const value = "1";
        return value;
    };

    const [value, setValue] = useState(getInitialState);

    const handleChange = (e) => {
        setValue(e.target.value);
    };

    const [result, setResult] = useState([]);
    const [uploadFile, setUploadFile] = React.useState();

    var unosi = [];
    for (var i = 0; i < parseInt(value); i++) {
        unosi.push(<>
            <FormLabel>Ulaz {i + 1}</FormLabel>
            <Input type="file" id={"input" + (i + 1).toString()} onChange={(e) => setUploadFile(e.target.files)} />
            <FormLabel>Izlaz {i + 1}</FormLabel>
            <Input type="file" id={"output" + (i + 1).toString()} onChange={(e) => setUploadFile(e.target.files)} />
        </>);
    }

    async function getBase64(file) {
        var reader = new FileReader();
        await reader.readAsDataURL(file);
        reader.onload = function () {
            return reader.result?.slice(23);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(23));
        reader.onerror = error => reject(error);
    });

    const newTask: SubmitHandler<NewTaskFormFields> = async (data) => {
        var testniPodaci = []
        var ulaz;
        var izlaz;
        var brojDat = []
        for (let i = 0; i < parseInt(value); i++) {
            brojDat.push(i + 1);
        }
        const vidljivost = convertVidljivost(vidljivostZad);
        console.log(vidljivost)
        const user = getUser();
        const idKorisnik = user.idKorisnik
        brojDat.forEach(async element => {
            ulaz = document.getElementById("input" + element.toString()).files[0]
            izlaz = document.getElementById("output" + element.toString()).files[0]
            var objekt = {
                ulaz: "",
                izlaz: "",
                idTesniPodaci: element
            }
            toBase64(ulaz).then(data => objekt.ulaz = data)
            toBase64(izlaz).then(data => objekt.izlaz = data)
            testniPodaci.push(objekt)

        });

        const allData = {
            ...data,
            vidljivost,
            idKorisnik,
            testniPodaci
        };
        console.log(allData)

        console.log("hello")
        setTimeout(async () => {
            try {
                const response = await axios.post(
                    process.env.REACT_APP_URL_PREFIX + "/Zadatak/noviZadatak",
                    allData)
                if (response.status === 200)
                    push('/alltasks');
                else console.log(response);
            } catch (err) {
                console.log(err);
                setAuthError("Niste ulogirani!");
            }
        }, 1000);
    };

    return (<>

        <Flex justify="space-between" minW="100%">
            <Center w='100%'>
                <Flex>
                    <VStack
                        as="form"
                        minW="full"
                        align="start"
                        spacing="3"
                        onSubmit={handleSubmit(newTask)}
                    >
                        {" "}
                        <Center w="100%">

                            <Heading as="h4" size="lg" isTruncated>
                                Dodavanje novog zadatka
                            </Heading>
                        </Center>{" "}
                        <Box minW="full">
                            <FormLabel>Naziv zadatka:</FormLabel>
                            <Input
                                {...register("nazivZadatak")}
                                required
                                disabled={isSubmitting}
                                type="text"
                            />
                        </Box>
                        <HStack spacing="3">
                            <Box>
                                <FormLabel>Maksimalni broj bodova:</FormLabel>
                                <Input
                                    {...register("bodovi")}
                                    required
                                    type="number"
                                />
                            </Box>

                            <Box>
                                <FormLabel>Maksimalno vrijeme izvođenja (s):</FormLabel>
                                <Input {...register("maxVrijeme")} type="number" required />
                            </Box>
                        </HStack>
                        <Box minW="full">
                            <FormLabel>Tekst zadatka:</FormLabel>
                            <Textarea
                                {...register("tekstZadatak")}
                                required
                                disabled={isSubmitting}
                                type="text"
                            />
                        </Box>
                        <Box minW="full">
                            <FormLabel>Vidljivost zadatka:</FormLabel>
                            <RadioGroup
                                name="vidljivostZad
                                "
                                defaultValue={vidljivostZad
                                }
                                onChange={(newVidljivost: Vidljivost) => {
                                    setVidljivost(newVidljivost);
                                }}
                            >
                                <Flex justify="space-between" w="full">
                                    <Radio value="privatan"> Privatan </Radio>
                                    <Radio value="javan"> Javan </Radio>
                                </Flex>
                            </RadioGroup>
                        </Box>
                        <HStack spacing="3">
                            <FormLabel minW="50%">Broj testnih primjera:</FormLabel>
                            <Select minW="50%" placeholder="Odaberite broj testnih podataka" value={value} onChange={handleChange}>
                                <option value='1'>1</option>
                                <option value='2'>2</option>
                                <option value='3'>3</option>
                                <option value='4'>4</option>
                                <option value='5'>5</option>
                                <option value='6'>6</option>
                                <option value='7'>7</option>
                                <option value='8'>8</option>
                                <option value='9'>9</option>
                                <option value='10'>10</option>
                                <option value='11'>11</option>
                                <option value='12'>12</option>
                                <option value='13'>13</option>
                                <option value='14'>14</option>
                                <option value='15'>15</option>
                                <option value='16'>16</option>
                                <option value='17'>17</option>
                                <option value='18'>18</option>
                                <option value='19'>19</option>
                                <option value='20'>20</option>
                            </Select>
                        </HStack>
                        {unosi}
                        <PrimaryButton
                            type="submit"
                            isLoading={isSubmitting}
                            loadingText="Dodavanje"
                        >
                            Dodavanje zadatka
                        </PrimaryButton>
                    </VStack>
                </Flex>
            </Center>
        </Flex>
    </>);
}