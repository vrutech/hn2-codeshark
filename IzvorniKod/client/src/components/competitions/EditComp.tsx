import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select,
    Table,
    Thead,
    Link,
    Tbody,
    Td,
    Th,
    Tr,
    Spinner,
    Button
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";


export const EditComp: FC = ({ }) => {

    var [natjecanje, setNatjecanje] = useState([]);
    var [task, setTasks] = useState([]);
    const { push } = useHistory();
    const { id } = useParams();
    const user = getUser();
    var [loading, setLoading] = useState([]);
    var [zadaci, setZadaci] = useState([]);
    const [uploadFile, setUploadFile] = React.useState();

    const getNatjecanje = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/dohvatiNatjecanje/" + id)
            .then((response) => {
                console.log(response.status);
                console.log(response.data);
                setNatjecanje(response.data);
                setLoading(true);
            });
    }

    useEffect(() => getNatjecanje(), []);

    function setUploadFunction() {
        var slika = document.getElementById("slika").files[0];
        var file64 = toBase64(slika).then(data => setUploadFile(data));
    }

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(22));
        reader.onerror = error => reject(error);
    });

    const [greska, setGreska] = useState("");

    const submitForm = async (event) => {
        event.preventDefault();
        const usr = getUser();
        const IdKorisnik = usr.idKorisnik;
        const NazivNatjecanje = document.getElementById("nazivNatjecanje")?.value;
        const VrijemePocetak = document.getElementById("pocetakNatjecanje")?.value;
        const VrijemeKraj = document.getElementById("krajNatjecanje")?.value;
        var PeharSlika;
        if (!uploadFile) {
            PeharSlika = natjecanje.peharSlika
        } else {
            PeharSlika = uploadFile;
        }

        const JeVirtualno = false;
        const podaci = {
            IdKorisnik,
            NazivNatjecanje,
            VrijemePocetak,
            VrijemeKraj,
            PeharSlika
        }
        console.log(podaci);

        try {
            //fix sa backendom
            const response = await axios.post(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/promijeniNatjecanje/" + natjecanje.idNatjecnaje,
                podaci)
            if (response.status === 200) {
                push("/comp/" + natjecanje.idNatjecnaje);
            } else if (response.status === 400) {
                console.log(response);
            }
        } catch (err) {
            /*poruka.push(<>
                <Text color="red" minW="0" fontSize="lg">Zadatak s tim imenom već postoji</Text>
                </>)*/
            setGreska("Natjecanje s tim nazivom već postoji ili je kraj natjecanja prije početka, te sva polja moraju biti popunjena!")
            console.log(err);
        }
    }
    return (<>
        <Box>
            <Center h="50%" w="100%">

                <Box>{!loading ? <Spinner></Spinner> : <></>}
                    {greska !== "" && <Center><Box minw="0" color="red.400" padding="0px"><Text>{greska}</Text></Box></Center>}
                    <br />
                    {natjecanje ? <>
                        <Center>
                            <Heading size="lg">Uređivanje natjecanja: {natjecanje.nazivNatjecanje}</Heading>
                        </Center>
                    </> : <></>}
                    <br />
                    <Center>
                        <Box minW="0" fontSize="lg">
                            <Center>
                                <Text fontWeight="bold">Naziv natjecanja:</Text>
                            </Center>
                            <Input placeholder="Napišite naziv natjecanja..." w="25rem" id="nazivNatjecanje" defaultValue={natjecanje.nazivNatjecanje} required></Input>
                        </Box>
                    </Center>
                    <br />
                    <HStack spacing="10">
                        <Box minW="0" fontSize="lg">
                            <Text fontWeight="bold">Datum i vrijeme početka:</Text>
                            <Input w="25rem" type="datetime-local" id="pocetakNatjecanje" defaultValue={natjecanje.vrijemePocetak} required></Input>
                        </Box>

                        <Box minW="0" fontSize="lg">
                            <Text fontWeight="bold">Datum i vrijeme kraja:</Text>
                            <Input w="25rem" type="datetime-local" id="krajNatjecanje" defaultValue={natjecanje.vrijemeKraj} required></Input>
                        </Box>
                    </HStack>
                    <br />
                    <Box minW="0" fontSize="lg">
                        <Text fontWeight="bold">Stavite svoju sliku pehara:</Text>
                        <Input type="file" id="slika" onChange={(e) => setUploadFunction()} accept="image/png" required />
                    </Box>
                    <br />
                    <Center>
                        <Flex as="form">
                            <Button onClick={submitForm}>Spremanje promjena</Button>
                        </Flex>
                        <Box Box minW="0" fontSize="lg" ml={5}>
                            <Link href={"/comp/" + natjecanje.idNatjecnaje}>
                                <Button>Odustani</Button>
                            </Link>
                        </Box>
                    </Center>
                    <Center w="full" >

                    </Center>
                </Box>
            </Center>
        </Box>
    </>)
}