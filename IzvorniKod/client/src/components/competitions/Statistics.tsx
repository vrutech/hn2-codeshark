import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select,
    Table,
    Thead,
    Link,
    Tbody,
    Td,
    Th,
    Tr,
    Spinner,
    Button
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";
import ReactPaginate from 'react-paginate';

export const Statistics: FC = ({ }) => {

    const { id } = useParams();
    var [statistika, setStatistika] = useState([]);
    var [natjecanje, setNatjecanje] = useState([]);
    const { push } = useHistory();

    const getNatjecanje = async () => {
        try {
            //fix sa backendom
            let response = await axios.get(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/getVirtualnoNatjecanjeIzZavrsenog/" + id)
            if (response.status === 200) {
                let broj = 1;
                /*for (const podaci of response.data) {
                    podaci.statistika.mjesto = broj;
                    broj++;
                }*/
                response.data.statistika.sort(function (a, b) { return b.bodovi - a.bodovi })
                for (let stat of response.data.statistika) {
                    stat.mjesto = broj;
                    broj++;
                }
                setStatistika(response.data.statistika);
                setNatjecanje(response.data);
                console.log(response.data);
            } else if (response.status === 400) {
                console.log(response);
            }
        } catch (err) {
            console.log(err);
        }

    }
    useEffect(() => getNatjecanje(), []);

    return (<>
        <>
            <Box>

                <Center h="50%" w="100%">
                    <Box>
                        {statistika.length === 0 ? (
                            <>
                                {" "}
                                <Heading as="h2" size="lg" isTruncated>
                                    Nema statistike!
                                </Heading>{" "}
                            </>
                        ) : (
                            <>
                                <Center>
                                    <Heading as="h4" size="md" isTruncated>
                                        Rang lista za natjecanje {natjecanje.nazivNatjecanje}:
                                    </Heading>
                                </Center>

                                <Table variant="striped" colorScheme="purple">
                                    <Thead>
                                        <Tr>
                                            <Th>Mjesto</Th>
                                            <Th>Korisničko ime</Th>
                                            <Th>Broj bodova</Th>
                                        </Tr>
                                    </Thead>
                                    <Tbody>
                                        {statistika.map((element) => {
                                            return (
                                                <Tr key={element.username}>
                                                    <Td>{element.mjesto}</Td>
                                                    <Td>{element.username}</Td>
                                                    <Td>{element.bodovi}</Td>
                                                </Tr>
                                            );
                                        })}
                                    </Tbody>
                                </Table>
                            </>
                        )}
                        <Center w="full">
                            <Link href={"/comp/" + natjecanje.idNatjecnaje}>
                                <Button >Povratak na natjecanje</Button>
                            </Link>
                        </Center>
                    </Box>

                </Center>
            </Box>
        </>
    </>)
}