import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select,
    Table,
    Thead,
    Link,
    Tbody,
    Td,
    Th,
    Tr,
    Spinner,
    Button
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";
import ReactPaginate from "react-paginate";

const css = `
        @import "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css";

        #container {
        margin: 1rem;
        }

        .items {
        margin-bottom: 1rem;
        }
        `
const convertTezina = (tezinaNat
    : Tezina) => {
    switch (tezinaNat
    ) {
        case "lagano":
            return 1;
        case "srednje":
            return 2;
        case "tesko":
            return 3;
        default:
            break;
    }
};



type Tezina = "lagano" | "srednje" | "tesko";



export const VirtualComp: FC = ({ }) => {
    var [odabrano, setOdabrano] = useState(false)
    var [natjecanje, setNatjecanje] = useState([]);
    const { push } = useHistory();
    const user = getUser();
    var [loading, setLoading] = useState(Boolean);
    var [zadaci, setZadaci] = useState([]);
    const [uploadFile, setUploadFile] = React.useState();
    var [trenutniZad, setTrenutniZad] = useState();
    var [zavrseno, setZavrseno] = useState(false);
    var [kreceTimer, setKreceTimer] = useState(false);
    var [spinner, setSpinner] = useState(false);
    var [kraj, setKraj] = useState(1);

    const [tezinaNat
        , setTezinaNat] = useState<Tezina>("lagano");

    const submitForm = async (event) => {
        event.preventDefault();
        const tezina = convertTezina(tezinaNat);
        try {
            //fix sa backendom
            const response = await axios.get(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/getRandomVirtualnoNatjecanje/" + tezina)
            if (response.status === 200) {
                setOdabrano(true);
                setNatjecanje(response.data);
                console.log(response.data);
                if (response.data.zadaci) {
                    for (let zad of response.data.zadaci) {
                        zad.dobiveniBodovi = null;
                        zad.vrijemeMS = null;
                    }
                    setZadaci(response.data.zadaci);
                    setCurrentItems(response.data.zadaci.slice(itemOffset, itemOffset + 1));
                    setPageCount(Math.ceil(response.data.zadaci.length / 1));
                    console.log(currentItems);
                    console.log(response.data.zadaci);
                    setKreceTimer(true);
                }
                setLoading(true);
            } else if (response.status === 400) {
                console.log(response);
            }
        } catch (err) {
            console.log(err);
        }

    }

    const [dobiveniBodovi, setDobiveniBodovi] = useState();
    const [vrijemeMS, setVrijemeMS] = useState();

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(26));
        reader.onerror = error => reject(error);
    });

    const submitTask = async (event) => {
        event.preventDefault();
        setSpinner(true);
        var programBase64 = null;
        console.log(trenutniZad + " ovo je trenutni lol");
        try {
            var file = await document.getElementById(trenutniZad).files[0];
        } catch (err) {
            console.log(err);
        }
        const user = getUser();
        const idKorisnik = user.idKorisnik;
        const korisnickoIme = user.korisnickoIme;
        const idZadatak = trenutniZad;
        console.log(idKorisnik);
        console.log(korisnickoIme);
        console.log(idZadatak);

        programBase64 = toBase64(file).then(data => axios
            .post(process.env.REACT_APP_URL_PREFIX + "/Rezultat/predajRjesenje", {
                programBase64: data,
                idKorisnik: idKorisnik,
                korisnickoIme: korisnickoIme,
                idZadatak: idZadatak
            }
            )
            .then((response) => {
                const povratnaPoruka = response.data.povratnaPoruka;
                setDobiveniBodovi(response.data.dobiveniBodovi);
                setVrijemeMS(response.data.vrijemeMS.totalSeconds);
                for (let zad of zadaci) {
                    if (zad.idZadatak === trenutniZad) {
                        if (response.data.dobiveniBodovi === 0) zad.dobiveniBodovi = 0;
                        else zad.dobiveniBodovi = response.data.dobiveniBodovi;
                        zad.vrijemeMS = response.data.vrijemeMS.totalSeconds;
                    }
                }
                /*for (let zad of zadaci) {
                    if (zad.idZadatak === trenutniZad) {
                        zad.dobiveniBodovi = 1;
                        zad.vrijemeMS = 0.23;
                    }
                }*/
                document.getElementById(trenutniZad).value = null;
                setSpinner(false);
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error);
                setSpinner(false);
                document.getElementById(trenutniZad).value = null;
            }))

    }

    var [rezultati, setRezultati] = useState([]);

    const submitComp = async (event) => {
        //event.preventDefault();
        const preostaloVrijeme = timeLeft;
        console.log(preostaloVrijeme);
        var ukupnoBodova = 0;
        for (const zad of zadaci) {
            if (zad.dobiveniBodovi)
                ukupnoBodova += zad.dobiveniBodovi
        }
        var maksBodova = 0;
        for (const zad of zadaci) {
            maksBodova += zad.bodovi;
        }
        var dodatniBodovi = 0;
        if (maksBodova === ukupnoBodova) {
            dodatniBodovi = (preostaloVrijeme.dana * 86400000 + preostaloVrijeme.sati * 3600000
                + preostaloVrijeme.minuta * 60000 + preostaloVrijeme.sekundi * 1000) * 0.000001;
        }
        const ukupnoPlusDodatno = ukupnoBodova + dodatniBodovi;
        rezultati.push(ukupnoBodova);
        rezultati.push(maksBodova);
        rezultati.push(dodatniBodovi);
        rezultati.push(ukupnoPlusDodatno);
        //setRezultati({ukupnoBodova, dodatniBodovi, ukupnoPlusDodatno})
        setZavrseno(true);
    }
    const submitComp2 = () => {
        //event.preventDefault();
        const preostaloVrijeme = timeLeft;
        console.log(preostaloVrijeme);
        var ukupnoBodova = 0;
        for (const zad of zadaci) {
            if (zad.dobiveniBodovi)
                ukupnoBodova += zad.dobiveniBodovi
        }
        var maksBodova = 0;
        for (const zad of zadaci) {
            maksBodova += zad.bodovi;
        }
        var dodatniBodovi = 0;
        if (maksBodova === ukupnoBodova) {
            dodatniBodovi = (preostaloVrijeme.dana * 86400000 + preostaloVrijeme.sati * 3600000
                + preostaloVrijeme.minuta * 60000 + preostaloVrijeme.sekundi * 1000) * 0.000001;
        }
        const ukupnoPlusDodatno = ukupnoBodova + dodatniBodovi;
        rezultati.push(ukupnoBodova);
        rezultati.push(maksBodova);
        rezultati.push(dodatniBodovi);
        rezultati.push(ukupnoPlusDodatno);
        //setRezultati({ukupnoBodova, dodatniBodovi, ukupnoPlusDodatno})
        setZavrseno(true);
    }

    //stranicenje
    const [currentItems, setCurrentItems] = useState(null);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        const endOffset = itemOffset + 1;
        console.log(`Loading items from ${itemOffset} to ${endOffset}`);
        setCurrentItems(zadaci.slice(itemOffset, endOffset));
        console.log(currentItems);
        setPageCount(Math.ceil(zadaci.length / 1));
    }, [itemOffset, 1]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * 1) % zadaci.length;
        console.log(
            `User requested page number ${event.selected}, which is offset ${newOffset}`
        );
        setItemOffset(newOffset);
    };

    //timer
    const calculateTimeLeft = () => {
        const difference = +new Date(`${natjecanje.vrijemeKraj}`) - +new Date();
        let timeLeft = {};

        if (difference > 0) {
            timeLeft = {
                dana: Math.floor(difference / (1000 * 60 * 60 * 24)),
                sati: Math.floor((difference / (1000 * 60 * 60)) % 24),
                minuta: Math.floor((difference / 1000 / 60) % 60),
                sekundi: Math.floor((difference / 1000) % 60),
            };
        }

        return timeLeft;
    };
    const calculateTimeLeft2 = () => {
        const difference = +new Date(`${natjecanje.vrijemeKraj}`) - +new Date();
        let timeLeft = {};

        return difference;
    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
    const [year] = useState(new Date().getFullYear());

    useEffect(() => {
        const timer = setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
            setKraj(calculateTimeLeft2());
        }, 1000);

        return () => clearTimeout(timer);
    });

    const timerComponents = [];

    Object.keys(timeLeft).forEach((interval) => {
        if (!timeLeft[interval]) {


        }

        timerComponents.push(
            <span>
                {timeLeft[interval]} {interval}{" "}
            </span>
        );
    });

    return (<>
        <Center w="100%">

            <VStack
                align="start"
                minW={{
                    base: "100vw",
                    md: "300px",
                }}
                w="full"
                maxW={{
                    base: "100vw",
                    md: "800px",
                }}
                minH={{
                    base: "100vh",
                    md: "200px",
                }}
                h="full"
                maxH={{
                    base: "100vh",
                    md: "650px",
                }}
                bg="white"
                boxShadow={{
                    base: "none",
                    md: "lg",
                }}
                borderRadius={{
                    base: "none",
                    md: "lg",
                }}
                mx="auto"
                p="6"
                spacing="6"
            >
                {zavrseno ? <>
                    <Flex justify="space-between" minW="full">
                        <Center w="full">
                            <VStack spacing="2" align="start" minW="full">
                                <Center w="full">
                                    <Flex minW="0">
                                        <Text fontSize="3xl" fontWeight="bold">
                                            Rezultati natjecanja
                                        </Text>
                                    </Flex>
                                </Center>
                                <Center w="full">
                                    <Box minW="0" fontSize="lg" fontWeight="bold">
                                        <Center>
                                            <Text>Broj bodova: {rezultati[0]} / {rezultati[1]}</Text>
                                        </Center>
                                    </Box>
                                </Center>
                                <Center w="full">
                                    <Text fontSize="lg" fontWeight="bold">Broj dodatnih bodova: {rezultati[2]}</Text>
                                </Center>
                                <Center w="full">
                                    <Text fontSize="lg" fontWeight="bold">Ukupan broj bodova: {rezultati[3]}</Text>
                                </Center>
                            </VStack>
                        </Center>
                    </Flex>
                    <Center w="full">
                        <Flex as="form">
                            <Link href="/landing-page">
                                <Button>
                                    Povratak na listu natjecanja
                                </Button>
                            </Link>
                        </Flex>
                    </Center>
                </> : <></>}

                {!odabrano ? (<>
                    <Flex justify="space-between" minW="full">
                        <Center w="full">
                            <VStack as="form" spacing="6" align="start" minW="full">
                                <Center w="full">
                                    <Flex minW="0">
                                        <Text fontSize="3xl" fontWeight="bold">
                                            Novo virtualno natjecanje
                                        </Text>
                                    </Flex>
                                </Center>
                                <Center w="full">
                                    <Box >
                                        <Text fontSize="lg" fontWeight="bold">Težina virtualnog natjecanja:</Text>
                                        <RadioGroup
                                            name="vidljivostZad
                                    " w="15rem"
                                            defaultValue={tezinaNat
                                            }
                                            onChange={(newTezina: Tezina) => {
                                                setTezinaNat(newTezina);
                                            }}
                                        >
                                            <Flex justify="space-between" w="full">
                                                <Radio value="lagano">Lagano</Radio>
                                                <Radio value="srednje">Srednje</Radio>
                                                <Radio value="tesko">Teško</Radio>
                                            </Flex>
                                        </RadioGroup>
                                    </Box>
                                </Center>
                                <Center w="full">
                                    <Box>
                                        <Flex as="form">
                                            <Button onClick={submitForm}>Kreiraj natjecanje</Button>
                                        </Flex>
                                    </Box>
                                </Center>
                                <Box>
                                    <Text fontSize="lg">
                                        Trajanje laganog natjecanja: pola sata
                                    </Text>
                                    <Text fontSize="lg">
                                        Trajanje srednjeg natjecanja: 1 sat
                                    </Text>
                                    <Text fontSize="lg">
                                        Trajanje teškog natjecanja: sat i pol
                                    </Text>
                                </Box>
                            </VStack>
                        </Center>
                    </Flex>

                </>) : <></>}
                {odabrano && !zavrseno ?
                    (<>
                        <Flex justify="space-between" minW="full">
                            <Center w="full">
                                <VStack spacing="2" align="start" minW="full">
                                    <Center w="full">
                                        <Box minW="0" fontSize="lg" fontWeight="bold">
                                            <Center>
                                                <Text>Preostalo vrijeme: </Text>
                                            </Center>
                                            {kraj < 0 ? <>{submitComp2()}</> : <></>}
                                            {timerComponents.length ? timerComponents : <Text>Vrijeme prošlo!</Text>}
                                        </Box>
                                    </Center>
                                    <Center w="full">
                                        <Flex minW="0">
                                            <Text fontSize="3xl" fontWeight="bold">
                                                Natjecanje: {natjecanje.nazivNatjecnaje}
                                            </Text>
                                        </Flex>
                                    </Center>
                                    <Center w="full">
                                        <Text fontSize="lg" fontWeight="bold">Broj zadataka: {zadaci?.length}</Text>
                                    </Center>
                                </VStack>
                            </Center>
                        </Flex>
                        <Center w="full">
                            <Flex as="form">
                                <Button onClick={submitComp}>Predaj rješenja</Button>
                            </Flex>
                        </Center>
                    </>) : <></>}
            </VStack>
        </Center>
        {odabrano && !zavrseno ? (<>
            <Box>
                {" "}
                {" "}
            </Box>
            <Box minW="0" fontSize="lg">
                <HStack>
                    <style>
                        {css}
                    </style>
                    {zadaci.length === 0 ? <></> : (<>
                        <Center w="full">
                            <ReactPaginate
                                breakLabel="..."
                                nextLabel="Sljedeći"
                                onPageChange={handlePageClick}
                                pageRangeDisplayed={10}
                                pageCount={pageCount}
                                previousLabel="Prethodni"
                                pageClassName="page-item"
                                pageLinkClassName="page-link"
                                previousClassName="page-item"
                                previousLinkClassName="page-link"
                                nextClassName="page-item"
                                nextLinkClassName="page-link"
                                breakClassName="page-item"
                                breakLinkClassName="page-link"
                                containerClassName="pagination"
                                activeClassName="active"
                            />
                        </Center>
                    </>)}
                </HStack>
            </Box>
            <VStack
                align="start"
                minW={{
                    base: "100vw",
                    md: "300px",
                }}
                w="full"
                maxW={{
                    base: "100vw",
                    md: "800px",
                }}
                minH={{
                    base: "100vh",
                    md: "250px",
                }}
                h="full"
                bg="white"
                boxShadow={{
                    base: "none",
                    md: "lg",
                }}
                borderRadius={{
                    base: "none",
                    md: "lg",
                }}
                mx="auto"
                p="6"
                spacing="6"
            >
                <Flex justify="space-between" minW="full"></Flex>

                {!currentItems ? <></> : (<>
                    {currentItems.map((element) => {
                        return (
                            <Flex justify="space-between" minW="full" key={element.idKorisnik}>
                                <Center w="full">
                                    <VStack spacing="0" align="start" minW="full">
                                        <Center w="full">
                                            <Flex minW="0">
                                                <Text fontSize="2xl" fontWeight="bold">
                                                    Zadatak: {element?.nazivZadatak}
                                                </Text>
                                            </Flex>
                                        </Center>

                                        <Box minW="0" fontSize="lg">
                                            <Text fontWeight="bold">Id zadatka:</Text>
                                            <Text>{element?.idZadatak}</Text>
                                        </Box>

                                        <Box minW="0" fontSize="lg">
                                            <Text fontWeight="bold">Maksimalni bodovi:</Text>
                                            <Text> {element?.bodovi}</Text>
                                        </Box>
                                        <Box minW="0" fontSize="lg">
                                            <Text fontWeight="bold">Vrijeme izvođenja u sekundama:</Text>
                                            <Text>{element?.maxVrijeme.totalSeconds}</Text>
                                        </Box>

                                        <Box minW="0" fontSize="lg">
                                            <Text fontWeight="bold">Tekst zadatka:</Text>
                                            <Text>{element?.tekstZadatak}</Text>
                                        </Box>
                                        <Flex as="form">
                                            <Input type="file" id={element?.idZadatak} onChange={(e) => {
                                                setTrenutniZad(element?.idZadatak)
                                                setUploadFile(e.target.files)
                                            }} />
                                            <Button onClick={submitTask}>Provjera rješenja</Button>
                                        </Flex>
                                        {spinner ? <Spinner></Spinner> : <></>}
                                        {element.dobiveniBodovi !== null ? (<Text fontSize="md" fontWeight="bold">Dobiveni bodovi: {element.dobiveniBodovi} / {element?.bodovi}</Text>) : (<></>)}
                                        {element.vrijemeMS ? (<Text fontSize="md" fontWeight="bold" >Vrijeme izvođenja: {element.vrijemeMS} sekundi</Text>) : (<></>)}
                                    </VStack>
                                </Center>
                            </Flex>)
                    })}</>
                )}
            </VStack>
        </>) : <></>}

    </>)

}