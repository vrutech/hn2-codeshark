import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select,
    Table,
    Thead,
    Link,
    Tbody,
    Td,
    Th,
    Tr,
    Spinner,
    Button
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";


export const NewComp: FC = ({ }) => {

    var [task, setTasks] = useState([]);
    const { push } = useHistory();
    const user = getUser();
    var [loading, setLoading] = useState([]);
    var [zadaci, setZadaci] = useState([]);
    const [uploadFile, setUploadFile] = React.useState();

    const getTasks = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Korisnik/voditeljeviNapravljeniZadaci/" + user.idKorisnik)
            .then((response) => {
                console.log(response.status);
                console.log(response.data);
                const myTasks = response.data;
                console.log(response.data);
                var usrTasks = []
                for (const zad of myTasks) {
                    if (!zad.vidljivost) {
                        usrTasks.push(zad)
                    }
                }

                setTasks(usrTasks);
                setLoading(true);
            });
    }

    useEffect(() => getTasks(), []);

    function chooseTask(data) {
        setTasks(task.filter(task => task.idZadatak !== data.idZadatak))
        zadaci.push(data);
        console.log(zadaci);
    }

    function removeTask(data) {
        setZadaci(zadaci.filter(zadaci => zadaci.idZadatak !== data.idZadatak));
        task.push(data);
        console.log(zadaci)
    }
    function setUploadFunction() {
        var slika = document.getElementById("slika").files[0];
        var file64 = toBase64(slika).then(data => setUploadFile(data));
    }

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(22));
        reader.onerror = error => reject(error);
    });

    const [greska, setGreska] = useState([]);

    const submitForm = async (event) => {
        event.preventDefault();
        const usr = getUser();
        const IdKorisnik = usr.idKorisnik;
        const NazivNatjecanje = document.getElementById("nazivNatjecanje")?.value;
        const VrijemePocetak = document.getElementById("pocetakNatjecanje")?.value;
        const VrijemeKraj = document.getElementById("krajNatjecanje")?.value;
        const PeharSlika = uploadFile;
        const JeVirtualno = false;
        var ListaZadataka = []
        console.log(zadaci)
        for (const zad of zadaci) {
            //ListaZadataka.push({IdZadataK: zad.idZadatak})
            ListaZadataka.push(zad.idZadatak)
        }
        const podaci = {
            IdKorisnik,
            NazivNatjecanje,
            VrijemePocetak,
            VrijemeKraj,
            PeharSlika,
            JeVirtualno,
            ListaZadataka
        }
        console.log(podaci);

        try {
            //fix sa backendom
            const response = await axios.post(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/novoNatjecanje",
                podaci)
            if (response.status === 200) {
                push("/landing-page");
            } else if (response.status === 400) {
                console.log(response);
            }
        } catch (err) {
            /*poruka.push(<>
                <Text color="red" minW="0" fontSize="lg">Zadatak s tim imenom već postoji</Text>
                </>)*/
            setGreska("Natjecanje s tim nazivom već postoji ili je kraj natjecanja prije početka")
            console.log(err);
        }
    }

    return (<>
        <Box>
            <Center h="50%" w="100%">
                <Box>{!loading ? <Spinner></Spinner> : <></>}
                    {greska !== null && <Center><Box minw="0" color="red.400" padding="0px"><Text>{greska}</Text></Box></Center>}
                    <br />
                    <Center>
                        <Box minW="0" fontSize="lg">
                            <Center>
                                <Text fontWeight="bold">Naziv natjecanja:</Text>
                            </Center>
                            <Input placeholder="Napišite naziv natjecanja..." w="25rem" id="nazivNatjecanje" required></Input>
                        </Box>
                    </Center>
                    <br />
                    <HStack spacing="10">
                        <Box minW="0" fontSize="lg">
                            <Text fontWeight="bold">Datum i vrijeme početka:</Text>
                            <Input w="25rem" type="datetime-local" id="pocetakNatjecanje" required></Input>
                        </Box>

                        <Box minW="0" fontSize="lg">
                            <Text fontWeight="bold">Datum i vrijeme kraja:</Text>
                            <Input w="25rem" type="datetime-local" id="krajNatjecanje" required></Input>
                        </Box>
                    </HStack>
                    <br />
                    <Box minW="0" fontSize="lg">
                        <Text fontWeight="bold">Stavite svoju sliku pehara:</Text>
                        <Input type="file" id="slika" onChange={(e) => setUploadFunction()} accept="image/png" required />
                    </Box>
                    <br />
                    {zadaci.length === 0 ? <></> : (<>
                        <Center>
                            <Heading as="h4" size="md" isTruncated>
                                Odabrani zadaci:
                            </Heading>
                        </Center>
                        <Table variant="striped" colorScheme="purple">
                            <Thead>
                                <Tr>
                                    <Th>Naziv zadatka</Th>
                                    <Th>Broj bodova</Th>
                                    <Th>Vrijeme izvođenja</Th>
                                    <Th></Th>
                                    <Th></Th>
                                </Tr>
                            </Thead>
                            <Tbody>
                                {zadaci.map((element) => {
                                    return (
                                        <Tr key={element.idZadatak}>
                                            <Td>{element.nazivZadatak}</Td>
                                            <Td>{element.bodovi}</Td>
                                            <Td>{element.maxVrijeme.totalSeconds}</Td>
                                            <Td>
                                                <VerifyButton
                                                    colorScheme="red"
                                                    onClick={() => removeTask(element)}
                                                    value={element.idZadatak}
                                                >
                                                    Obriši zadatak
                                                </VerifyButton>
                                            </Td>

                                        </Tr>
                                    );
                                }
                                )}
                            </Tbody>
                        </Table>
                        <br />
                        <Center>
                            <Flex as="form">
                                <Button onClick={submitForm}>Kreiraj natjecanje</Button>
                            </Flex>
                        </Center>
                        <br />
                    </>)}
                    {task.length === 0 ? (
                        <>
                            {" "}
                            <Center>
                                <Heading as="h2" size="lg" isTruncated>
                                    Nema privatnih zadataka!
                                </Heading>{" "}
                            </Center>
                        </>
                    ) : (
                        <>
                            <Center>
                                <Heading as="h4" size="md" isTruncated>
                                    Odaberite zadatke za natjecanje:
                                </Heading>
                            </Center>

                            <Table variant="striped" colorScheme="purple">
                                <Thead>
                                    <Tr>
                                        <Th>Naziv zadatka</Th>
                                        <Th>Broj bodova</Th>
                                        <Th>Vrijeme izvođenja</Th>
                                        <Th></Th>
                                        <Th></Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    {task.map((element) => {
                                        if (element.idZadatak in zadaci) { } else {
                                            return (
                                                <Tr key={element.idZadatak}>
                                                    <Td>{element.nazivZadatak}</Td>
                                                    <Td>{element.bodovi}</Td>
                                                    <Td>{element.maxVrijeme.totalSeconds}</Td>
                                                    <Td>
                                                        <VerifyButton
                                                            colorScheme="facebook"
                                                            onClick={() => chooseTask(element)}
                                                            value={element.idZadatak}
                                                        >
                                                            Dodaj zadatak
                                                        </VerifyButton>
                                                    </Td>

                                                </Tr>
                                            );
                                        }
                                    })}
                                </Tbody>
                            </Table>
                        </>
                    )}
                </Box>
            </Center>
        </Box>
    </>)
}