import {
    Box,
    Flex,
    FormLabel,
    HStack,
    IconButton,
    Input,
    Radio,
    RadioGroup,
    Text,
    VStack,
    Center,
    Textarea,
    Heading,
    Select,
    Table,
    Thead,
    Link,
    Tbody,
    Td,
    Th,
    Tr,
    Spinner,
    Button
} from "@chakra-ui/react";
import React, { FC, useState, useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { MdClose } from "react-icons/md";
import { useHistory, useParams } from "react-router";
import { PrimaryButton, SecondaryButton, VerifyButton } from "../shared/Buttons";
import axios from "axios";
import { getUser } from "../shared/Utils";
import ReactPaginate from 'react-paginate';

const css = `
        @import "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css";

        #container {
        margin: 1rem;
        }

        .items {
        margin-bottom: 1rem;
        }
        `
export const CompView: FC = ({ }) => {
    const { id } = useParams();

    var [natjecanje, setNatjecanje] = useState([]);
    const { push } = useHistory();
    const user = getUser();
    var [loading, setLoading] = useState(Boolean);
    var [zadaci, setZadaci] = useState([]);
    const [uploadFile, setUploadFile] = React.useState();
    var [trenutniZad, setTrenutniZad] = useState();
    var [spinner, setSpinner] = useState(false);
    var [kraj, setKraj] = useState(1);

    const getNatjecanje = () => {
        axios
            .get(process.env.REACT_APP_URL_PREFIX + "/Natjecanje/dohvatiNatjecanje/" + id)
            .then((response) => {
                console.log(response.status);
                console.log(response.data);
                setNatjecanje(response.data);
                if (response.data.zadaci) {
                    for (let zad of response.data.zadaci) {
                        zad.dobiveniBodovi = null;
                        zad.vrijemeMS = null;
                    }
                    setZadaci(response.data.zadaci);
                    setCurrentItems(response.data.zadaci.slice(itemOffset, itemOffset + 1));
                    setPageCount(Math.ceil(response.data.zadaci.length / 1));
                    console.log(currentItems);
                    console.log(response.data.zadaci);
                }
                setLoading(true);
            });
    }

    useEffect(() => getNatjecanje(), []);

    const [dobiveniBodovi, setDobiveniBodovi] = useState();
    const [vrijemeMS, setVrijemeMS] = useState();

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result?.slice(26));
        reader.onerror = error => reject(error);
    });

    const submitForm = async (event) => {
        event.preventDefault();
        setSpinner(true);
        var programBase64 = null;
        console.log(trenutniZad + " ovo je trenutni lol");
        try {
            var file = await document.getElementById(trenutniZad).files[0];
        } catch (err) {
            console.log(err);
        }
        const user = getUser();
        const idKorisnik = user.idKorisnik;
        const korisnickoIme = user.korisnickoIme;
        const idZadatak = trenutniZad;
        console.log(idKorisnik);
        console.log(korisnickoIme);
        console.log(idZadatak);

        programBase64 = toBase64(file).then(data => axios
            .post(process.env.REACT_APP_URL_PREFIX + "/Rezultat/predajRjesenje", {
                programBase64: data,
                idKorisnik: idKorisnik,
                korisnickoIme: korisnickoIme,
                idZadatak: idZadatak,
                idNatjecanje: natjecanje.idNatjecnaje
            }
            )
            .then((response) => {
                const povratnaPoruka = response.data.povratnaPoruka;
                setDobiveniBodovi(response.data.dobiveniBodovi);
                setVrijemeMS(response.data.vrijemeMS.totalSeconds);
                for (let zad of zadaci) {
                    if (zad.idZadatak === trenutniZad) {
                        if (response.data.dobiveniBodovi === 0) zad.dobiveniBodovi = 0;
                        else zad.dobiveniBodovi = response.data.dobiveniBodovi;
                        zad.vrijemeMS = response.data.vrijemeMS.totalSeconds;
                    }
                }
                document.getElementById(trenutniZad).value = null;
                setSpinner(false);
                console.log(response.data)
            })
            .catch((error) => {
                console.log(error)
                setSpinner(false);
                document.getElementById(trenutniZad).value = null;
            }))

    }


    //stranicenje
    const [currentItems, setCurrentItems] = useState(null);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);

    useEffect(() => {
        const endOffset = itemOffset + 1;
        console.log(`Loading items from ${itemOffset} to ${endOffset}`);
        setCurrentItems(zadaci.slice(itemOffset, endOffset));
        console.log(currentItems);
        setPageCount(Math.ceil(zadaci.length / 1));
    }, [itemOffset, 1]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * 1) % zadaci.length;
        console.log(
            `User requested page number ${event.selected}, which is offset ${newOffset}`
        );
        setItemOffset(newOffset);
    };

    //timer
    const calculateTimeLeft = () => {
        const difference = +new Date(`${natjecanje.vrijemeKraj}`) - +new Date();
        let timeLeft = {};

        if (difference > 0) {
            timeLeft = {
                "dan(a)": Math.floor(difference / (1000 * 60 * 60 * 24)),
                "sat(i)": Math.floor((difference / (1000 * 60 * 60)) % 24),
                minuta: Math.floor((difference / 1000 / 60) % 60),
                sekundi: Math.floor((difference / 1000) % 60),
            };
        }

        return timeLeft;
    };

    const calculateTimeLeft2 = () => {
        const difference = +new Date(`${natjecanje.vrijemeKraj}`) - +new Date();
        let timeLeft = {};

        return difference;
    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
    const [year] = useState(new Date().getFullYear());

    useEffect(() => {
        const timer = setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
            setKraj(calculateTimeLeft2());
        }, 1000);

        return () => clearTimeout(timer);
    });

    const timerComponents = [];

    Object.keys(timeLeft).forEach((interval) => {
        if (!timeLeft[interval]) {

        }

        timerComponents.push(
            <span>
                {timeLeft[interval]} {interval}{" "}
            </span>
        );
    });

    const endComp = () => {
        push('/comp/' + natjecanje.idNatjecnaje)
    }

    return (<>
        <Center w="100%">
            <VStack
                align="start"
                minW={{
                    base: "100vw",
                    md: "300px",
                }}
                w="full"
                maxW={{
                    base: "100vw",
                    md: "800px",
                }}
                minH={{
                    base: "100vh",
                    md: "200px",
                }}
                h="full"
                bg="white"
                boxShadow={{
                    base: "none",
                    md: "lg",
                }}
                borderRadius={{
                    base: "none",
                    md: "lg",
                }}
                mx="auto"
                p="6"
                spacing="6"
            >
                {natjecanje.stanje === 2 ? (<>
                    <Center w="full">
                        <Box minW="0" fontSize="lg" fontWeight="bold" spacing="0">
                            {kraj < 0 ? <>{endComp()}</> : <></>}
                            {timerComponents.length ? timerComponents : <Text>Vrijeme prošlo!</Text>}
                        </Box>
                    </Center>
                </>) : <></>}
                <Flex justify="space-between" minW="full">

                    <Center w="full">
                        <VStack spacing="6" align="start" minW="full">
                            <Center w="full">
                                <Flex minW="0">
                                    <Text fontSize="3xl" fontWeight="bold">
                                        Natjecanje: {natjecanje.nazivNatjecanje}
                                    </Text>
                                </Flex>
                            </Center>
                            {loading === true ? <></> : <>
                                <Center w="full">
                                    <Spinner></Spinner>
                                </Center>
                            </>}
                            {natjecanje.stanje === 1 ?
                                (<>
                                    <Center w="full">
                                        <Box minW="0" fontSize="lg">
                                            <Center w="full">
                                                <Text fontWeight="bold">Natjecanje još nije počelo...</Text>
                                            </Center>
                                            <Text fontWeight="bold">Natjecanje počinje: {natjecanje.vrijemePocetak.slice(8, 10) + "."
                                                + natjecanje.vrijemePocetak.slice(5, 7) + "." + natjecanje.vrijemePocetak.slice(0, 4)
                                                + ". u " + natjecanje.vrijemePocetak.slice(11, 16) + " sati"}</Text>
                                        </Box>
                                    </Center>
                                    <Center w="full">
                                        <Box Box minW="0" fontSize="lg">
                                            <Link href="/calendar">
                                                <Button fontWeight="bold">Povratak na kalendar natjecanja</Button>
                                            </Link>
                                        </Box>
                                    </Center>
                                    {user.idKorisnik === natjecanje.idKreatora || user.idPrivilegija === 1 ? <>
                                        <Center w="full">
                                            <Box Box minW="0" fontSize="lg">
                                                <Link href={"/editcomp/" + natjecanje.idNatjecnaje}>
                                                    <Button fontWeight="bold">Uređivanje natjecanja</Button>
                                                </Link>
                                            </Box>
                                        </Center>
                                    </> : <></>}
                                </>)
                                : <></>}
                            {natjecanje.stanje === 2 ?
                                (<>
                                    <Center w="full">
                                        <Box minW="0" fontSize="lg">
                                            <Center w="full">
                                                <Text fontWeight="bold">Natjecanje je u tijeku!</Text>
                                            </Center>
                                            <Text fontWeight="bold">Natjecanje traje do: {natjecanje.vrijemeKraj.slice(8, 10) + "."
                                                + natjecanje.vrijemeKraj.slice(5, 7) + "." + natjecanje.vrijemeKraj.slice(0, 4)
                                                + ". do " + natjecanje.vrijemeKraj.slice(11, 16) + " sati"}</Text>
                                        </Box>
                                    </Center>
                                </>)
                                : <></>}
                            {natjecanje.stanje === 3 ?
                                (<>
                                    <Center w="full">
                                        <Box minW="0" fontSize="lg">
                                            <Center w="full">
                                                <Text fontWeight="bold">Natjecanje je završilo!</Text>
                                            </Center>
                                            <Text fontWeight="bold">Natjecanje je trajalo do: {natjecanje.vrijemeKraj.slice(8, 10) + "."
                                                + natjecanje.vrijemeKraj.slice(5, 7) + "." + natjecanje.vrijemeKraj.slice(0, 4)
                                                + ". do " + natjecanje.vrijemeKraj.slice(11, 16) + " sati"}</Text>
                                        </Box>
                                    </Center>
                                    <Center w="full" >
                                        <Box Box minW="0" fontSize="lg">
                                            <Link href={"/ranglista/" + natjecanje.idNatjecnaje}>
                                                <Button fontWeight="bold">Rang lista natjecanja</Button>
                                            </Link>
                                        </Box>
                                    </Center>
                                    <Center w="full">
                                        <Box Box minW="0" fontSize="lg">
                                            <Link href="/calendar">
                                                <Button fontWeight="bold">Povratak na kalendar natjecanja</Button>
                                            </Link>
                                        </Box>
                                    </Center>
                                    <Center w="full">
                                        <Box minW="0" fontSize="lg">
                                            <Link href={"/oldvirtualcomp/" + natjecanje.idNatjecnaje}>
                                                <Button href="/" fontWeight="bold">Ponovno rješavanje natjecanja</Button>
                                            </Link>
                                        </Box>
                                    </Center>
                                </>)
                                : <></>}
                        </VStack>
                    </Center>
                </Flex>
            </VStack>
        </Center>
        <Box>
            {" "}
            {" "}
        </Box>
        <Box minW="0" fontSize="lg">
            <HStack>
                <style>
                    {css}
                </style>
                {zadaci.length === 0 ? <></> : (<>
                    <Center w="full">
                        <ReactPaginate
                            breakLabel="..."
                            nextLabel="Sljedeći"
                            onPageChange={handlePageClick}
                            pageRangeDisplayed={10}
                            pageCount={pageCount}
                            previousLabel="Prethodni"
                            pageClassName="page-item"
                            pageLinkClassName="page-link"
                            previousClassName="page-item"
                            previousLinkClassName="page-link"
                            nextClassName="page-item"
                            nextLinkClassName="page-link"
                            breakClassName="page-item"
                            breakLinkClassName="page-link"
                            containerClassName="pagination"
                            activeClassName="active"
                        />
                    </Center>
                </>)}
            </HStack>
        </Box>
        <VStack
            align="start"
            minW={{
                base: "100vw",
                md: "300px",
            }}
            w="full"
            maxW={{
                base: "100vw",
                md: "800px",
            }}
            minH={{
                base: "100vh",
                md: "250px",
            }}
            h="full"
            bg="white"
            boxShadow={{
                base: "none",
                md: "lg",
            }}
            borderRadius={{
                base: "none",
                md: "lg",
            }}
            mx="auto"
            p="6"
            spacing="6"
        >
            <Flex justify="space-between" minW="full"></Flex>

            {!currentItems ? <></> : (<>
                {currentItems.map((element) => {
                    return (
                        <Flex justify="space-between" minW="full" key={element.idKorisnik}>
                            <Center w="full">
                                <VStack spacing="0" align="start" minW="full">
                                    <Center w="full">
                                        <Flex minW="0">
                                            <Text fontSize="2xl" fontWeight="bold">
                                                Zadatak: {element?.nazivZadatak}
                                            </Text>
                                        </Flex>
                                    </Center>

                                    <Box minW="0" fontSize="lg">
                                        <Text fontWeight="bold">Id zadatka:</Text>
                                        <Text>{element?.idZadatak}</Text>
                                    </Box>

                                    <Box minW="0" fontSize="lg">
                                        <Text fontWeight="bold">Maksimalni bodovi:</Text>
                                        <Text> {element?.bodovi}</Text>
                                    </Box>
                                    <Box minW="0" fontSize="lg">
                                        <Text fontWeight="bold">Vrijeme izvođenja u sekundama:</Text>
                                        <Text>{element?.maxVrijeme.totalSeconds}</Text>
                                    </Box>

                                    <Box minW="0" fontSize="lg">
                                        <Text fontWeight="bold">Tekst zadatka:</Text>
                                        <Text>{element?.tekstZadatak}</Text>
                                    </Box>
                                    <Flex as="form">
                                        <Input type="file" id={element?.idZadatak} onChange={(e) => {
                                            setTrenutniZad(element?.idZadatak)
                                            setUploadFile(e.target.files)
                                        }} />
                                        <Button onClick={submitForm}>Provjera rješenja</Button>
                                    </Flex>
                                    {spinner ? <Spinner></Spinner> : <></>}
                                    {element.dobiveniBodovi !== null ? (<Text fontSize="md" fontWeight="bold">Dobiveni bodovi: {element.dobiveniBodovi} / {element?.bodovi}</Text>) : (<></>)}
                                    {element.vrijemeMS ? (<Text fontSize="md" fontWeight="bold" >Vrijeme izvođenja: {element.vrijemeMS} sekundi</Text>) : (<></>)}
                                </VStack>
                            </Center>
                        </Flex>)
                })}</>
            )}
        </VStack>
    </>)
}