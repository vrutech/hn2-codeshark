import { Text, Flex, Grid, Td } from "@chakra-ui/react";
import axios from "axios";
import { VerifyButton } from "components/shared/Buttons";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { getUser } from "../shared/Utils";
const user = getUser();

export interface Zadatak {
  idZadatak: number;
  nazivZadatak: string;
  bodovi: number;
  tekstZadatak: string;
  vidljivost: boolean;
}

export const TaskList = () => {
  const [zadaci, setZadaci] = useState<Zadatak[]>([]);

  useEffect(() => {
    const getUserData = async () => {
      const response = await axios.get<Zadatak[]>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/voditeljeviNapravljeniZadaci/" + user.idKorisnik
      );

      setZadaci(response.data);
    };

    getUserData();
  }, []);

  const { push } = useHistory();

  function chooseTask(id) {
    push('/task/' + id);
  }
  function editTask(id) {
    push('/task/edit/' + id);
  }

  return (
    <Flex direction="column" bg="white" borderRadius="md" mx="3">
      <Grid
        templateColumns="repeat(5, 1fr)"
        gap="1"
        placeItems="center"
        fontWeight="bold"
        fontSize="xl"
      >
        <Text>Naziv zadatka</Text>
        <Text>Bodovi</Text>
        <Text>Tekst zadatka</Text>
      </Grid>
      {zadaci?.map((zadatak) => (
        <Grid
          key={zadatak.idZadatak}
          templateColumns="repeat(5, 1fr)"
          gap="1"
          placeItems="center"
          p="1"
        >
          <Text>{zadatak.nazivZadatak}</Text>
          <Text>{zadatak.bodovi}</Text>
          <Text>{zadatak.tekstZadatak}</Text>
          <VerifyButton
              colorScheme="facebook"
              onClick={() => chooseTask(zadatak.idZadatak)}
              value={zadatak.idZadatak}
          >
              Odaberi zadatak
          </VerifyButton>
          <VerifyButton
              colorScheme="facebook"
              onClick={() => editTask(zadatak.idZadatak)}
              value={zadatak.idZadatak}
          >
              Uređivanje
          </VerifyButton>
        </Grid>
      ))}
    </Flex>
  );
};