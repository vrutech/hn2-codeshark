import {
  Avatar,
  Box,
  Circle,
  Flex,
  Input,
  Spinner,
  Text,
  VStack,
} from "@chakra-ui/react";
import axios from "axios";
import { getUser, privilegijaMap } from "components/shared/Utils";
import React, {
  ChangeEventHandler,
  FC,
  useEffect,
  useRef,
  useState,
} from "react";
import "react-calendar/dist/Calendar.css";
import { TaskList } from "./TaskList";

interface User {
  korisnickoIme: string;
  ime: string;
  prezime: string;
  email: string;
  profilSlika: string;
  privilegija: number;
}

const getBase64 = (file: File) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result?.slice(23));
    reader.onerror = (error) => reject(error);
  });

export const ProfileBody: FC = () => {
  const [user, setUser] = useState<User>({
    email: "",
    ime: "",
    korisnickoIme: "",
    prezime: "",
    privilegija: 0,
    profilSlika: "",
  });
  const fileInputRef = useRef<HTMLInputElement>(null);
  var [usercorrecttask, setUserCorrectTasks] = useState<number>();
  var [useralltask, setUserAllTasks] = useState<number>();
  const usr = getUser();

  useEffect(() => {
    const getUserData = async () => {
      

      const response = await axios.get<User>(
        process.env.REACT_APP_URL_PREFIX + `/Korisnik/getProfile`,
        {
          params: {
            username: usr.korisnickoIme,
          },
        }
      );
      setUser({ ...response.data, privilegija: usr.idPrivilegija });

      const response2 = await axios.get<number>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/brojTocnihRjesenja/" + usr.idKorisnik
      );
      setUserCorrectTasks(response2.data);

      const response3 = await axios.get<number>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/brojDjelomicnihRjesenja/" + usr.idKorisnik
      );
      setUserAllTasks(response3.data);

    };

    getUserData();
  }, []);

  const triggerFileInput = () => {
    fileInputRef.current?.click();
  };

  const sendProfilePicture: ChangeEventHandler<HTMLInputElement> = async (
    event
  ) => {
    try {
      const { files } = event.target;
      const file = files?.item(0);

      if (file) {
        const base64String = (await getBase64(file)) as string;

        await axios.post(
          process.env.REACT_APP_URL_PREFIX + "/Korisnik/updateProfilePicture",
          { picture: base64String },
          { headers: { "Content-Type": "application/json" } }
        );

        setUser({ ...user, profilSlika: base64String });
      }
    } catch (error) {
      console.error(error);
    } finally {
      event.target.value = "";
    }
  };

  return (
    <Flex direction="column">
      {/* top info */}
      <Flex bg="white" align="center" p="3" mb="3">
        {user ? (
          <>
            <Box position="relative">
              <Circle
                position="absolute"
                zIndex="overlay"
                size="16"
                bg="gray.100"
                transition="all 150ms ease-in"
                cursor="pointer"
                opacity={0}
                _hover={{
                  opacity: 1,
                }}
                onClick={triggerFileInput}
              >
                Edit
              </Circle>
              <Avatar
                boxSize="16"
                src={`data:image/jpg;base64,${user.profilSlika}`}
              />
            </Box>
            <VStack align="start" ml="3" spacing="0">
              <Text fontSize="xl" fontWeight="bold">
                {user.ime} {user.prezime}
              </Text>
              <Text fontSize="md" color="gray.400">
                {user.email}
              </Text>
              <Text fontSize="sm" color="gray.300">
                {privilegijaMap[user.privilegija]}
              </Text>
            </VStack>
            <Input
              ref={fileInputRef}
              type="file"
              name="file-input"
              accept="image/jpeg"
              multiple={false}
              display="none"
              onChange={sendProfilePicture}
            />
          </>
        ) : (
          <Spinner />
        )}
      </Flex>
      {usr.idPrivilegija === 3 ? (
        <VStack align="start" ml="3" spacing="0">
          <Text fontSize="xl" fontWeight="bold">Broj uspješno rješenih zadataka: {usercorrecttask}</Text>
          <Text fontSize="xl" fontWeight="bold">Broj otvorenih zadataka: {useralltask}</Text>
          {useralltask !== 0 ? (
            <Text fontSize="xl" fontWeight="bold">Postotak rješenosti zadataka: {usercorrecttask/useralltask}%</Text>
          ):(
            <Text fontSize="xl" fontWeight="bold">Postotak rješenosti zadataka: 0%</Text>
          )}
          
        </VStack>
      ):(
      <TaskList />
      )}   
    </Flex>
  );
};