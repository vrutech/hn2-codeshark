import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { Logout } from "components/auth/Logout";
import AdminPage from "pages/AdminPage";
import Authentification from "pages/Authentification";
import LandingPage from "pages/LandingPage";
import NewCompPage from "pages/NewCompPage";
import NewTaskPage from "pages/NewTaskPage";
import ProfilePage from "pages/ProfilePage";
import TaskEditPage from "pages/TaskEditPage";
import AllTasksPage from "pages/AllTasksPage";
import TaskViewPage from "pages/TaskViewPage";
import UnconfirmedPage from "pages/UnconfirmedPage";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import OtherUsersProfilePage from "user/OtherUsersProfilePage";
import UserSearchPage from "user/UserSearchPage";
import OtherUsersTasksPage from "user/OtherUsersTasksPage";
import EditOtherProfilePage from "user/EditOtherProfilePage";
import CompViewPage from "pages/CompViewPage";
import VirtualCompPage from "pages/VirtualCompPage";
import OldCompViewPage from "pages/OldCompViewPage";
import CalendarPage from "pages/CalendarPage";
import TaskSearchPage from "pages/TaskSearchPage";
import StatisticsPage from "pages/StatisticsPage";
import OtherUserCalendarPage from "user/OtherUserCalendarPage";
import EditCompPage from "pages/EditCompPage";

const theme = extendTheme({
  colors: {
    "primary-100": "#5D5FEF",
    "primary-80": "#7879F1",
    "primary-60": "#A5A6F6",
    secondary: "",
    accent: "",
  },

  styles: {
    global: {
      "html, body, #root": {
        height: "full",
        background: "#F6F8FB",
      },
    },
  },
});

export const App = () => {
  return (
    <Router>
      <ChakraProvider resetCSS theme={theme}>
        <Switch>
          <Route path={["/"]} exact>
            <Authentification />
          </Route>
          <Route path={"/landing-page"} exact>
            <LandingPage />
          </Route>
          <Route path={"/admin-page"} exact>
            <AdminPage />
          </Route>
          <Route path={"/unconfirmed-page"} exact>
            <UnconfirmedPage />
          </Route>
          <Route path={"/profile"} exact>
            <ProfilePage />
          </Route>
          <Route path={"/newtask"} exact>
            <NewTaskPage />
          </Route>
          <Route path={"/alltasks"} exact>
            <AllTasksPage />
          </Route>
          <Route path={"/task-search"} exact>
            <TaskSearchPage />
          </Route>
          <Route path={"/task/:id"} exact>
            <TaskViewPage />
          </Route>
          <Route path={"/task/edit/:id"} exact>
            <TaskEditPage />
          </Route>
          <Route path={"/user-search"} exact>
            <UserSearchPage />
          </Route>
          <Route path={"/logout"} exact>
            <Logout />
          </Route>
          <Route path={"/newcomp"} exact>
            <NewCompPage />
          </Route>
          <Route path={"/comp/:id"} exact>
            <CompViewPage />
          </Route>
          <Route path={"/ranglista/:id"} exact>
            <StatisticsPage />
          </Route>
          <Route path={"/virtualcomp"} exact>
            <VirtualCompPage />
          </Route>
          <Route path={"/editcomp/:id"} exact>
            <EditCompPage />
          </Route>
          <Route path={"/oldvirtualcomp/:id"} exact>
            <OldCompViewPage />
          </Route>
          <Route path={"/other-user-profile/:username"} exact>
            <OtherUsersProfilePage />
          </Route>
          <Route path={"/users-tasks/:id"} exact>
            <OtherUsersTasksPage />
          </Route>
          <Route path={"/edit-other-profile/:username"} exact>
            <EditOtherProfilePage />
          </Route>
          <Route path={"/calendar"} exact>
            <CalendarPage />
          </Route>
          <Route path={"/users-competitions/:id"} exact>
            <OtherUserCalendarPage />
          </Route>
        </Switch>
      </ChakraProvider>
    </Router>
  );
};
