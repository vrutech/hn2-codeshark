import {
    VStack,
    Spinner,
    Td,
    Input,
    Button,
    Avatar,
    Box,
    Flex,
    Text,
    useColorModeValue,
    useBreakpointValue,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Heading,
    Center,
    Link,
  } from "@chakra-ui/react";
  import axios from "axios";
  import React, { FC, useEffect, useState, FormEventHandler } from "react";
  import { useHistory, useParams } from "react-router";
  import { VerifyButton } from "components/shared/Buttons";
  import { getUser } from "components/shared/Utils";
 
 export const OtherUsersTasksBody: FC = () => {
 
    const usrId = useParams();
    var [task, setTasks] = useState([]);
    
 
    const getTasks = () => {
       axios
           .get(process.env.REACT_APP_URL_PREFIX + "/Korisnik/voditeljeviNapravljeniZadaci/" +usrId.id)
           .then((response) => {
               console.log(response.data);
               const myTasks = response.data;
 
               setTasks(myTasks);
           });
   }
 
   const usr2 = getUser();
 
   useEffect(() => getTasks(), []);
 
   const { push } = useHistory();
 
   function chooseTask(id) {
       push('/task/' + id);
   }
   function editTask(id) {
     push('/task/edit/' + id);
   }
 
    return (
       <Box>
                 <Center h="50%" w="100%">
                     <Box>
                         {task.length === 0 ? (
                             <>
                                 {" "}
                                 <Heading as="h2" size="lg" isTruncated>
                                     Nema zadataka!
                                 </Heading>{" "}
                             </>
                         ) : (
                             <>
                                 <Center>
                                     <Heading as="h4" size="md" isTruncated>
                                         Lista svih zadataka:
                                     </Heading>
                                 </Center>
 
                                 <Table variant="striped" colorScheme="purple">
                                     <Thead>
                                         <Tr>
                                             <Th>Naziv zadatka</Th>
                                             <Th>Broj bodova</Th>
                                             <Th>Vrijeme izvođenja</Th>
                                             <Th></Th>
                                             <Th></Th>
                                         </Tr>
                                     </Thead>
                                     <Tbody>
                                         {task.map((element) => {
                                             return (
                                                 <Tr key={element.idZadatak}>
                                                     <Td>{element.nazivZadatak}</Td>
                                                     <Td>{element.bodovi}</Td>
                                                     <Td>{element.maxVrijeme.totalSeconds}</Td>
                                                     <Td>
                                                         <VerifyButton
                                                             colorScheme="facebook"
                                                             onClick={() => chooseTask(element.idZadatak)}
                                                             value={element.idZadatak}
                                                         >
                                                             Odaberi zadatak
                                                         </VerifyButton>
                                                     </Td>
                                                     {usr2.idPrivilegija === 1 ? (
                                                     <Td>
                                                         <VerifyButton
                                                             colorScheme="facebook"
                                                             onClick={() => editTask(element.idZadatak)}
                                                             value={element.idZadatak}
                                                         >
                                                             Uređivanje
                                                         </VerifyButton>
                                                     </Td>
                                                     ):(<Td></Td>)}
                                                 </Tr>
                                             );
                                         })}
                                     </Tbody>
                                 </Table>
                             </>
                         )}
                     </Box>
                 </Center>
             </Box>
    );
 }