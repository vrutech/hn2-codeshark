import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";
import {EditOtherProfileBody} from "./EditOtherProfileBody";

const EditOtherProfilePage: FC = () => {

  return (
    <Flex direction="column">
      <NavBar />

      <EditOtherProfileBody />
    </Flex>
   );
 };
 
 export default EditOtherProfilePage;