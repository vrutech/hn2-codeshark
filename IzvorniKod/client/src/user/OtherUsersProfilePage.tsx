import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";
import { OtherUserProfileBody } from "./OtherUserProfileBody";

const OtherUsersProfilePage: FC = () => {

  return (
    <Flex direction="column">
      <NavBar />

      <OtherUserProfileBody />
    </Flex>
   );
 };
 
 export default OtherUsersProfilePage;