import React, { FC } from "react";
import axios from "axios";

import {
   Box,
   Flex,
   Text,
   useColorModeValue,
   useBreakpointValue,
   Table,
   Thead,
   Tbody,
   Tr,
   Th,
   Td,
   Heading,
   Center,
   Link,
   FormControl,
   FormLabel,
   Input,
   Button
} from '@chakra-ui/react';
import { useState } from "react";
import { useEffect } from "react";
import { setSyntheticTrailingComments } from "typescript";
import { useHistory } from "react-router";
import { VerifyButton } from "components/shared/Buttons";

const UserSearchPage: FC = () => {


   var [users, setUsers] = useState([]);
   var [str, setStr] = useState("");
   var [search, setSearch] = useState(false);

   const getUsers = () => {
      axios.get(process.env.REACT_APP_URL_PREFIX + "/korisnik/searchusers", { params: { input: str } })
         .then((response) => {
            console.log(response.status);
            console.log(response.data);
            const users2 = response.data;
            setSearch(true)
            setUsers(users2);
         });
   };
   const Funkcija = () => {
      getUsers();
   }

   const { push } = useHistory();

   function chooseUser(username1) {
      push('/other-user-profile/' + username1);
   }

   return (
      <Box>
         <Flex
            bg={useColorModeValue('white', 'gray.800')}
            color={useColorModeValue('gray.600', 'white')}
            minH={'60px'}
            py={{ base: 2 }}
            px={{ base: 4 }}
            borderBottom={1}
            borderStyle={'solid'}
            borderColor={useColorModeValue('gray.200', 'gray.900')}
            align={'center'}>
            <Flex
               flex={{ base: 1, md: 'auto' }}
               ml={{ base: -2 }}
               display={{ base: 'flex', md: 'none' }}>
            </Flex>
            <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
               <Link href="/">
                  <Text
                     textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
                     fontFamily={'heading'}
                     color={useColorModeValue('gray.800', 'white')}>
                     <Heading>CodeShark</Heading>
                  </Text>
               </Link>
               <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
                  <Center>
                     <Heading as="h4" size="md" isTruncated>Pretraživanje korisnika</Heading>
                  </Center>
               </Flex>
            </Flex>
         </Flex>

         <Center h="50%" w="100%">


            <Box>
               <FormControl>
                  <FormLabel htmlFor='username'>Unesite znakove za pretraživanje korisnika:</FormLabel>
                  <Input
                     id='username'
                     type='text'
                     required
                     value={str}
                     onChange={(e) => setStr(e.target.value)} />
                  <Center w="full">
                     <Button
                        onClick={Funkcija}
                     >Pretraga
                     </Button>
                  </Center>
               </FormControl>
               {!search ? <></> : <>
                  {users.length == 0 ? <> <Heading as="h2" size="md" isTruncated>Nema korisnika za prikaz!</Heading> </> : <><Center>
                     <Heading as="h4" size="md" isTruncated>Popis korisnika:</Heading>
                  </Center>

                     <Table variant="striped" colorScheme="purple">
                        <Thead>
                           <Tr>
                              <Th>Korisničko ime</Th>
                              <Th></Th>
                           </Tr>
                        </Thead>
                        <Tbody>
                           {users.map((element) => {
                              return (
                                 <Tr key={element}>
                                    <Td>{element}</Td>
                                    <Td>
                                       <VerifyButton
                                          colorScheme="facebook"
                                          onClick={() => chooseUser(element)}
                                          value={element}
                                       >
                                          Odaberi
                                       </VerifyButton>
                                    </Td>
                                 </Tr>
                              );
                           })}
                        </Tbody>
                     </Table>
                  </>}
               </>}

            </Box>
         </Center>
      </Box>
   );
}

export default UserSearchPage;