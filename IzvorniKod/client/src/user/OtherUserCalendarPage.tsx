import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";
import {OtherUserCalendarBody} from "./OtherUserCalendarBody";


const EditOtherProfilePage: FC = () => {

  return (
    <Flex direction="column">
      <NavBar />

      <OtherUserCalendarBody />
    </Flex>
   );
 };
 
 export default EditOtherProfilePage;