import {
  Flex,
  Text,
  VStack,
  Spinner,
  Td,
  Input,
  Button,
  Avatar,
  Spacer,
  Square,
  Box,
  Circle,
  Grid,
  Center,
  Link
} from "@chakra-ui/react";
import axios from "axios";
import React, { FC, useEffect, useState, FormEventHandler, useRef } from "react";
import { useHistory, useParams } from "react-router";
import { VerifyButton } from "components/shared/Buttons";
import { getUser } from "components/shared/Utils";
import Slika from "images/trophyImage.png";

export const privilegijaMap: { [key: number]: string } = {
  1: "Administrator",
  2: "Voditelj",
  3: "Natjecatelj",
};

interface OtherUser {
  korisnickoIme: string;
  ime: string;
  prezime: string;
  email: string;
  profilSlika: string;
  idPrivilegija: number;
  idKorisnik: number;
}

interface Pobjednik {
  idNatjecanje: number;
  nazivNatjecanje: string;
  peharSlika: string;
}

const getBase64 = (file: File) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result?.slice(23));
    reader.onerror = (error) => reject(error);
  });

export const OtherUserProfileBody: FC = () => {
  const [otheruser, setOtherUser] = useState<OtherUser>();
  var [usercorrecttask, setUserCorrectTasks] = useState<number>();
  var [useralltask, setUserAllTasks] = useState<number>();
  var [pobj, setPobj] = useState<Pobjednik[]>([]);
  const usr = useParams();

  useEffect(() => {
    const getOtherUserData = async () => {

      const response = await axios.get<OtherUser>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/getProfile",
        {
          params: {
            username: usr.username,
          },
        }
      );
      console.log(response.data);
      setOtherUser({ ...response.data});
      console.log(otheruser)

      const response2 = await axios.get<number>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/brojTocnihRjesenja/" + response.data.idKorisnik
      );
      setUserCorrectTasks(response2.data);
      console.log(response2.data);

      const response3 = await axios.get<number>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/brojDjelomicnihRjesenja/" + response.data.idKorisnik
      );
      setUserAllTasks(response3.data);

      const response4 = await axios.get<Pobjednik>(
        process.env.REACT_APP_URL_PREFIX + "/Korisnik/pobjedioNatjecanja/" + response.data.idKorisnik
      )
      setPobj(response4.data);
    };
    getOtherUserData();
  }, []);

  const usr2 = getUser();
  console.log(usr2);

  
  const { push } = useHistory();

  function editUser() {
    push('/edit-other-profile/' + usr.username);
  };

  function showUsersTasks() {
    push('/users-tasks/' + otheruser?.idKorisnik);
  }
  function showUsersCompetitions() {
    push('/users-competitions/' + otheruser?.idKorisnik);
  }

  function chooseComp(id) {
    push('/comp/' + id);
  }

  return (
    <Flex direction="column">
      <Flex bg="white" align="center" p="3" mb="3">
        {otheruser ? (
          <>
            <Box position="relative">
              <Circle
                position="absolute"
                zIndex="overlay"
                size="16"
                bg="gray.100"
                transition="all 150ms ease-in"
                cursor="pointer"
                opacity={0}
              >
                Edit
              </Circle>
              <Avatar
                boxSize="16"
                src={`data:image/jpg;base64,${otheruser.profilSlika}`}
              />
            </Box>
            <VStack align="start" ml="3" spacing="0">
              <Text fontSize="xl" fontWeight="bold">
                {otheruser.ime} {otheruser.prezime} ({otheruser.korisnickoIme})
              </Text>
              <Text fontSize="md" color="gray.400">
                {otheruser.email}
              </Text>
              <Text fontSize="sm" color="gray.400">
                {privilegijaMap[otheruser.idPrivilegija]}
              </Text>
            </VStack>

            <VStack align="right" ml="3" spacing="0">
              <Flex align="center" p="3" mb="3">
              {usr2.idPrivilegija === 1 ? (
                <VerifyButton
                    colorScheme="facebook"
                    bgColor = "#FF8000"
                    textColor = "white"
                    onClick={() => editUser()}
                >
                    Uredi korisnikove podatke
                </VerifyButton>
                
                ) : (
                  <VStack></VStack>
                )}
              </Flex>
            </VStack>
          </>
        ) : (
          <Spinner />
        )}
      </Flex>
      <Center bg = "white">
      <VStack >
          {/* statistika */}
          {otheruser?.idPrivilegija === 3 ? (
              <VStack align="center" ml="3" spacing="3">
                <Text fontSize="xl" fontWeight="bold">Broj uspješno rješenih zadataka: {usercorrecttask}</Text>
                <Text fontSize="xl" fontWeight="bold">Broj otvorenih zadataka: {useralltask}</Text>
                {useralltask !== 0 ? (
                  <Text fontSize="xl" fontWeight="bold">Postotak rješenosti zadataka: {usercorrecttask/useralltask*100}%</Text>
                ):(
                  <Text fontSize="xl" fontWeight="bold">Postotak rješenosti zadataka: 0%</Text>
                )}
                <Square size='35px'>
                  <Text></Text>
                </Square>
                <Text fontSize="2xl" fontWeight="bold">Osvojena prva mjesta na natjecanjima:</Text>
                <Flex direction="column" bg="white" borderRadius="md" mx="3">
                  <Grid
                    templateColumns="repeat(2, 1fr)"
                    gap="1"
                    placeItems="center"
                    fontWeight="bold"
                    fontSize="xl"
                  >
                    <Text>Trofej</Text>
                    <Text>Naziv natjecanja</Text>
                  </Grid>
                  {pobj?.map((pobjed) => (
                    <Grid
                      key={pobjed.idNatjecanje}
                      templateColumns="repeat(2, 1fr)"
                      gap="1"
                      placeItems="center"
                      p="1"
                    >
                      {pobjed.peharSlika !== null ? (
                        <Avatar
                        boxSize="16"
                        src={`data:image/jpg;base64,${pobjed.peharSlika}`}
                        />
                      ):
                      (
                        <Avatar
                        boxSize="16"
                        src= {Slika}
                        />)}
                      
                      <Link>
                        <Text onClick={() => chooseComp(pobjed.idNatjecanje)}>{pobjed.nazivNatjecanje}</Text>
                      </Link>
                    </Grid>
                  ))}
                </Flex>
              </VStack>

          ):
          (
            <VStack align="start" ml="3" spacing="0">
              <Flex>
                <VerifyButton
                    colorScheme="facebook"
                    bg = "lightblue"
                    onClick={() => showUsersTasks()}
                >
                    Korisnikovi zadaci
                </VerifyButton>
                <Square size='50px'>
                  <Text></Text>
                </Square>
                <VerifyButton
                  colorScheme="facebook"
                  bg = "lightgreen"
                  onClick={() => showUsersCompetitions()}
                >
                  Kalendar korisnikovih natjecanja
                </VerifyButton>
              </Flex>
            </VStack>
          )}

      </VStack>
      </Center>
    </Flex>
    
  );
};