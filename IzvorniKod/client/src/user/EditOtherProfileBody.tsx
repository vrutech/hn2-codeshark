import { Box, Button, Center, Flex, Input, Radio, RadioGroup, Textarea, VStack, Text } from "@chakra-ui/react";
import axios from "axios";
import { NavBar } from "components/shared/NavBar";
import React, { FC, useState, useEffect } from "react";
import { useHistory, useParams } from "react-router";


interface OtherUser {
   korisnickoIme: string;
   ime: string;
   prezime: string;
   email: string;
   slika: Uint8Array[];
   idPrivilegija: number;
   idKorisnik: number;
}

const convertPrivilegija = (privil
    : Privilegija) => {
    switch (privil
    ) {
        case "admin":
            return 1;
        case "voditelj":
            return 2;
        case "natjecatelj":
            return 3;
        default:
            break;
    }
};

type Privilegija = "admin" | "voditelj" | "natjecatelj";


export const EditOtherProfileBody: FC = () => {

   const [otheruser, setOtherUser] = useState<OtherUser>();
   const usr = useParams();
   const { push } = useHistory();
   const[privil, setPrivil] = useState<Privilegija>("natjecatelj");
   

   useEffect(() => {
      const getOtherUserData = async () => {
  
        const response = await axios.get<OtherUser>(
          process.env.REACT_APP_URL_PREFIX + "/Korisnik/getProfile",
          {
            params: {
              username: usr.username,
            },
          }
        );
        console.log(response.data);
        setOtherUser({ ...response.data});
        if(response.data.idPrivilegija === 1) {
            setPrivil("admin");
        }
        if(response.data.idPrivilegija === 2) {
            setPrivil("voditelj");
        }
      };
      getOtherUserData();
    }, []);



    const [greska, setGreska] = useState();

    const submitForm = async (event) => {
      event.preventDefault();
      const KorisnickoIme = document.getElementById("korisnickoIme")?.value;
      const Ime = document.getElementById("ime")?.value;
      const Prezime = document.getElementById("prezime")?.value;
      const IdPrivilegija =convertPrivilegija(privil);
      const user = {
         KorisnickoIme,
         Ime,
         Prezime,
         IdPrivilegija
      }
      console.log(user);
      try {
          const response = await axios.post(process.env.REACT_APP_URL_PREFIX + "/Korisnik/editProfile/" + otheruser?.idKorisnik, user)
          if (response.status === 200) {
              push('/other-user-profile/' + document.getElementById("korisnickoIme")?.value)
          } else if (response.status === 400) {
              console.log(response);
          }
      } catch (err) {
          console.log(err)
          setGreska("Korisnik s tim imenom već postoji!");
      }

  }

  return (
    <Center w="100%">
            <VStack>
        <Center>
        {greska !== null && <Center><Box minw="0" color="red.400" padding="0px"><Text>{greska}</Text></Box></Center>}
                </Center>
            <VStack
                align="start"
                minW={{
                    base: "100vw",
                    md: "300px",
                }}
                w="full"
                maxW={{
                    base: "100vw",
                    md: "800px",
                }}
                minH={{
                    base: "100vh",
                    md: "400px",
                }}
                h="full"
                maxH={{
                    base: "100vh",
                    md: "650px",
                }}
                bg="white"
                boxShadow={{
                    base: "none",
                    md: "lg",
                }}
                borderRadius={{
                    base: "none",
                    md: "lg",
                }}
                mx="auto"
                p="6"
                spacing="6"
            >
                 

                <Flex justify="space-between" minW="full">
                    <Center w="full">
                        <VStack as="form" spacing="6" align="start" minW="full">
                            
                            <Center w="full">
                                <Flex minW="0">
                                    <Text fontSize="3xl" fontWeight="bold">
                                        Uređivanje korisnika {otheruser?.korisnickoIme}
                                    </Text>
                                </Flex>
                            </Center>
                            <Box minW="0" fontSize="lg">
                                    <Text fontWeight="bold">Korisnicko ime:</Text>
                                    <Input defaultValue={otheruser?.korisnickoIme} w="25rem" id="korisnickoIme" required></Input>
                            </Box>

                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Ime:</Text>
                                <Input defaultValue={otheruser?.ime} w="10rem" id="ime" required></Input>
                            </Box>
                            <Box minW="0" fontSize="lg">
                                <Text fontWeight="bold">Prezime:</Text>
                                <Input defaultValue={otheruser?.prezime} w="10rem" id="prezime" required></Input>
                            </Box>
                            <Box minW="full">
                            <Text fontSize="lg" fontWeight="bold">ID privilegije:</Text>
                                {otheruser?.idPrivilegija === 1? (
                                    <RadioGroup
                                    name="idPrivilegije" w="15rem"
                                    id = "idPrivilegije"
                                    defaultValue={"admin"}
                                    onChange={(newPrivil: Privilegija) => {
                                        setPrivil(newPrivil);
                                    }}
                                >
                                    <Flex justify="space-between" w="full">
                                       
                                          <Radio value="admin" padding = "1rem"> Admin</Radio>
                                          <Radio value="voditelj" padding = "1rem"> Voditelj </Radio>
                                          <Radio value="natjecatelj" padding = "1rem"> Natjecatelj </Radio>
                                    </Flex>
                                </RadioGroup>
                                ):(<VStack></VStack>)}
                                {otheruser?.idPrivilegija === 2? (
                                    <RadioGroup
                                    name="idPrivilegije" w="15rem"
                                    id = "idPrivilegije"
                                    defaultValue={"voditelj"}
                                    onChange={(newPrivil: Privilegija) => {
                                        setPrivil(newPrivil);
                                    }}
                                >
                                    <Flex justify="space-between" w="full">
                                       
                                          <Radio value="admin" padding = "1rem"> Admin</Radio>
                                          <Radio value="voditelj" padding = "1rem"> Voditelj </Radio>
                                          <Radio value="natjecatelj" padding = "1rem"> Natjecatelj </Radio>
                                    </Flex>
                                </RadioGroup>
                                ):(<VStack></VStack>)}
                                {otheruser?.idPrivilegija === 3? (
                                    <RadioGroup
                                    name="idPrivilegije" w="15rem"
                                    id = "idPrivilegije"
                                    defaultValue={"natjecatelj"}
                                    onChange={(newPrivil: Privilegija) => {
                                        setPrivil(newPrivil);
                                    }}
                                >
                                    <Flex justify="space-between" w="full">
                                       
                                          <Radio value="admin" padding = "1rem"> Admin</Radio>
                                          <Radio value="voditelj" padding = "1rem"> Voditelj </Radio>
                                          <Radio value="natjecatelj" padding = "1rem"> Natjecatelj </Radio>
                                    </Flex>
                                </RadioGroup>
                                ):(<VStack></VStack>)}
                            </Box>
                            <Flex as="form">
                                <Button onClick={submitForm}>Spremi promjene</Button>
                            </Flex>
                            
                        </VStack>
                    </Center>
                </Flex>
            </VStack>
            </VStack>
        </Center>
   );
};
 
