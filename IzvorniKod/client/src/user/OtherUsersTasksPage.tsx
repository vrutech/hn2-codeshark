import { Flex } from "@chakra-ui/react";
import { NavBar } from "components/shared/NavBar";
import React, { FC } from "react";
import { OtherUsersTasksBody } from "./OtherUsersTasksBody";

const OtherUsersTasksPage: FC = () => {

  return (
    <Flex direction="column">
      <NavBar />

      <OtherUsersTasksBody />
    </Flex>
   );
 };
 
 export default OtherUsersTasksPage;