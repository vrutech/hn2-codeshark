﻿using backend.Models;

namespace backend.DTOs
{
    public class KorisnikProfileDTO
    {
        public string KorisnickoIme { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public byte[] ProfilSlika { get; set; }
        public int IdPrivilegija { get; set; }
        public int IdKorisnik { get; set; }

        public KorisnikProfileDTO(Korisnik korisnik)
        {
            KorisnickoIme = korisnik.KorisnickoIme;
            Ime = korisnik.Ime;
            Prezime = korisnik.Prezime;
            Email = korisnik.Email;
            ProfilSlika = korisnik.ProfilSlika;
            IdPrivilegija = korisnik.IdPrivilegija;
            IdKorisnik = korisnik.IdKorisnik;
        }
    }
}