﻿using backend.Models;
using System.Collections.Generic;

namespace backend.DTOs
{
    public class KorisnikDto
    {
        public int IdKorisnik { get; set; }
        public string KorisnickoIme { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public bool PotvrdjenMail { get; set; }
        public int IdPrivilegija { get; set; }
        public bool PotvrdjenaPrivilegija { get; set; }
        public byte[] ProfilSlika { get; set; }
        public string Token { get; set; }

        public KorisnikDto(Korisnik korisnik, string token)
        {
            IdKorisnik = korisnik.IdKorisnik;
            KorisnickoIme = korisnik.KorisnickoIme;
            Ime = korisnik.Ime;
            Prezime = korisnik.Prezime;
            Email = korisnik.Email;
            PotvrdjenMail = korisnik.PotvrdjenMail;
            IdPrivilegija = korisnik.IdPrivilegija;
            PotvrdjenaPrivilegija = korisnik.PotvrdjenaPrivilegija;
            ProfilSlika = korisnik.ProfilSlika;
            Token = token;
        }
    }
}
