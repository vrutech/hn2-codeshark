﻿using System;
using System.Collections.Generic;
using backend.Models;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class NoviZadatakDTO
    {
        public string NazivZadatak { get; set; }
        public int Bodovi { get; set; }
        public int MaxVrijeme { get; set; }
        public string TekstZadatak { get; set; }
        public bool Vidljivost { get; set; }
        public int IdKorisnik { get; set; }

        public IEnumerable<TestniPodaci> TestniPodaci { get; set; }
    }
}
