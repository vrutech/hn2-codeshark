﻿using System;
using System.Collections.Generic;
using backend.Models;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class NatjecanjeKalendarDTO
    {
        public string NazivNatjecanje { get; set; }
        public int IdNatjecanje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public int BrojZadataka { get; set; }

        public string? VoditeljTvorac { get; set; }

        public NatjecanjeKalendarDTO(Natjecanje nat, string? tvorac)
        {
            NazivNatjecanje = nat.NazivNatjecanje;
            IdNatjecanje = nat.IdNatjecanje;
            VrijemePocetak = nat.VrijemePocetak;
            VrijemeKraj = nat.VrijemeKraj;
            BrojZadataka = nat.BrojZadataka;
            VoditeljTvorac = tvorac;
        }
    }
}
