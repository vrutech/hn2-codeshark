﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class NovoNatjecanjeDTO
    {
        public string NazivNatjecanje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public byte[] PeharSlika { get; set; }
        public bool JeVirtualno { get; set; }
        public int IdKorisnik { get; set; }
        public int[] ListaZadataka { get; set; }
    }
}
