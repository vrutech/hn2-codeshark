﻿using System.Text.Json.Serialization;
using backend.Models;

namespace backend.DTOs
{
    public class StatsDTO
    {
        [JsonInclude]
        public string Username;
        [JsonInclude]
        public decimal Bodovi;

        public StatsDTO(string username, decimal bodovi)
        {
            Username = username;
            Bodovi = bodovi;
        }
        
        public StatsDTO(Statistika stat)
        {
            Username = stat.KorisnickoIme;
            Bodovi = stat.Bodovi;
        }
    }
}