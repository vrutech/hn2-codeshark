﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Models;

namespace backend.DTOs
{
    public class PobjednikDTO
    {
        public int IdNatjecanje { get; set; }
        public string NazivNatjecanje { get; set; }
        public byte[] PeharSlika { get; set; }

        public PobjednikDTO(Natjecanje nat)
        {
            IdNatjecanje = nat.IdNatjecanje;
            NazivNatjecanje = nat.NazivNatjecanje;
            PeharSlika = nat.PeharSlika;
        }
    }
}
