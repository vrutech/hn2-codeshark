﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class EvaluacijaDTO
    {
        public int idZadatak { get; set; }
        public int idKorisnik { get; set; }
        public string korisnickoIme { get; set; }
        public byte[] programBase64 { get; set; }
        public int? idNatjecanje { get; set; }
    }
}
