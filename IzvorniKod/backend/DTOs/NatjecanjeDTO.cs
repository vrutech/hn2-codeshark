﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class NatjecanjeDTO
    {
        public const int BEFORE = 1;
        public const int IN_PROGRESS = 2;
        public const int AFTER = 3;

        public int IdNatjecnaje { get; set; }
        public string NazivNatjecanje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public byte[] PeharSlika { get; set; }
        public int BrojZadataka { get; set; }
        public int IdKreatora { get; set; }
        public string KorisnickoImeKreatora { get; set; }
        public int Stanje { get; set; }
        public IEnumerable<Zadatak> Zadaci { get; set; }
        public IEnumerable<StatsDTO> Statistika { get; set; }

        public NatjecanjeDTO(Natjecanje natjecanje, Korisnik autor, int stanje, IEnumerable<Zadatak> listaZadataka, IEnumerable<StatsDTO> statistika)
        {
            IdNatjecnaje = natjecanje.IdNatjecanje;
            NazivNatjecanje = natjecanje.NazivNatjecanje;
            VrijemePocetak = natjecanje.VrijemePocetak;
            VrijemeKraj = natjecanje.VrijemeKraj;
            PeharSlika = natjecanje.PeharSlika;
            BrojZadataka = natjecanje.BrojZadataka;
            IdKreatora = autor.IdKorisnik;
            KorisnickoImeKreatora = autor.KorisnickoIme;
            Stanje = stanje;
            Zadaci = listaZadataka;
            Statistika = statistika;
        }
    }
}
