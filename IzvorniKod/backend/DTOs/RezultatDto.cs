﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backend.Models;

namespace backend.DTOs
{
    public class RezultatDto
    {
        public int DobiveniBodovi { get; set; }
        public DateTime VrijemePredaje { get; set; }
        public byte[] PredanoRjesenje { get; set; }
        public int IdZadatak { get; set; }
        public int IdKorisnik { get; set; }
        public RezultatDto(Rezultat rezultat)
        {
            DobiveniBodovi = (int)rezultat.DobiveniBodovi;
            VrijemePredaje = rezultat.VrijemePredaje;
            PredanoRjesenje = rezultat.PredanoRjesenje;
            IdZadatak = rezultat.IdZadatak;
            IdKorisnik = rezultat.IdKorisnik;
        }
    }
    
}
