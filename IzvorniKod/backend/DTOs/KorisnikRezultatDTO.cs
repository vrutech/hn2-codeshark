﻿using System;

namespace backend.DTOs
{
    public class KorisnikRezultatDTO
    {
        public KorisnikRezultatDTO(int idKorisnik, string korisnickoIme, int idZadatak, int bodovi, decimal dobiveniBodovi, DateTime vrijemePredaje, DateTime vrijemeKraj)
        {
            IdKorisnik = idKorisnik;
            KorisnickoIme = korisnickoIme;
            IdZadatak = idZadatak;
            Bodovi = bodovi;
            DobiveniBodovi = dobiveniBodovi;
            VrijemePredaje = vrijemePredaje;
            VrijemeKraj = vrijemeKraj;
        }

        public int IdKorisnik { get; set; }
        public string KorisnickoIme { get; set; }
        public int IdZadatak { get; set; }
        public int Bodovi { get; set; }
        public decimal DobiveniBodovi { get; set; }
        public DateTime VrijemePredaje { get; set; }
        public DateTime VrijemeKraj { get; set; }
    }
}