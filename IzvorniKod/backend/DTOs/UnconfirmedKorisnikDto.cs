﻿using backend.Models;

namespace backend.DTOs
{
    public class UnconfirmedKorisnikDto
    {
        public int IdKorisnik { get; set; }
        public string KorisnickoIme { get; set; }
        public string Email { get; set; }
        public int IdPrivilegija { get; set; }

        public UnconfirmedKorisnikDto(Korisnik korisnik)
        {
            IdKorisnik = korisnik.IdKorisnik;
            KorisnickoIme = korisnik.KorisnickoIme;
            Email = korisnik.Email;
            IdPrivilegija = korisnik.IdPrivilegija;
        }
    }
}