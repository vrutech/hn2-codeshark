﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class RandomVirtualnoNatjecanjeDTO
    {

        public string NazivNatjecnaje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public int BrojZadataka { get; set; }
        public IEnumerable<Zadatak> Zadaci { get; set; }
    }
}