﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class EditPictureDTO
    {
        public byte[] Picture { get; set; }
    }
}
