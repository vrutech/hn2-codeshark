﻿using System;
using System.Collections.Generic;
using backend.Models;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class EditNatjecanjeDTO
    {
        public string NazivNatjecanje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public byte[] PeharSlika { get; set; }
        //public int[] ListZadataka { get; set; }
    }
}