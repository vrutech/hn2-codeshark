﻿using backend.Models;

namespace backend.DTOs
{
    public class TestFileDTO
    {
        public int id;
        public byte[] input;
        public byte[] output;

        public TestFileDTO(TestniPodaci podatak)
        {
            id = podatak.IdTesniPodaci;
            input = podatak.Ulaz;
            output = podatak.Izlaz;
        }
    }
}