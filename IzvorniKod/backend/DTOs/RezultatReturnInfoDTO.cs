﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.DTOs
{
    public class RezultatReturnInfoDTO
    {
        public string PovratnaPoruka { get; set; }
        public double DobiveniBodovi { get; set; }
        public TimeSpan VrijemeMS { get; set; }
    }
}
