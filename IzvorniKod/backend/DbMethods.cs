﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using System.Security.Claims;
using System.Security.Principal;
using backend.DTOs;
using backend.Models;

namespace backend
{
    public static class DbMethods
    {
        public static Korisnik dbGetKorisnikByIdKorisnik(int id)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Korisniks.Single(x => x.IdKorisnik == id); //Prouči ovo: https://docs.microsoft.com/en-us/ef/core/querying/
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static Korisnik dbGetKorisnikByIdKorisnikWithNavigation(int id)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Korisniks.Single(x => x.IdKorisnik == id);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static Korisnik? dbGetKorisnikByKorisnickoIme(string userName)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Korisniks.Single(x => x.KorisnickoIme == userName);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return null;
            }
        }

        public static Korisnik? dbGetKorisnikByEmail(string email)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Korisniks.Single(x => x.Email == email);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return null;
            }
        }
        public static void dbNoviKorisnik(Korisnik noviKorisnik)
        {
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    dbContext.Korisniks.Add(noviKorisnik);
                    var i = dbContext.SaveChanges();
                    if (i <= 0)
                    {
                        return;
                    }

                    Console.WriteLine("Upisan korisnik");
                    SaljiAktivacijskiMail(noviKorisnik);
                    if (noviKorisnik.PotvrdjenaPrivilegija != true)
                    {
                        SaljiAdminuMail(noviKorisnik);
                    }
                }
            }
        }

        public static void dbDeleteKorisnikWithIdKorisnik(int id)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                dbContext.Korisniks.Remove(dbContext.Korisniks.First(x => x.IdKorisnik == id));
                dbContext.SaveChanges();
            }
        }

        public static void dbUpdateKorisnikPotvrdjenMail(string uuid)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = dbContext.Korisniks.Single(x => x.Uuid == uuid);
                if (result == null)
                {
                    return;
                }

                result.PotvrdjenMail = true;
                dbContext.SaveChanges();
            }
        }

        public static void dbUpdateKorisnikPotvrdjenaPrivilegija(int id)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = dbContext.Korisniks.Single(x => x.IdKorisnik == id);
                if (result == null)
                {
                    return;
                }

                result.PotvrdjenaPrivilegija = true;
                dbContext.SaveChanges();
            }
        }

        public static IEnumerable<Korisnik> dbGetAllUnconfirmedPrivilegeKorisnik()
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                return dbContext.Korisniks.Where(k => k.PotvrdjenaPrivilegija == false).ToList();
            }
        }

        private static void SaljiAdminuMail(Korisnik korisnik)
        {
            var aktivacijskiKod = korisnik.IdKorisnik.ToString();
            var verifyUrl = AppSettingsProvider.Configuration["UrlPrefix"] + "/api/v1/Korisnik/confirmUserPrivilege/" + aktivacijskiKod;
            var fromMail = new MailAddress(AppSettingsProvider.Configuration["MailAddress"], "Vrutech Team");
            var toMail = new MailAddress(AppSettingsProvider.Configuration["MailAddress"]);
            var fromMailPassword = AppSettingsProvider.Configuration["MailPassword"];
            string subject = "Zahtjev za aktivaciju privilegiranog računa";
            string body = "<br/><br/>Bok, admin" +
                "<br /><br /> Korisnik " + korisnik.KorisnickoIme +
                " traži privilegirani pristup, potvrditi ga možeš na: " +
                "<br/><br/><a href='" + verifyUrl + "'>link</a> ";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromMail.Address, fromMailPassword)

            };
            using (var message = new MailMessage(fromMail, toMail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                try
                {
                    smtp.Send(message);
                }
                catch (SmtpFailedRecipientsException ex)
                {
                    foreach (var exception in ex.InnerExceptions)
                    {
                        var status = exception.StatusCode;
                        if (status is SmtpStatusCode.MailboxBusy or SmtpStatusCode.MailboxUnavailable)
                        {
                            Console.WriteLine("Delivery failed - retrying in 5 seconds.");
                            System.Threading.Thread.Sleep(5000);
                            smtp.Send(message);
                        }
                        else
                        {
                            Console.WriteLine("Failed to deliver message to {0}",
                                exception.FailedRecipient);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex);
                }
        }

        private static void SaljiAktivacijskiMail(Korisnik korisnik)
        {

            var aktivacijskiKod = korisnik.Uuid;

            var verifyUrl = AppSettingsProvider.Configuration["UrlPrefix"] + "/api/v1/Korisnik/confirmUserMail/" + aktivacijskiKod;
            Console.WriteLine(verifyUrl);
            var fromMail = new MailAddress(AppSettingsProvider.Configuration["MailAddress"], "Vrutech Team");
            var toMail = new MailAddress(korisnik.Email);
            var fromMailPassword = AppSettingsProvider.Configuration["MailPassword"];
            var subject = "Aktivacija računa";
            string body = "<br/><br/>Bok," + korisnik.KorisnickoIme +
              "<br/><br/> Molimo te posjeti sljedeći poveznicu da bi se tvoj račun aktivirao: " +
              "<br/><br/><a href='" + verifyUrl + "'>link</a> ";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromMail.Address, fromMailPassword)

            };
            using (var message = new MailMessage(fromMail, toMail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                try
                {
                    smtp.Send(message);
                }
                catch (SmtpFailedRecipientsException ex)
                {
                    foreach (var exception in ex.InnerExceptions)
                    {
                        var status = exception.StatusCode;
                        if (status is SmtpStatusCode.MailboxBusy or SmtpStatusCode.MailboxUnavailable)
                        {
                            Console.WriteLine("Delivery failed - retrying in 5 seconds.");
                            System.Threading.Thread.Sleep(5000);
                            smtp.Send(message);
                        }
                        else
                        {
                            Console.WriteLine("Failed to deliver message to {0}",
                                exception.FailedRecipient);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex);
                }
        }

        public static Zadatak? dbGetZadatakByNaziv(string nazivZadatak)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Zadataks.Single(x => x.NazivZadatak == nazivZadatak);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }
        public static Zadatak dbGetZadatakById(int idZadatak)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Zadataks.Single(x => x.IdZadatak == idZadatak);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }
        public static List<Zadatak> dbGetSviVidljiviZadaci()
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Zadataks.Where(k => k.Vidljivost == true).ToList();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static List<Zadatak> dbGetKorisnikNapravioZadatke(int idVoditelj)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    List<Zadatak> zadaci = dbContext.Zadataks.Where(k => k.IdKorisnik == idVoditelj).ToList();
                    return zadaci;
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static void postaviVidljivostZadatka(int id)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var zad = dbContext.Zadataks.Single(k => k.IdZadatak == id);
                if (zad == null)
                {
                    return;
                }

                zad.Vidljivost = true;
                dbContext.SaveChanges();
            }
        }
        public static void dbNoviZadatak(Zadatak noviZadatak)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                dbContext.Zadataks.Add(noviZadatak);
                var i = dbContext.SaveChanges();
            }
        }

        public static void dbNoviTestniPodatak(TestniPodaci noviPodatak)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                dbContext.TestniPodacis.Add(noviPodatak);
                var i = dbContext.SaveChanges();
            }
        }

        public static IEnumerable<TestFileDTO> getTestFiles(Zadatak zadatak)
        {
            var result = new List<TestFileDTO>();
            using var dbContext = new CodeSharkDBContext();
            var testFiles =
                dbContext.TestniPodacis
                    .Where(testFile => testFile.IdZadatak == zadatak.IdZadatak)
                    .ToList();
            foreach (var testFile in testFiles)
            {
                result.Add(new TestFileDTO(testFile));
            }
            return result;
        }

        public static void updateProfilePicture(int id, byte[] newPicture)
        {
            using var dbContext = new CodeSharkDBContext();
            var korisnik = dbContext.Korisniks.Find(id);
            korisnik.ProfilSlika = newPicture;
            dbContext.SaveChanges();
        }

        public static void deleteProfilePicture(int id)
        {
            using var dbContext = new CodeSharkDBContext();
            var korisnik = dbContext.Korisniks.Find(id);
            korisnik.ProfilSlika = null;
            dbContext.SaveChanges();
        }

        public static IEnumerable<string> searchUsersByName(string input)
        {
            using var dbContext = new CodeSharkDBContext();
            var searchResult = dbContext.Korisniks
                .Where(user => user.KorisnickoIme.Contains(input)).ToList();
            return searchResult.Select(user => user.KorisnickoIme);
        }

        public static KorisnikProfileDTO getUserProfile(string username)
        {
            using var dbContext = new CodeSharkDBContext();
            var user = dbContext.Korisniks.Single(korisnik => korisnik.KorisnickoIme == username);
            return new KorisnikProfileDTO(user);
        }
        public static void dbSpremiPromjeneKorisnik(int id, Korisnik promjene)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var og = dbContext.Korisniks.Single(x => x.IdKorisnik == id);
                og.KorisnickoIme = promjene.KorisnickoIme;
                og.Ime = promjene.Ime;
                og.Prezime = promjene.Prezime;
                og.IdPrivilegija = promjene.IdPrivilegija;
                dbContext.SaveChanges();
            }
        }

        public static void dbSpremiPromjeneZadatak(int id, Zadatak promjene)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var og = dbContext.Zadataks.Single(x => x.IdZadatak == id);
                og.NazivZadatak = promjene.NazivZadatak;
                og.Bodovi = promjene.Bodovi;
                og.MaxVrijeme = promjene.MaxVrijeme;
                og.TekstZadatak = promjene.TekstZadatak;
                og.Vidljivost = promjene.Vidljivost;
                dbContext.SaveChanges();
            }
        }
        public static int dbGetTocnaRjesenjaKorisnik(int idKorisnik)
        {
            using var dbContext = new CodeSharkDBContext();
            int res = 0;
            Korisnik k = dbGetKorisnikByIdKorisnik(idKorisnik);
            var rez = dbContext.Rezultats.Where(x => x.IdKorisnik == idKorisnik);
            foreach (var rezultat in rez)
            {
                Zadatak zad = dbContext.Zadataks.Single(x => x.IdZadatak == rezultat.IdZadatak);
                if (rezultat.DobiveniBodovi == zad.Bodovi)
                {
                    res++;
                }
            }
            return res;
        }
        public static int dbGetDjelomicnaRjesenjaKorisnik(int idKorisnik)
        {
            using var dbContext = new CodeSharkDBContext();
            int res = 0;
            Korisnik k = dbGetKorisnikByIdKorisnik(idKorisnik);
            var rez = dbContext.Rezultats.Where(x => x.IdKorisnik == idKorisnik);
            foreach (var rezultat in rez)
            {
                    res++;
            }
            return res;
        }
        public static List<int> dbGetIdKorisnikaRijesiliZadatak(int idZadatak)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                List<int> res = new List<int>();
                List<Rezultat> rezultati = dbContext.Rezultats.Where(x => x.IdZadatak == idZadatak).ToList();
                foreach (var rezultat in rezultati)
                {
                    res.Add(rezultat.IdKorisnik);
                }
                return res;
            }
        }
        public static Rezultat dbSpremiRezultat(Rezultat rez)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                dbContext.Rezultats.Add(rez);
                dbContext.Zadataks.Single(x => x.IdZadatak == rez.IdZadatak).Rezultats.Add(rez);
                dbContext.Korisniks.Single(x => x.IdKorisnik == rez.IdKorisnik).Rezultats.Add(rez);
                var i = dbContext.SaveChanges();
                return rez;
            }
        }
        public static void sbPoveziRezultatSaNatjecanjem(Rezultat rez, int idNat)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                RezultatNatjecanje rezNaz = new RezultatNatjecanje
                {
                    VrijemePredaje = rez.VrijemePredaje,
                    IdZadatak = rez.IdZadatak,
                    IdKorisnik = rez.IdKorisnik,
                    IdNatjecanje = idNat
                };
                dbContext.RezultatNatjecanjes.Add(rezNaz);
                dbContext.SaveChanges();
            }
        }

        public static IEnumerable<RezultatDto> dbGetRjesenjaZadatka(int idZadatak)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = new List<RezultatDto>();
                var rezultati =
                dbContext.Rezultats
                    .Where(x => x.IdZadatak == idZadatak)
                    .ToList();
                foreach (var res in rezultati)
                {
                    result.Add(new RezultatDto(res));
                }
                return result;
            }
        }
        public static IEnumerable<NatjecanjeKalendarDTO> dbGetKorisnikNapravioSvaNatjecanja(int idVoditelj)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = new List<NatjecanjeKalendarDTO>();
                var voditelj = dbContext.Korisniks.SingleOrDefault(x => x.IdKorisnik == idVoditelj);
                List<Natjecanje> natjecanja = dbContext.Natjecanjes.Where(x => x.IdKorisnik == idVoditelj).ToList();

                foreach (var nat in natjecanja)
                {
                    result.Add(new NatjecanjeKalendarDTO(nat, voditelj.KorisnickoIme));
                }
                return result;
            }
        }
        public static IEnumerable<NatjecanjeKalendarDTO> dbGetKorisnikNapravioNevirtualnaNatjecanja(int idVoditelj)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = new List<NatjecanjeKalendarDTO>();
                var voditelj = dbContext.Korisniks.SingleOrDefault(x => x.IdKorisnik == idVoditelj);
                List<Natjecanje> natjecanja = dbContext.Natjecanjes.Where(x => x.IdKorisnik == idVoditelj && x.JeVirtualno == false).ToList();

                foreach (var nat in natjecanja)
                {
                    result.Add(new NatjecanjeKalendarDTO(nat, voditelj.KorisnickoIme));
                }
                return result;
            }
        }
        public static IEnumerable<NatjecanjeKalendarDTO> dbGetNatjecanja()
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = new List<NatjecanjeKalendarDTO>();
                var natjecanja = dbContext.Natjecanjes.Where(x => x.JeVirtualno == false);
                foreach (var nat in natjecanja)
                {
                    var voditelj = dbContext.Korisniks.SingleOrDefault(x => x.IdKorisnik == nat.IdKorisnik);
                    result.Add(new NatjecanjeKalendarDTO(nat, voditelj.KorisnickoIme));
                }
                return result;

            }
        }

        public static bool dbPostojiNatjecanje(int id)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                return dbContext.Natjecanjes.Any(x => x.IdNatjecanje == id);
            }
        }
        public static Natjecanje dbGetNatjecanjeById(int id)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Natjecanjes.Single(x => x.IdNatjecanje == id);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static IEnumerable<Zadatak> dbGetZadatkeIzNatjecanja(int idNatjecanje)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var result = new List<Zadatak>();
                var zadaciIdList = dbContext.ZadUnats.Where(x => x.IdNatjecanje == idNatjecanje).Select(x => x.IdZadatak).ToList();
                foreach (int zadatakID in zadaciIdList)
                {
                    result.Add(dbContext.Zadataks.Single(x => x.IdZadatak == zadatakID));
                }
                return result;
            }
        }

        public static void dbNovoNatjecanje(Natjecanje novoNatjecanje)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    dbContext.Natjecanjes.Add(novoNatjecanje);
                    var i = dbContext.SaveChanges();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static void dbSpremiNatjecanje(int id, Natjecanje natjecanje)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    var og = dbContext.Natjecanjes.Single(x => x.IdNatjecanje == id);
                    og.NazivNatjecanje = natjecanje.NazivNatjecanje;
                    og.VrijemePocetak = natjecanje.VrijemePocetak;
                    og.VrijemeKraj = natjecanje.VrijemeKraj;
                    og.PeharSlika = natjecanje.PeharSlika;
                    //og.BrojZadataka = natjecanje.BrojZadataka;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }
        public static Natjecanje? dbGetNatjecanjeByNaziv(string nazivNatjecanja)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    return dbContext.Natjecanjes.Single(x => x.NazivNatjecanje == nazivNatjecanja);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }
        public static void dbInsertZadatciUNatjecanje(int idNatjecanje, List<ZadUnat> zadatciUNatjecanju)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    var zadatci = dbContext.ZadUnats.Where(x => x.IdNatjecanje == idNatjecanje);
                    if (zadatci == null)
                    {
                        foreach (ZadUnat zadUnat in zadatciUNatjecanju)
                        {
                            dbContext.ZadUnats.Add(zadUnat);
                        }
                    }
                    else
                    {
                        foreach (ZadUnat zadUnat in zadatciUNatjecanju)
                        {
                            if (!zadatci.Contains(zadUnat))
                            {
                                dbContext.ZadUnats.Add(zadUnat);
                            }
                        }
                    }

                    var i = dbContext.SaveChanges();
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                throw;
            }
        }

        public static IEnumerable<Statistika> CalculateStats(int idNatjecanje)
        {
            try
            {
                using var dbContext = new CodeSharkDBContext();
                var natjecanje = dbContext.Natjecanjes.Find(idNatjecanje);
                var mess = (from nat in dbContext.Natjecanjes
                        join rezNat in dbContext.RezultatNatjecanjes
                            on nat.IdNatjecanje equals rezNat.IdNatjecanje
                        join kor in dbContext.Korisniks
                            on rezNat.IdKorisnik equals kor.IdKorisnik
                        join rez in dbContext.Rezultats
                            on new {rezNat.IdZadatak, rezNat.IdKorisnik, rezNat.VrijemePredaje} equals new {rez.IdZadatak, rez.IdKorisnik, rez.VrijemePredaje}
                        join zad in dbContext.Zadataks
                            on rezNat.IdZadatak equals zad.IdZadatak
                        where nat.IdNatjecanje == idNatjecanje
                        select new KorisnikRezultatDTO(kor.IdKorisnik, kor.KorisnickoIme, zad.IdZadatak, zad.Bodovi,
                            rez.DobiveniBodovi.Value, rez.VrijemePredaje, nat.VrijemeKraj)
                    ).ToList();
                var korisnici = mess
                    .Select(k => new {k.KorisnickoIme, k.IdKorisnik})
                    .Distinct()
                    .ToList();
                var stats = new List<Statistika>();
                foreach (var kor in korisnici)
                {
                    var korisnik = kor.KorisnickoIme;
                    //var stat = new Statistika(idNatjecanje, kor.IdKorisnik, kor.KorisnickoIme, 0);
                    var stat = new Statistika
                    {
                        IdNatjecanje = idNatjecanje,
                        IdKorisnik = kor.IdKorisnik,
                        KorisnickoIme = kor.KorisnickoIme,
                        Bodovi = 0
                    };
                    var rezultati = mess.Where(m => m.KorisnickoIme == korisnik).ToList();
                    var zadatci = rezultati.Select(rez => rez.IdZadatak).Distinct().ToList();
                    var najboljiRezultati =
                        zadatci
                            .Select(zadatak => rezultati.Where(rez => rez.IdZadatak == zadatak)
                                .OrderByDescending(rez => rez.DobiveniBodovi)
                                .First())
                            .ToList();

                    foreach (var rezultat in najboljiRezultati)
                    {
                        stat.Bodovi += rezultat.DobiveniBodovi;
                    }

                    if (najboljiRezultati.Count == natjecanje.BrojZadataka && najboljiRezultati.All(rez => rez.DobiveniBodovi == rez.Bodovi))
                    {
                        var zadnjiRezultat = rezultati.OrderByDescending(rez => rez.VrijemePredaje).First();
                        var bonus = Convert.ToInt32((zadnjiRezultat.VrijemeKraj - zadnjiRezultat.VrijemePredaje).TotalMilliseconds) * decimal.Divide(1, 1000000);
                        stat.Bodovi += bonus;
                    }
                    
                    stats.Add(stat);
                }

                return stats;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        public static IEnumerable<StatsDTO> GetStats(int idNatjecanje)
        {
            using var dbContext = new CodeSharkDBContext();
            var natjecanje = dbContext.Natjecanjes.Find(idNatjecanje);
            if (natjecanje is null)
            {
                throw new ArgumentException("To natjecanje ne postoji");
            }

            if (natjecanje.VrijemeKraj >= DateTime.Now)
            {
                throw new ArgumentException("Natjecanje još nije završilo");
            }
                
            var stats = dbContext.Statistikas.Where(stat => stat.IdNatjecanje == idNatjecanje).ToList();
            if (stats.Count == 0)
            {
                stats = CalculateStats(idNatjecanje).ToList();
                foreach (var stat in stats)
                {
                    dbContext.Statistikas.Add(stat);
                }

                var winnerPoints = stats.OrderByDescending(stat => stat.Bodovi).First().Bodovi;
                var winners = stats.Where(stat => stat.Bodovi == winnerPoints);
                foreach (var winner in winners)
                {
                    dbContext.Pobjedniks.Add(new Pobjednik
                    {
                        IdKorisnik = winner.IdKorisnik,
                        IdNatjecanje = idNatjecanje
                    });
                }
                dbContext.SaveChanges();

                //postavi zadatke javnim
                try
                {
                    dbPostaviZadatkeIzNatjecanjaVidljivim(idNatjecanje);
                }
                catch(Exception)
                {
                    Console.WriteLine("Zadaci se nisu mogli postaviti javnim.");
                }
            }

            return stats.Select(stat => new StatsDTO(stat)).ToList();
        }

        public static List<int> dbVratiIdPobjedenihNatjecanja(int idKorisnik)
        {
            using (var dbContext = new CodeSharkDBContext())
            {
                var idNatPobjeda = dbContext.Pobjedniks.Where(x => x.IdKorisnik == idKorisnik).ToList();
                List<int> natIds = new List<int>();
                foreach (var pobjeda in idNatPobjeda)
                {
                    natIds.Add(pobjeda.IdNatjecanje);
                }
                return natIds;
            }
        }

        public static void dbPostaviZadatkeIzNatjecanjaVidljivim(int idNatjecanje)
        {
            List<int> zadaciIdList;
            using (var dbContext = new CodeSharkDBContext())
            {
                zadaciIdList = dbContext.ZadUnats.Where(x => x.IdNatjecanje == idNatjecanje).Select(x => x.IdZadatak).ToList();
            }
            foreach (int zadatakID in zadaciIdList)
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    var zadatak = dbContext.Zadataks.Single(x => x.IdZadatak == zadatakID);
                    zadatak.Vidljivost = true;
                    dbContext.SaveChanges();
                }
            }
        }
        public static IEnumerable<Zadatak> dbGetZadatakZaVirtualno(int tezina)
        {
            List<Zadatak> sviZad = dbGetSviVidljiviZadaci();
            List<Zadatak> rezultat = new List<Zadatak>();
            Random random = new Random();

            List<Zadatak> laki = sviZad.Where(x => x.Bodovi >= 0 & x.Bodovi < 6).ToList();
            List<Zadatak> srednji = sviZad.Where(x => x.Bodovi >= 6 & x.Bodovi < 10).ToList();

            int randomnumber;
            int i = 0;
            while (i < 2)
            {
                randomnumber = random.Next(0, laki.Count);
                var zadatak = laki[randomnumber];

                if (!rezultat.Contains(zadatak))
                {
                    rezultat.Add(zadatak);
                    i++;
                }

            }
            // 2 laka + 2 srednja
            if (tezina == 1)
            {
                i = 0;
                while (i < 2)
                {
                    randomnumber = random.Next(0, srednji.Count);
                    var zadatak = srednji[randomnumber];

                    if (!rezultat.Contains(zadatak))
                    {
                        rezultat.Add(zadatak);
                        i++;
                    }
                }
            }
            // 2 laka + 1 srednji + 1 tezi
            else if (tezina == 2)
            {
                i = 0;
                while (i < 1)
                {
                    randomnumber = random.Next(0, srednji.Count);
                    var zadatak = srednji[randomnumber];

                    if (!rezultat.Contains(zadatak))
                    {
                        rezultat.Add(zadatak);
                        i++;
                    }
                }

                List<Zadatak> tezi= sviZad.Where(x => x.Bodovi >= 10).ToList();

                i = 0;
                while (i < 1)
                {
                    randomnumber = random.Next(0, tezi.Count);
                    var zadatak = tezi[randomnumber];

                    if (!rezultat.Contains(zadatak))
                    {
                        rezultat.Add(zadatak);
                        i++;
                    }
                }
            }
            // 2 laka + 2 srednja + 1 tezi
            else if (tezina == 3)
            {
                i = 0;
                while (i < 2)
                {
                    randomnumber = random.Next(0, srednji.Count);
                    var zadatak = srednji[randomnumber];

                    if (!rezultat.Contains(zadatak))
                    {
                        rezultat.Add(zadatak);
                        i++;
                    }
                }

                List<Zadatak> tezi = sviZad.Where(x => x.Bodovi >= 10).ToList();

                i = 0;
                while (i < 1)
                {
                    randomnumber = random.Next(0, tezi.Count);
                    var zadatak = tezi[randomnumber];

                    if (!rezultat.Contains(zadatak))
                    {
                        rezultat.Add(zadatak);
                        i++;
                    }
                }
            }
            return rezultat;
        }
    }
}
