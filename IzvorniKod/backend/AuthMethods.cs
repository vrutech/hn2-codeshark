﻿#nullable enable
using System;
using System.Security.Authentication;
using System.Security.Claims;
using System.Security.Principal;

namespace backend
{
    public static class AuthMethods
    {
        public static bool IsUserAdmin(IIdentity? identity)
        {
            return GetTokenPrivilege(identity) == 1;
        }
        
        public static bool IsUserHimselfOrAdmin(IIdentity? identity, int id)
        {
            return GetTokenId(identity) == id || IsUserAdmin(identity);
        }
        
        public static bool IsUserHimselfOrAdmin(IIdentity? identity, string username)
        {
            return GetTokenUsername(identity) == username || IsUserAdmin(identity);
        }

        public static int GetTokenId(IIdentity? identity)
        {
            if (identity is not ClaimsIdentity claimsIdentity)
            {
                throw new AuthenticationException();
            }
            
            var id = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? throw new AuthenticationException();
            
            return int.Parse(id);
        }

        public static string GetTokenUsername(IIdentity? identity)
        {
            if (identity is not ClaimsIdentity claimsIdentity)
            {
                throw new AuthenticationException();
            }

            var username = claimsIdentity.FindFirst("username")?.Value ?? throw new AuthenticationException();

            return username;
        }
        
        public static int GetTokenPrivilege(IIdentity? identity)
        {
            if (identity is not ClaimsIdentity claimsIdentity)
            {
                throw new AuthenticationException();
            }

            var privilege = claimsIdentity.FindFirst("privilege")?.Value ?? throw new AuthenticationException();

            return int.Parse(privilege);
        }
    }
}