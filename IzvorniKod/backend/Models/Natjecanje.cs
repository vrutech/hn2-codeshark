﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Natjecanje
    {
        public Natjecanje()
        {
            RezultatNatjecanjes = new HashSet<RezultatNatjecanje>();
            ZadUnats = new HashSet<ZadUnat>();
        }

        public int IdNatjecanje { get; set; }
        public DateTime VrijemePocetak { get; set; }
        public DateTime VrijemeKraj { get; set; }
        public byte[] PeharSlika { get; set; }
        public int BrojZadataka { get; set; }
        public bool JeVirtualno { get; set; }
        public int IdKorisnik { get; set; }
        public string NazivNatjecanje { get; set; }

        public virtual Korisnik IdKorisnikNavigation { get; set; }
        public virtual ICollection<RezultatNatjecanje> RezultatNatjecanjes { get; set; }
        public virtual ICollection<ZadUnat> ZadUnats { get; set; }
    }
}
