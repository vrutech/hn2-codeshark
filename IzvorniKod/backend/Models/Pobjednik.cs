﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Pobjednik
    {
        public int IdNatjecanje { get; set; }
        public int IdKorisnik { get; set; }
    }
}
