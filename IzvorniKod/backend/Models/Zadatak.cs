﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Zadatak
    {
        public Zadatak()
        {
            Rezultats = new HashSet<Rezultat>();
            TestniPodacis = new HashSet<TestniPodaci>();
            ZadUnats = new HashSet<ZadUnat>();
        }

        public int IdZadatak { get; set; }
        public string NazivZadatak { get; set; }
        public int Bodovi { get; set; }
        public TimeSpan MaxVrijeme { get; set; }
        public string TekstZadatak { get; set; }
        public bool Vidljivost { get; set; }
        public int IdKorisnik { get; set; }

        public virtual Korisnik IdKorisnikNavigation { get; set; }
        public virtual ICollection<Rezultat> Rezultats { get; set; }
        public virtual ICollection<TestniPodaci> TestniPodacis { get; set; }
        public virtual ICollection<ZadUnat> ZadUnats { get; set; }
    }
}
