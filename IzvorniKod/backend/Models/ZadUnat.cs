﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class ZadUnat
    {
        public int IdNatjecanje { get; set; }
        public int IdZadatak { get; set; }

        public virtual Natjecanje IdNatjecanjeNavigation { get; set; }
        public virtual Zadatak IdZadatakNavigation { get; set; }
    }
}
