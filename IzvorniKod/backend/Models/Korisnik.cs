﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Korisnik
    {
        public Korisnik()
        {
            Natjecanjes = new HashSet<Natjecanje>();
            Rezultats = new HashSet<Rezultat>();
            Zadataks = new HashSet<Zadatak>();
        }

        public int IdKorisnik { get; set; }
        public string KorisnickoIme { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool PotvrdjenMail { get; set; }
        public int IdPrivilegija { get; set; }
        public bool PotvrdjenaPrivilegija { get; set; }
        public string Uuid { get; set; }
        public byte[] ProfilSlika { get; set; }

        public virtual Privilegija IdPrivilegijaNavigation { get; set; }
        public virtual ICollection<Natjecanje> Natjecanjes { get; set; }
        public virtual ICollection<Rezultat> Rezultats { get; set; }
        public virtual ICollection<Zadatak> Zadataks { get; set; }
    }
}
