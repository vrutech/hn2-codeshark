﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class RezultatNatjecanje
    {
        public DateTime VrijemePredaje { get; set; }
        public int IdZadatak { get; set; }
        public int IdKorisnik { get; set; }
        public int IdNatjecanje { get; set; }

        public virtual Natjecanje IdNatjecanjeNavigation { get; set; }
        public virtual Rezultat Rezultat { get; set; }
    }
}
