﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class TestniPodaci
    {
        public byte[] Ulaz { get; set; }
        public byte[] Izlaz { get; set; }
        public int IdTesniPodaci { get; set; }
        public int IdZadatak { get; set; }

        public virtual Zadatak IdZadatakNavigation { get; set; }
    }
}
