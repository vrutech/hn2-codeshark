﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Statistika
    {
        public int IdNatjecanje { get; set; }
        public int IdKorisnik { get; set; }
        public string KorisnickoIme { get; set; }
        public decimal Bodovi { get; set; }
    }
}
