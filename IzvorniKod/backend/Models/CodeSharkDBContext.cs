﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace backend.Models
{
    public partial class CodeSharkDBContext : DbContext
    {
        public CodeSharkDBContext()
        {
        }

        public CodeSharkDBContext(DbContextOptions<CodeSharkDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Korisnik> Korisniks { get; set; }
        public virtual DbSet<Natjecanje> Natjecanjes { get; set; }
        public virtual DbSet<Pobjednik> Pobjedniks { get; set; }
        public virtual DbSet<Privilegija> Privilegijas { get; set; }
        public virtual DbSet<Rezultat> Rezultats { get; set; }
        public virtual DbSet<RezultatNatjecanje> RezultatNatjecanjes { get; set; }
        public virtual DbSet<Statistika> Statistikas { get; set; }
        public virtual DbSet<TestniPodaci> TestniPodacis { get; set; }
        public virtual DbSet<ZadUnat> ZadUnats { get; set; }
        public virtual DbSet<Zadatak> Zadataks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(AppSettingsProvider.Configuration["ConnectionString"]);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Korisnik>(entity =>
            {
                entity.HasKey(e => e.IdKorisnik)
                    .HasName("PK__Korisnik__80AA40631B9DCC9A");

                entity.ToTable("Korisnik");

                entity.HasIndex(e => e.KorisnickoIme, "UQ__Korisnik__46BA678E1FECB37A")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "uniqueEmail")
                    .IsUnique();

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.IdPrivilegija).HasColumnName("idPrivilegija");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ime");

                entity.Property(e => e.KorisnickoIme)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("korisnickoIme");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("password");

                entity.Property(e => e.PotvrdjenMail).HasColumnName("potvrdjenMail");

                entity.Property(e => e.PotvrdjenaPrivilegija).HasColumnName("potvrdjenaPrivilegija");

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("prezime");

                entity.Property(e => e.ProfilSlika).HasColumnName("profilSlika");

                entity.Property(e => e.Uuid)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .HasColumnName("UUID")
                    .IsFixedLength(true);

                entity.HasOne(d => d.IdPrivilegijaNavigation)
                    .WithMany(p => p.Korisniks)
                    .HasForeignKey(d => d.IdPrivilegija)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Korisnik__idPriv__6E01572D");
            });

            modelBuilder.Entity<Natjecanje>(entity =>
            {
                entity.HasKey(e => e.IdNatjecanje)
                    .HasName("PK__Natjecan__E551E72B52416BAD");

                entity.ToTable("Natjecanje");

                entity.Property(e => e.IdNatjecanje).HasColumnName("idNatjecanje");

                entity.Property(e => e.BrojZadataka).HasColumnName("brojZadataka");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.JeVirtualno).HasColumnName("jeVirtualno");

                entity.Property(e => e.NazivNatjecanje)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nazivNatjecanje");

                entity.Property(e => e.PeharSlika).HasColumnName("peharSlika");

                entity.Property(e => e.VrijemeKraj)
                    .HasColumnType("datetime")
                    .HasColumnName("vrijemeKraj");

                entity.Property(e => e.VrijemePocetak)
                    .HasColumnType("datetime")
                    .HasColumnName("vrijemePocetak");

                entity.HasOne(d => d.IdKorisnikNavigation)
                    .WithMany(p => p.Natjecanjes)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Natjecanj__idKor__6EF57B66");
            });

            modelBuilder.Entity<Pobjednik>(entity =>
            {
                entity.HasKey(e => new { e.IdNatjecanje, e.IdKorisnik })
                    .HasName("PK__pobjedni__CD5B432D32860A59");

                entity.ToTable("pobjednik");

                entity.Property(e => e.IdNatjecanje).HasColumnName("idNatjecanje");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");
            });

            modelBuilder.Entity<Privilegija>(entity =>
            {
                entity.HasKey(e => e.IdPrivilegija)
                    .HasName("PK__Privileg__E0E7D5D59059D15F");

                entity.ToTable("Privilegija");

                entity.Property(e => e.IdPrivilegija).HasColumnName("idPrivilegija");

                entity.Property(e => e.NazivPrivilegija)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("nazivPrivilegija");
            });

            modelBuilder.Entity<Rezultat>(entity =>
            {
                entity.HasKey(e => new { e.VrijemePredaje, e.IdZadatak, e.IdKorisnik })
                    .HasName("PK__Rezultat__CA0AF029C41A468F");

                entity.ToTable("Rezultat");

                entity.Property(e => e.VrijemePredaje)
                    .HasColumnType("datetime")
                    .HasColumnName("vrijemePredaje")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdZadatak).HasColumnName("idZadatak");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.DobiveniBodovi)
                    .HasColumnType("decimal(5, 2)")
                    .HasColumnName("dobiveniBodovi");

                entity.Property(e => e.PredanoRjesenje).HasColumnName("predanoRjesenje");

                entity.Property(e => e.Trajanje).HasColumnName("trajanje");

                entity.HasOne(d => d.IdKorisnikNavigation)
                    .WithMany(p => p.Rezultats)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rezultat__idKori__6FE99F9F");

                entity.HasOne(d => d.IdZadatakNavigation)
                    .WithMany(p => p.Rezultats)
                    .HasForeignKey(d => d.IdZadatak)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Rezultat__idZada__70DDC3D8");
            });

            modelBuilder.Entity<RezultatNatjecanje>(entity =>
            {
                entity.HasKey(e => new { e.VrijemePredaje, e.IdZadatak, e.IdKorisnik, e.IdNatjecanje })
                    .HasName("PK__Rezultat__B8A4A537366B5928");

                entity.ToTable("RezultatNatjecanje");

                entity.Property(e => e.VrijemePredaje)
                    .HasColumnType("datetime")
                    .HasColumnName("vrijemePredaje");

                entity.Property(e => e.IdZadatak).HasColumnName("idZadatak");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.IdNatjecanje).HasColumnName("idNatjecanje");

                entity.HasOne(d => d.IdNatjecanjeNavigation)
                    .WithMany(p => p.RezultatNatjecanjes)
                    .HasForeignKey(d => d.IdNatjecanje)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RezultatN__idNat__71D1E811");

                entity.HasOne(d => d.Rezultat)
                    .WithMany(p => p.RezultatNatjecanjes)
                    .HasForeignKey(d => new { d.VrijemePredaje, d.IdZadatak, d.IdKorisnik })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RezultatNatjecan__72C60C4A");
            });

            modelBuilder.Entity<Statistika>(entity =>
            {
                entity.HasKey(e => new { e.IdNatjecanje, e.IdKorisnik })
                    .HasName("PK__statisti__CD5B432DF5DA7104");

                entity.ToTable("statistika");

                entity.Property(e => e.IdNatjecanje).HasColumnName("idNatjecanje");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.Bodovi)
                    .HasColumnType("decimal(10, 6)")
                    .HasColumnName("bodovi");

                entity.Property(e => e.KorisnickoIme)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("korisnickoIme");
            });

            modelBuilder.Entity<TestniPodaci>(entity =>
            {
                entity.HasKey(e => new { e.IdTesniPodaci, e.IdZadatak })
                    .HasName("PK__TestniPo__CDCE101FDA1FA0F2");

                entity.ToTable("TestniPodaci");

                entity.Property(e => e.IdTesniPodaci).HasColumnName("idTesniPodaci");

                entity.Property(e => e.IdZadatak).HasColumnName("idZadatak");

                entity.Property(e => e.Izlaz).HasColumnName("izlaz");

                entity.Property(e => e.Ulaz).HasColumnName("ulaz");

                entity.HasOne(d => d.IdZadatakNavigation)
                    .WithMany(p => p.TestniPodacis)
                    .HasForeignKey(d => d.IdZadatak)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TestniPod__idZad__73BA3083");
            });

            modelBuilder.Entity<ZadUnat>(entity =>
            {
                entity.HasKey(e => new { e.IdNatjecanje, e.IdZadatak })
                    .HasName("PK__ZadUNat__D50DC7ED0666D423");

                entity.ToTable("ZadUNat");

                entity.Property(e => e.IdNatjecanje).HasColumnName("idNatjecanje");

                entity.Property(e => e.IdZadatak).HasColumnName("idZadatak");

                entity.HasOne(d => d.IdNatjecanjeNavigation)
                    .WithMany(p => p.ZadUnats)
                    .HasForeignKey(d => d.IdNatjecanje)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ZadUNat__idNatje__75A278F5");

                entity.HasOne(d => d.IdZadatakNavigation)
                    .WithMany(p => p.ZadUnats)
                    .HasForeignKey(d => d.IdZadatak)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ZadUNat__idZadat__76969D2E");
            });

            modelBuilder.Entity<Zadatak>(entity =>
            {
                entity.HasKey(e => e.IdZadatak)
                    .HasName("PK__Zadatak__05C20C62FB025215");

                entity.ToTable("Zadatak");

                entity.Property(e => e.IdZadatak).HasColumnName("idZadatak");

                entity.Property(e => e.Bodovi).HasColumnName("bodovi");

                entity.Property(e => e.IdKorisnik).HasColumnName("idKorisnik");

                entity.Property(e => e.MaxVrijeme).HasColumnName("maxVrijeme");

                entity.Property(e => e.NazivZadatak)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nazivZadatak");

                entity.Property(e => e.TekstZadatak)
                    .IsRequired()
                    .HasMaxLength(8000)
                    .IsUnicode(false)
                    .HasColumnName("tekstZadatak");

                entity.Property(e => e.Vidljivost).HasColumnName("vidljivost");

                entity.HasOne(d => d.IdKorisnikNavigation)
                    .WithMany(p => p.Zadataks)
                    .HasForeignKey(d => d.IdKorisnik)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Zadatak__idKoris__74AE54BC");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
