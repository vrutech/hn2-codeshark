﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Rezultat
    {
        public Rezultat()
        {
            RezultatNatjecanjes = new HashSet<RezultatNatjecanje>();
        }

        public decimal? DobiveniBodovi { get; set; }
        public DateTime VrijemePredaje { get; set; }
        public byte[] PredanoRjesenje { get; set; }
        public TimeSpan Trajanje { get; set; }
        public int IdZadatak { get; set; }
        public int IdKorisnik { get; set; }

        public virtual Korisnik IdKorisnikNavigation { get; set; }
        public virtual Zadatak IdZadatakNavigation { get; set; }
        public virtual ICollection<RezultatNatjecanje> RezultatNatjecanjes { get; set; }
    }
}
