﻿using System;
using System.Collections.Generic;

#nullable disable

namespace backend.Models
{
    public partial class Privilegija
    {
        public Privilegija()
        {
            Korisniks = new HashSet<Korisnik>();
        }

        public int IdPrivilegija { get; set; }
        public string NazivPrivilegija { get; set; }

        public virtual ICollection<Korisnik> Korisniks { get; set; }
    }
}
