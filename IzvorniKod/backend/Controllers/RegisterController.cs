﻿using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        [HttpPost]
        [Route("/api/v1/register")]
        public ActionResult Post([FromBody] Korisnik newUser)
        {
            try
            {
                if (DbMethods.dbGetKorisnikByEmail(newUser.Email) != null)
                {
                    return BadRequest("Korisnik s navedenim e-mailom već postoji");
                }
                if (DbMethods.dbGetKorisnikByKorisnickoIme(newUser.KorisnickoIme) != null)
                {
                    return BadRequest("Korisnik s navedenim korisničkim imenom već postoji");
                }
                newUser.Uuid = Guid.NewGuid().ToString();
                newUser.PotvrdjenMail = false;
                if (newUser.IdPrivilegija == 3)
                {
                    newUser.PotvrdjenaPrivilegija = true;
                }
                var hasher = SHA256.Create();
                var hashedPassword = Convert.ToHexString(hasher.ComputeHash(Encoding.UTF8.GetBytes(newUser.Password))).ToLower();
                newUser.Password = hashedPassword;

                DbMethods.dbNoviKorisnik(newUser);
                return Ok("Pohranjen novi korisnik");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }
    }
}
