using backend.DTOs;
using backend.Interfaces;
using backend.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class NatjecanjeController : Controller
    {
        private readonly ITokenService _tokenService;

        public NatjecanjeController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }


        [HttpGet]
        [Route("dohvatiNatjecanje/{id}")]
        public ActionResult<NatjecanjeDTO> GetNatjecanje(int id)
        {
            try
            {
                Natjecanje trazenoNatjecanje;
                try
                {
                    trazenoNatjecanje = DbMethods.dbGetNatjecanjeById(id);
                }
                catch (Exception)
                {
                    return BadRequest("Traženo natjecanje ne postoji");
                }

                DateTime trenutnoVrijeme = DateTime.Now.AddHours(1);
                int stanje = 0;
                if (trenutnoVrijeme < trazenoNatjecanje.VrijemePocetak)
                {
                    stanje = NatjecanjeDTO.BEFORE;
                }
                else if (trenutnoVrijeme >= trazenoNatjecanje.VrijemePocetak && trenutnoVrijeme <= trazenoNatjecanje.VrijemeKraj)
                {
                    stanje = NatjecanjeDTO.IN_PROGRESS;
                }
                else if (trenutnoVrijeme > trazenoNatjecanje.VrijemeKraj)
                {
                    stanje = NatjecanjeDTO.AFTER;
                }
                else
                {
                    return BadRequest("Time error!");
                }

                IEnumerable<Zadatak> zadaci = null;
                if (stanje == NatjecanjeDTO.IN_PROGRESS)
                {
                    zadaci = DbMethods.dbGetZadatkeIzNatjecanja(trazenoNatjecanje.IdNatjecanje);
                }

                IEnumerable<StatsDTO> statistika = null;
                if (stanje == NatjecanjeDTO.AFTER)
                {
                    statistika = DbMethods.GetStats(trazenoNatjecanje.IdNatjecanje);
                }

                NatjecanjeDTO natjecanje = new NatjecanjeDTO(
                                                trazenoNatjecanje,
                                                DbMethods.dbGetKorisnikByIdKorisnik(trazenoNatjecanje.IdKorisnik),
                                                stanje,
                                                zadaci,
                                                statistika);
                return natjecanje;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }

        [HttpPost]
        [Route("novoNatjecanje")]
        public ActionResult Post([FromBody] NovoNatjecanjeDTO newNatjecanj)
        {
            try
            {
                try
                {
                    if (DbMethods.dbGetNatjecanjeByNaziv(newNatjecanj.NazivNatjecanje) != null)
                    {
                        return BadRequest("Natjecanje s navedenim naslovom već postoji");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Natjecanje sa zadanim naslovom ne postoji. Nastavi upload");
                }

                if (DateTime.Compare(newNatjecanj.VrijemePocetak, DateTime.Now.AddHours(1)) > 0 && DateTime.Compare(newNatjecanj.VrijemeKraj, newNatjecanj.VrijemePocetak) > 0)
                {
                    DbMethods.dbNovoNatjecanje(new Natjecanje
                    {
                        NazivNatjecanje = newNatjecanj.NazivNatjecanje,
                        VrijemePocetak = newNatjecanj.VrijemePocetak,
                        VrijemeKraj = newNatjecanj.VrijemeKraj,
                        PeharSlika = newNatjecanj.PeharSlika,
                        BrojZadataka = newNatjecanj.ListaZadataka.Length,
                        JeVirtualno = newNatjecanj.JeVirtualno,
                        IdKorisnik = newNatjecanj.IdKorisnik
                    });

                    //dodavanje zadataka
                    int dodanoNatjecanjeId = DbMethods.dbGetNatjecanjeByNaziv(newNatjecanj.NazivNatjecanje).IdNatjecanje;
                    var zadUnats = new List<ZadUnat>();

                    foreach(int zadatakId in newNatjecanj.ListaZadataka)
                    {
                        zadUnats.Add(new ZadUnat
                        {
                            IdNatjecanje = dodanoNatjecanjeId,
                            IdZadatak = zadatakId
                        });
                    }

                    DbMethods.dbInsertZadatciUNatjecanje(dodanoNatjecanjeId, zadUnats);

                    return Ok("Pohranjeno novo natjecanje sa zadatcima");
                }
                else
                {
                    return BadRequest("Neispravno vrijeme!");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message + error.StackTrace);
            }
        }

        [HttpPost]
        [Route("promijeniNatjecanje/{id}")]
        public ActionResult<Natjecanje> SpremiPromjeneNatjecanje(int id, [FromBody] EditNatjecanjeDTO natjecanje)
        {
            try
            {
                if (DateTime.Compare(natjecanje.VrijemePocetak, DateTime.Now.AddHours(1)) > 0)
                {
                    Natjecanje original = DbMethods.dbGetNatjecanjeById(id);

                    if (original == null)
                    {
                        return BadRequest("Natjecanje s navedenim id ne postoji!");
                    }

                    if (DateTime.Compare(original.VrijemePocetak, DateTime.Now.AddHours(1)) > 0)
                    {
                        /*
                        Natjecanje istiNaziv = DbMethods.dbGetNatjecanjeByNaziv(natjecanje.NazivNatjecanje);

                        if (istiNaziv.IdNatjecanje != original.IdNatjecanje)
                        {
                            return BadRequest("Natjecanje koje se isto zove vec postoji!");
                        }
                        */

                        if (original.NazivNatjecanje != natjecanje.NazivNatjecanje)
                        {
                            try
                            {
                                if (DbMethods.dbGetNatjecanjeByNaziv(natjecanje.NazivNatjecanje) != null)
                                {
                                    return BadRequest("Natjecanje s navedenim naslovom već postoji");
                                }
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Natjecanje sa zadanim naslovom ne postoji. Nastavi upload");
                            }
                        }

                        original.NazivNatjecanje = natjecanje.NazivNatjecanje;
                        original.VrijemePocetak = natjecanje.VrijemePocetak;
                        original.VrijemeKraj = natjecanje.VrijemeKraj;
                        original.PeharSlika = natjecanje.PeharSlika;
                        //original.BrojZadataka = natjecanje.ListZadataka.Length;
                        /*
                        var zadUnats = new List<ZadUnat>();

                        foreach (int zadatakId in natjecanje.ListZadataka)
                        {
                            zadUnats.Add(new ZadUnat
                            {
                                IdNatjecanje = original.IdNatjecanje,
                                IdZadatak = zadatakId
                            });
                        }
                        
                        DbMethods.dbInsertZadatciUNatjecanje(original.IdNatjecanje, zadUnats);
                        */
                        DbMethods.dbSpremiNatjecanje(id, original);
                        return Ok("Pohranjene promjene na natjecanju");
                        //return original;
                    } 
                    else
                    {
                        return BadRequest("Natjecanje se ne moze mijenjati nakon sto je pocelo!");
                    }
                }
                else
                {
                    return BadRequest("Neispravno vrijeme!");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }

        [HttpGet]
        [Route("GetSvaNatjecanja")]
        public ActionResult<IEnumerable<NatjecanjeKalendarDTO>> GetSvaNatjecanja()
        {
            try
            {
                return DbMethods.dbGetNatjecanja().ToList();
            }
            catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }
        [HttpGet]
        [Route("GetStats/{idNatjecanja:int}")]
        public ActionResult<IEnumerable<StatsDTO>> GetStats(int idNatjecanja)
        {
            try
            {
                return Ok(DbMethods.GetStats(idNatjecanja));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("getVirtualnoNatjecanjeIzZavrsenog/{id}")]
        public ActionResult<NatjecanjeDTO> GetVirtualnoIzZavresnogNat(int id)
        {
            try
            {
                Natjecanje trazenoNatjecanje;
                try
                {
                    trazenoNatjecanje = DbMethods.dbGetNatjecanjeById(id);
                }
                catch (Exception)
                {
                    return BadRequest("Traženo natjecanje ne postoji");
                }

                DateTime trenutnoVrijeme = DateTime.Now.AddHours(1);
                if (trenutnoVrijeme < trazenoNatjecanje.VrijemeKraj)
                {
                    return BadRequest("Ne može se generirati virtualno natjecanje, jer traženo natjecanje još nije završilo.");
                }

                var zadaci = DbMethods.dbGetZadatkeIzNatjecanja(trazenoNatjecanje.IdNatjecanje);
                var statistika = DbMethods.GetStats(trazenoNatjecanje.IdNatjecanje);

                NatjecanjeDTO natjecanje = new NatjecanjeDTO(
                                    trazenoNatjecanje,
                                    DbMethods.dbGetKorisnikByIdKorisnik(trazenoNatjecanje.IdKorisnik),
                                    NatjecanjeDTO.IN_PROGRESS,
                                    zadaci,
                                    statistika);
                return natjecanje;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }

        [HttpGet]
        [Route("getRandomVirtualnoNatjecanje/{tezina}")]
        public ActionResult<RandomVirtualnoNatjecanjeDTO> GetRandomVirtualnoNatjecanje(int tezina)
        {
            try
            {
                var natjecanje = new RandomVirtualnoNatjecanjeDTO();
                natjecanje.VrijemePocetak = DateTime.Now.AddHours(1);

                if (tezina == 1)
                {
                    natjecanje.NazivNatjecnaje = "Laganica random virtualno natjecanje";
                    natjecanje.VrijemeKraj = natjecanje.VrijemePocetak.AddMinutes(30);
                }
                else if (tezina == 2)
                {
                    natjecanje.NazivNatjecnaje = "Srednje random virtualno natjecanje";
                    natjecanje.VrijemeKraj = natjecanje.VrijemePocetak.AddMinutes(60);
                }
                else if (tezina == 3)
                {
                    natjecanje.NazivNatjecnaje = "Tesko random virtualno natjecanje";
                    natjecanje.VrijemeKraj = natjecanje.VrijemePocetak.AddMinutes(90);
                }
                else
                {
                    return BadRequest("Neispravan unos tezine");
                }

                natjecanje.Zadaci = DbMethods.dbGetZadatakZaVirtualno(tezina);
                natjecanje.BrojZadataka = natjecanje.Zadaci.Count();

                return natjecanje;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }
    }
}
