﻿using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using backend.DTOs;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ZadatakController : Controller
    {
        // dodavanje novog zadatka - može samo voditelj ili admin
        /*
        [HttpPost]
        [Route("noviZadatak")]
        public ActionResult Post([FromBody] Zadatak newZad)
        {
            try
            {
                if (DbMethods.dbGetZadatakByNaziv(newZad.NazivZadatak) != null)
                {
                    return BadRequest("Zadatak s navedenim naslovom već postoji");
                }
                DbMethods.dbNoviZadatak(newZad);
                return Ok("Pohranjen novi zadatak");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }

        */

        //voditelj i vise
        [HttpPost]
        [Route("noviZadatak")]
        public ActionResult Post([FromBody] NoviZadatakDTO newZad)
        {
            try
            {
                try 
                { 
                    if (DbMethods.dbGetZadatakByNaziv(newZad.NazivZadatak) != null)
                    {
                        return BadRequest("Zadatak s navedenim naslovom već postoji");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Zadatak sa zadanim naslovom ne postoji. Nastavi upload");
                }
                DbMethods.dbNoviZadatak(new Zadatak
                {
                    NazivZadatak = newZad.NazivZadatak,
                    Bodovi = newZad.Bodovi,
                    MaxVrijeme = TimeSpan.FromSeconds(newZad.MaxVrijeme),
                    TekstZadatak = newZad.TekstZadatak,
                    Vidljivost = newZad.Vidljivost,
                    IdKorisnik = newZad.IdKorisnik
                });

                var stvoreniZadatak = DbMethods.dbGetZadatakByNaziv(newZad.NazivZadatak);

                foreach (var testniPodatak in newZad.TestniPodaci)
                {
                    testniPodatak.IdZadatak = stvoreniZadatak.IdZadatak;
                    DbMethods.dbNoviTestniPodatak(testniPodatak);
                }
                return Ok("Pohranjen novi zadatak");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }


        // pregled svih korisniku vidljivih zadataka
        // ako je vidljivost zadatka 1 
        [HttpGet]
        [Route("pregledSvihZadataka")]
        public ActionResult<List<Zadatak>> Get()
        {
            try
            {
                var res = DbMethods.dbGetSviVidljiviZadaci();
                return res;
            }
            catch (Exception err)
            {
                return NotFound("Zadaci se ne mogu dohvatiti!" + err.Message);
            }
        }

        //natjecatelji i vise
        [HttpGet]
        [Route("pregledSvihRjesenjaZadatka/{id:int}")]
        public ActionResult<List<RezultatDto>> GetAllRjesenjaZadatka(int id)
        {
            try
            {
                /*var zad = DbMethods.dbGetZadatakById(id);

                var res = new List<byte[]>();
                
                using var enumerator = zad.Rezultats.GetEnumerator();
                while (enumerator.MoveNext()) {
                    Rezultat val = enumerator.Current;
                    res.Add(val.PredanoRjesenje);
                }
            */
                /*   foreach (var rezultat in zad.Rezultats)
                   {
                       res.Add(rezultat.PredanoRjesenje);
                   }
                   return res;
               }
               catch (Exception err)
               {
                   return NotFound("Nije bilo moguce dohvatiti zadatke za korisnika!" + id + "error: " + err.Message);
               }*/
                var rezultati = DbMethods.dbGetRjesenjaZadatka(id).ToList();
                return rezultati;
            }
            catch (Exception err)
            {
                return NotFound("Nije bilo moguce dohvatiti rjesenja zadatka " + id + "error: " + err.Message);
            }
        }


        //natjecatelji i vise
        [HttpGet]
        [Route("{id:int}")]
        public ActionResult<Zadatak> DohvatiZadatak(int id)
        {
            try
            {
                Zadatak zadatak = DbMethods.dbGetZadatakById(id);
                return zadatak;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.InnerException.Message);
            }

            
        }

        //uredivanje vec napravljenog zadatka
        [HttpPost]
        [Route("promijeniZadatak/{id}")]
        public ActionResult<Zadatak> SpremiPromjeneZadatak(int id, [FromBody] EditZadatakDTO zad)
        {
            try
            {
                Zadatak original = DbMethods.dbGetZadatakById(id);

                // provjera jel to voditelj ciji je zadatak?

                if (original == null)
                {
                    return BadRequest("Zadatak s navedenim id ne postoji");
                }

                

                if (original.NazivZadatak != zad.NazivZadatak)
                {
                    try
                    {
                        if (DbMethods.dbGetZadatakByNaziv(zad.NazivZadatak) != null)
                        {
                            return BadRequest("Zadatak s navedenim naslovom već postoji");
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Zadatak sa zadanim naslovom ne postoji. Nastavi upload");
                    }
                }

                original.NazivZadatak = zad.NazivZadatak;
                original.Bodovi = zad.Bodovi;
                original.MaxVrijeme = TimeSpan.FromSeconds(zad.MaxVrijeme);
                original.TekstZadatak = zad.TekstZadatak;
                original.Vidljivost = zad.Vidljivost;

                DbMethods.dbSpremiPromjeneZadatak(original.IdZadatak, original);
                return Ok("Pohranjene promjene na zadatku");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.InnerException.Message);
            }
        }
        
    }
}
