﻿using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using backend.DTOs;
using backend.Interfaces;

namespace backend.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ITokenService _tokenService;

        public LoginController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("/api/v1/login")]
        public ActionResult<KorisnikDto> Post([FromBody] Korisnik user)
        {
            try
            {
                var korisnik = DbMethods.dbGetKorisnikByEmail(user.Email);
                var hasher = SHA256.Create();
                var hashedPassword = Convert
                    .ToHexString(hasher.ComputeHash(Encoding.UTF8.GetBytes(user.Password)))
                    .ToLower();

                if (korisnik is null)
                {
                    return NotFound("Traženi korisnik nije pronađen");
                }
                
                if (hashedPassword != korisnik.Password)
                {
                    return BadRequest("Pogrešna lozinka");
                }

                if (korisnik.PotvrdjenaPrivilegija)
                {
                    return new KorisnikDto(korisnik,
                        _tokenService.CreateToken(korisnik.IdKorisnik.ToString(), korisnik.KorisnickoIme,
                            korisnik.IdPrivilegija.ToString()));
                }
                
                return Ok();

            }
            catch (Exception err)
            {
                return BadRequest(err);
            }
        }
    }
}
