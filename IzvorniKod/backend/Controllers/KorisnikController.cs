﻿using backend.DTOs;
using backend.Interfaces;
using backend.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class KorisnikController : ControllerBase
    {
        private readonly ITokenService _tokenService;

        public KorisnikController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }
        
        [HttpGet]
        [Route("unconfirmedUsers")]
        public ActionResult<IEnumerable<UnconfirmedKorisnikDto>> Get()
        {
            try
            {
                if (!AuthMethods.IsUserAdmin(HttpContext.User.Identity))
                {
                    return Unauthorized(); 
                }
                
                var response = DbMethods.dbGetAllUnconfirmedPrivilegeKorisnik();
                return response.Select(korisnik => new UnconfirmedKorisnikDto(korisnik)).ToList();
            }
            catch (Exception err)
            {
                return NotFound("Ne može se pristupiti bazi podataka!" + err.Message);
            }
        }
        
        [HttpGet("{id:int}")]
        public ActionResult<Korisnik> Get(int id)
        {
            try
            {
                if (!AuthMethods.IsUserHimselfOrAdmin(HttpContext.User.Identity, id))
                {
                    return Unauthorized(); 
                }

                return DbMethods.dbGetKorisnikByIdKorisnik(id);
            }
            catch (Exception err)
            {
                return NotFound("Traženi korisnik ne postoji!" + err.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("confirmUserMail/{uuid}")]
        public ActionResult ConfirmKorisnikMail(string uuid)
        {
            try
            {
                DbMethods.dbUpdateKorisnikPotvrdjenMail(uuid);
                //return Ok("Korisnikov mail je potvrđen. Vratite se na Login stranicu.");
                return Redirect(AppSettingsProvider.Configuration["UrlPrefix"]);
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }
        
        [Route("confirmUserPrivilege/{id:int}")]
        [HttpGet]
        public ActionResult<Korisnik> ConfirmKorisnikPrivilege(int id)
        {
            try
            {
                if (!AuthMethods.IsUserAdmin(HttpContext.User.Identity))
                {
                    return Unauthorized(); 
                }
                
                DbMethods.dbUpdateKorisnikPotvrdjenaPrivilegija(id);
                
                return Ok("Korisnikova privilegija je potvrđena.");
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }


        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (!AuthMethods.IsUserHimselfOrAdmin(HttpContext.User.Identity, id))
                {
                    return Unauthorized(); 
                }
                
                DbMethods.dbDeleteKorisnikWithIdKorisnik(id);
                return Ok("Korisnik s id = " + id + " je obrisan.");
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }

        [Route("searchUsers")]
        [HttpGet]
        public IEnumerable<string> SearchUsers([FromQuery] string input)
        {
            return DbMethods.searchUsersByName(input);
        }

        [Route("getProfile")]
        [HttpGet]
        public ActionResult<KorisnikProfileDTO> GetProfile([FromQuery] string username)
        {
            try
            {
                return DbMethods.getUserProfile(username);
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }

        [Route("editProfile/{id}")]
        [HttpPost]
        public ActionResult<Korisnik> EditProfile(int id, [FromBody] Korisnik editanKorisnik)
        {
            try
            {
                Korisnik original = DbMethods.dbGetKorisnikByIdKorisnik(id);

                if (original == null)
                {
                    return BadRequest("Korisnik s navedenim id ne postoji");
                }



                if (original.KorisnickoIme != editanKorisnik.KorisnickoIme)
                {
                    try
                    {
                        if (DbMethods.dbGetKorisnikByKorisnickoIme(editanKorisnik.KorisnickoIme) != null)
                        {
                            return BadRequest("Korisnik s navedenim korisničkim imenom već postoji");
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Korisničko ime nije zauzeto. Nastavi upload.");
                    }
                }

                original.KorisnickoIme = editanKorisnik.KorisnickoIme;
                original.Ime = editanKorisnik.Ime;
                original.Prezime = editanKorisnik.Prezime;
                original.IdPrivilegija = editanKorisnik.IdPrivilegija;
                DbMethods.dbSpremiPromjeneKorisnik(original.IdKorisnik, original);
                return Ok("Pohranjene promjene na korisniku");

            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.InnerException.Message);
            }
        }

        [Route("updateProfilePicture")]
        [HttpPost]
        public ActionResult UpdateProfilePicture([FromBody] EditPictureDTO pictureJSON)
        {
            try
            {
                byte[] picture = pictureJSON.Picture;
                var id = AuthMethods.GetTokenId(HttpContext.User.Identity);
                DbMethods.updateProfilePicture(id, picture);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }

        [Route("deleteProfilePicture")]
        [HttpPost]
        public ActionResult DeleteProfilePicture()
        {
            try
            {
                var id = AuthMethods.GetTokenId(HttpContext.User.Identity);
                DbMethods.deleteProfilePicture(id);
                return Ok();
            }
            catch (Exception)
            {
                return NotFound("Traženi korisnik ne postoji!");
            }
        }
        [HttpGet]
        [Route("brojTocnihRjesenja/{id}")]
        public ActionResult<int> BrojTocnihRjesenja(int id)
        {
            try
            {
                return DbMethods.dbGetTocnaRjesenjaKorisnik(id);
            }catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }

        [HttpGet]
        [Route("brojDjelomicnihRjesenja/{id}")]
        public ActionResult<int> BrojDjelomicnihRjesenja(int id)
        {
            try
            {
                return DbMethods.dbGetDjelomicnaRjesenjaKorisnik(id);
            }catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }
        [HttpGet]
        [Route("korisnikovaNapravljenaNatjecanja/{id}")]
        public ActionResult<IEnumerable<NatjecanjeKalendarDTO>> KorisnikovaNapravljenaNatjecanja(int id)
        {
            try
            {
                /*
                if (!AuthMethods.IsUserHimselfOrAdmin(HttpContext.User.Identity, id))
                {
                    return DbMethods.dbGetKorisnikNapravioSvaNatjecanja(id).ToList();
                }
                */
            
                return DbMethods.dbGetKorisnikNapravioNevirtualnaNatjecanja(id).ToList();
            }
            catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }
        [HttpGet]
        [Route("voditeljeviNapravljeniZadaci/{id}")]
        public ActionResult<IEnumerable<Zadatak>> VoditeljeviNapravljeniZadaci(int id)
        {
            try
            {
                var zadatak = DbMethods.dbGetKorisnikNapravioZadatke(id).ToList();
                if (!AuthMethods.IsUserHimselfOrAdmin(HttpContext.User.Identity, id))
                {
                    return zadatak.Where(x => x.Vidljivost == true).ToList();
                } else
                {
                    return zadatak;
                }
            }
            catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
            
        }



        [HttpGet]
        [Route("pobjedioNatjecanja/{id}")]
        public ActionResult<List<PobjednikDTO>> TrofejiKorisnika(int id)
        {
            try
            {
                var natIds = DbMethods.dbVratiIdPobjedenihNatjecanja(id);
                List<PobjednikDTO> result = new List<PobjednikDTO>();
                foreach (int idNat in natIds)
                {
                    result.Add(new PobjednikDTO(DbMethods.dbGetNatjecanjeById(idNat)));
                }
                return Ok(result);
            }
            catch (Exception error)
            {
                return BadRequest("Greška!\n" + error.InnerException?.Message);
            }
        }

    }
}
    