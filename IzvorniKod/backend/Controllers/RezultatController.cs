﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CliWrap;
using CliWrap.Buffered;
using FileIO = System.IO.File;
using backend.Models;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using backend.DTOs;
using System.Threading;

namespace backend.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RezultatController : Controller
    {
        [HttpPost]
        [Route("predajRjesenje")]
        public async Task<ActionResult<RezultatReturnInfoDTO>> PredajRjesenje([FromBody] EvaluacijaDTO predanoRj)
        {

            DateTime postTime = DateTime.Now.AddHours(1);
            Natjecanje natjecanje = null;
            if (predanoRj.idNatjecanje != null)
            {
                if (!(DbMethods.dbPostojiNatjecanje((int)predanoRj.idNatjecanje)))
                {
                    return BadRequest("Natjecanje ne postoji!");
                }
                natjecanje = DbMethods.dbGetNatjecanjeById((int)predanoRj.idNatjecanje);
                if (natjecanje.VrijemeKraj < postTime)
                {
                    return BadRequest("Vrijeme je isteklo!");
                }
                if (natjecanje.VrijemePocetak > postTime)
                {
                    return BadRequest("Natjecanje jos nije pocelo!");
                }
            }
            //List<TestniPodaci> tp = rjesenje.IdZadatakNavigation.TestniPodacis.ToList();
            //int maxBodovi = rjesenje.IdZadatakNavigation.Bodovi;
            //mozda treba ovako ali ne vidim zasto ako imam rjesenje.IdZadatakNavigation?
            Zadatak zadatak = DbMethods.dbGetZadatakById(predanoRj.idZadatak);
            IEnumerable<TestFileDTO> testpod = DbMethods.getTestFiles(zadatak);
            string userName = predanoRj.korisnickoIme;

            int maxBodovi = zadatak.Bodovi;

            int brTestova = testpod.Count();
            int brTocnihRj = 0;

            long sumOfRuntime = 0;

            //var resultSB = new StringBuilder();
            var povratnaPoruka = "Uspjesno evaluirano!";

            //prep
            await Cli.Wrap("mkdir").WithArguments($"./TempPython/{userName}").ExecuteAsync();

            FileIO.WriteAllBytes($"./TempPython/{userName}/UserSentProgram.py", predanoRj.programBase64);
            foreach (TestFileDTO pair in testpod)
            {
                FileIO.WriteAllBytes($"./TempPython/{userName}/input{pair.id}.txt", pair.input);
                FileIO.WriteAllBytes($"./TempPython/{userName}/output{pair.id}.txt", pair.output);


                var stdOutBuffer = new StringBuilder();

                await using var input = FileIO.OpenRead($"./TempPython/{userName}/input{pair.id}.txt");
                string correctOutput = FileIO.ReadAllText($"./TempPython/{userName}/output{pair.id}.txt");
                correctOutput = correctOutput.Replace("\r\n", "\n");

                using var cts = new CancellationTokenSource();
                cts.CancelAfter(zadatak.MaxVrijeme);

                //timer
                var watch = System.Diagnostics.Stopwatch.StartNew();

                try
                {
                    await Cli.Wrap("python3").WithArguments($"UserSentProgram.py")
                                .WithWorkingDirectory($"./TempPython/{userName}/")
                                .WithStandardInputPipe(PipeSource.FromStream(input))
                                .WithStandardOutputPipe(PipeTarget.ToStringBuilder(stdOutBuffer)) //.WithStandardErrorPipe(PipeTarget.ToStringBuilder(stdErrBuffer))
                                .ExecuteAsync(cts.Token);
                } 
                catch (Exception error)
                {
                    //resultSB.Append(error.Message);
                    povratnaPoruka = error.Message;
                }

                watch.Stop();
                sumOfRuntime += watch.ElapsedMilliseconds;

                var userOutput = stdOutBuffer.ToString();

                //resultSB.Append(userOutput);
                //resultSB.Append("-------------------------------------------------------------------------------------\n");
                //resultSB.Append(correctOutput);
                //resultSB.Append("=====================================================================================\n");
                if (userOutput == correctOutput)
                {
                    brTocnihRj++;
                }
            }

            decimal earnedPoints = (decimal)Math.Round((double)maxBodovi * brTocnihRj / brTestova, 2);
            //dobiveni bodovi u tablici trebaju biti tipa koji omogucuje decimale

            //obrisi folder
            await Cli.Wrap("rm").WithArguments($"-r {userName}").WithWorkingDirectory("./TempPython").ExecuteAsync();

            Rezultat rjesenje = new Rezultat
            {
                DobiveniBodovi = earnedPoints,
                VrijemePredaje = postTime,
                PredanoRjesenje = predanoRj.programBase64,
                Trajanje = TimeSpan.FromMilliseconds(sumOfRuntime),
                IdZadatak = predanoRj.idZadatak,
                IdKorisnik = predanoRj.idKorisnik
            };
            Rezultat rezultat =  DbMethods.dbSpremiRezultat(rjesenje);

            if (predanoRj.idNatjecanje != null)
            {
                DbMethods.sbPoveziRezultatSaNatjecanjem(rezultat, natjecanje.IdNatjecanje);
            }

            //resultSB.Append($"{earnedPoints}/{maxBodovi}");
            var povratniInfo = new RezultatReturnInfoDTO
            {
                //PovratnaPoruka = resultSB.ToString(),
                PovratnaPoruka = povratnaPoruka,
                DobiveniBodovi = (double)earnedPoints,
                VrijemeMS = TimeSpan.FromMilliseconds(sumOfRuntime)
            };

            return povratniInfo;
            //var error = stdErrBuffer.ToString();
        }


        //Privremeni api za stavljanje unos ispis podataka
        [HttpPost]
        [Route("noviIOPar")]
        public async Task<ActionResult<string>> PostIO([FromBody] TestniPodaci podatak)
        {
            try
            {
                using (var dbContext = new CodeSharkDBContext())
                {
                    dbContext.TestniPodacis.Add(podatak);
                    var i = dbContext.SaveChanges();
                }
                return Ok("Pohranjen novi IO");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.GetType());
                return BadRequest("Greska!\n" + error.Message);
            }
        }
    }
}
