﻿using backend.Models;

namespace backend.Interfaces
{
    public interface ITokenService
    {
        string CreateToken(string idKorisnik, string korisnickoIme, string idPrivilegija);
    }
}
