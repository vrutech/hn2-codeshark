﻿using System.Dynamic;
using Microsoft.Extensions.Configuration;

namespace backend
{
    public static class AppSettingsProvider
    {
        public static IConfiguration Configuration { get; set; }
    }
}